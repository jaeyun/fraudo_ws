# README #

### What is this repository for? ###
Some code samples are uploaded under a ROS catkin package directory called *fraudo_motion_planning*. In order to compile the code satisfactorily a second ROS catkin package called *fraudo_msgs* is also uploaded. 
The *fraudo_motion_planning* package is dedicated to plan paths for tracked mobile robots over terrains specified from another ROS catkin package that publishes terrain models (this latter package is not uploaded to this repository). 


### A brief description ###

The main() function is defined in *src/fraudo_motion_plannig/planPathOverStaticGlobalMap.cpp*.

The essential functions for motion planning are defined in  
*src/fraudo_motion_planning/include/fraudo_motion_planning/Planner.hpp* and *src/fraudo_motion_planning/include/fraudo_motion_planning/RRTRelatedAlgorithms.hpp*. 

The following classes are defined in the fraudo_motion_planning package: 

* Terrain (in *src/fraudo_motion_planning/include/fraudo_motion_planning/Terrain.h*) 
* Robot (in *src/fraudo_motion_planning/include/fraudo_motion_planning/Robot.h*)
* Planner (in *src/fraudo_motion_planning/include/fraudo_motion_planning/Planner.hpp*)
* RRT (in *src/fraudo_motion_planning/include/fraudo_motion_planning/RRTRelatedAlgorithms.hpp*)
* BiRRT (in *src/fraudo_motion_planning/include/fraudo_motion_planning/RRTRelatedAlgorithms.hpp*)
* GraphNode (in *src/fraudo_motion_planning/include/fraudo_motion_planning/graph.hpp*)
* GraphEdge (in *src/fraudo_motion_planninginclude/fraudo_motion_planning/graph.hpp*)
* Graph (in *src/fraudo_motion_planning/include/fraudo_motion_planning/graph.hpp*)
* Config (in *src/fraudo_motion_planninginclude/fraudo_motion_planning/robotConfig.h*)
* TrackedMobileRobotConfig  (in *src/fraudo_motion_planning/include/fraudo_motion_planning/robotConfig.h*)
* TrackedMobileRobotWithArmConfig  (in *src/fraudo_motion_planning/include/fraudo_motion_planning/robotConfig.h*)

**Some comments on these classes:** 

* Both RRT and BiRRT have Graph, GraphNode and GraphEdge objects, which are used to build trees (formed by nodes and edges with no cycles). 
* Both RRT and BiRRT are children of Planner, where BiRRT inherits from RRT, and RRT inherits in turn from Planner. 
* Planner is a friend class of the Robot class, and both Planner and Robot classes are friends of the Terrain class. 
* Planner, RRT, and BiRRT are templated classes of type TConfig, which refers to various robot configurations defined in the children classes of the Config class.  

These packages have been satisfactorily compiled in the ROS hydro version. 

Notice that *fraudo_motion_planning* has linked a customized dynamic library called *libFraudoMotionPlanning.so*. 

Should you have further questions, please feel free to contact Jae Yun JUN KIM (jaeyunjk@gmail.com).