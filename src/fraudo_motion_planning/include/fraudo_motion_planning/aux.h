/**
 * @file aux.h  
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         June 24 2013; 
 * Lastly modified: April 30 2015; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2015>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#ifndef AUX_H_
#define AUX_H_

// ROS
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Header.h>
#include <tf/transform_broadcaster.h>

#include <fraudo_msgs/ElevationMap.h>
#include <fraudo_msgs/SegmentedScene.h>
#include <fraudo_msgs/Trajectory.h>

#include <pcl_conversions/pcl_conversions.h> // for ros hydro

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/surface/convex_hull.h>
#include <pcl/common/geometry.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/segmentation/sac_segmentation.h>

// Opencv
#include <opencv2/imgproc/imgproc.hpp>

// Other 
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm> // for standard template functions such as min(a,b), remove_if
#include <vector>

#include <Eigen/Dense>
#include <Eigen/StdVector> // for vector<Matrix3f,aligned_allocator<Matrix3f> > jacobian_;

#include <math.h> // for floor(), ceil(), and more
#include <limits>

#include <ctime>


// namespace fraudo{
	
// #define DOUBLE_PRECISION

#define NUM_CONTACT_FLIPPER 1 // instead of defining as a const int I define it with the preprocessing format in order to use preprocessor conditions such as #if.

#define TRACKED_MOBILEROBOT_CONFIG 		 0 
#define TRACKED_MOBILEROBOT_WITHARM_CONFIG  1
#define ROBOT_CONFIG_TYPE	TRACKED_MOBILEROBOT_CONFIG
//#define ROBOT_CONFIG_TYPE  TRACKED_MOBILEROBOT_WITHARM_CONFIG


#ifdef DOUBLE_PRECISION
  typedef double Real;
#else
  typedef float Real;  
#endif

inline Real maxRealValue()
{
  #ifdef DOUBLE_PRECISION
    return DBL_MAX;
  #else
    return FLT_MAX;
  #endif
}

// CONSTANTS
const int NUM_CONTACT_BODY = 3;

//const int NUM_CONTACT_TRACK = 2;
//const int NUM_CONTACT_TRACK = 3;
const int NUM_CONTACT_TRACK = 5;
//const int NUM_CONTACT_TRACK = 7;
//const int NUM_CONTACT_TRACK = 9;
//const int NUM_CONTACT_TRACK = 11; 

// const int NUM_CONTACT_FLIPPER = 1; //0; // 2; 
const int NUM_TOTAL_CONTACT = (2*NUM_CONTACT_TRACK + 2*NUM_CONTACT_FLIPPER); 
const int NUM_BOUNDING_BOX_PTS = 8; 

const int NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT = 7; // x, y, z, roll, pitch, yaw, ang_flipper 
const int NUM_JOINTS = 7; // one joint for the two coupled front flippers, and 6 joints for three wheels per side. 

const int NUM_SAMPLE_DIMENSIONS_TRACKEDMOBILEROBOT = 2; // x, y

const Real HEIGHT_MAX_DEFAULT = 10000.0;
const Real DEFAULT_NON_DEFINED_TERRAIN_HEIGHT = -1.0; // [m]

const Real ACCEL_GRAVITY = 9.81; // [m/s^2], gravitational acceleration. 

const Real TIME_INTEGRATION_TIME_LCP = 2.0; // [s]
const Real TOL_CONTACT_CRITERION_LCP = 1.5E-2; //5E-3 //0.015; [m]
const Real INCR_DISTANCE = 1.0E-5; // [m], value used for computing the terrain normal vector
const Real TOL_CONVERGENCE_LCP = 1.0E-8; // [J], energy
const Real NUM_MIN_CYCLES_LCP = 3; // [-] 

// Robot's body
const Real ROBOT_BODY_LENGTH = 0.670; // [m], along x-direction
const Real ROBOT_BODY_WIDTH = 0.367; // [m], along y-direction
const Real ROBOT_BODY_HEIGHT = 0.087; // [m], along z-direction 
const Real ROBOT_BODY_WIDTH_FRONT = 0.270; // [m], along y-direction

// Robot's tracks
const Real ROBOT_TRACK_LENGTH = 0.670; // [m], along x-direction
const Real ROBOT_TRACK_WIDTH = 0.073; // [m], along y-direction
const Real ROBOT_TRACK_HEIGHT = 0.190; // [m], along z-direction
const Real ROBOT_SPROCKET_RADIUS = (ROBOT_TRACK_HEIGHT / 2.0); // [m]
const Real ROBOT_DISTANCE_BW_TRACKS = 0.440; // [m]

// Robot's flippers
const Real ROBOT_FLIPPER_WIDTH = 0.045; // [m], along y-direction
const Real ROBOT_FLIPPER_HEIGHT_CLOSE_TO_MAIN_FRAME = 0.190; // [m], along z-direction
const Real ROBOT_FLIPPER_HEIGHT_FAR_FROM_MAIN_FRAME = (0.066 + 2*0.005); // [m], 66mm is the diameter of the far-end cylinder of the flipper and the 5mm is the thickness of the tracks
const Real ROBOT_FLIPPER_DIAMETER_CLOSE_TO_MAIN_FRAME = ROBOT_FLIPPER_HEIGHT_CLOSE_TO_MAIN_FRAME; // [m]
const Real ROBOT_FLIPPER_DIAMETER_FAR_FROM_MAIN_FRAME = ROBOT_FLIPPER_HEIGHT_FAR_FROM_MAIN_FRAME; // [m]
const Real ROBOT_FLIPPER_LENGTH = (0.297 + ROBOT_FLIPPER_DIAMETER_CLOSE_TO_MAIN_FRAME / 2); // [m], along x-direction

const Real RATIO_COM_FORWARD = 0.6; // [-]

// Robot's initial pose
const Real POSE_ROBOT_INIT_X = 0.48; // [m]
const Real POSE_ROBOT_INIT_Y = 0.84; // [m]
const Real POSE_ROBOT_INIT_Z = 5.0; // ROBOT_TRACK_HEIGHT/2; // [m]
const Real POSE_ROBOT_INIT_ROLL = 0.0; // [rad]
const Real POSE_ROBOT_INIT_PITCH = 0.0; // [rad]
const Real POSE_ROBOT_INIT_YAW = 0.0; //M_PI/2; // M_PI/4; [rad]

const Real ANG_FLIPPER_INIT = 0.1; //  0.1415; //-0.1415; // This should be the angle at which the flippers make contact with flat surface. // [rad], M_PI/2
//I should not lower the flippers too much b/c otherwise the evaluation for the collision detection will be always positive. 

const Real ANG0_ARM_INIT = 0.0; 
const Real ANG1_ARM_INIT = 0.0; 
const Real ANG2_ARM_INIT = 0.0; 
const Real ANG3_ARM_INIT = 0.0; 


const Real ROBOT_BODY_MASS = 27; // [kg], approximated total mass w/o arm. Essentially, this value corresponds to the mass of the robot body assuming that the mass of the tracks and flippers are small. 
const Real ROBOT_ARM_MASS = 15; // 
const Real ROBOT_TOTAL_MASS = (ROBOT_BODY_MASS+ROBOT_ARM_MASS); // 27; // [kg], approximated total mass w/o arm. Essentially, this value corresponds to the mass of the robot body assuming that the mass of the tracks and flippers are small.

const Real ROBOT_BODY_INERTIA_ROLL = (ROBOT_BODY_MASS / 12 * (ROBOT_BODY_HEIGHT*ROBOT_BODY_HEIGHT + ROBOT_BODY_WIDTH*ROBOT_BODY_WIDTH)); 
const Real ROBOT_BODY_INERTIA_PITCH = (ROBOT_BODY_MASS / 12 * (ROBOT_BODY_HEIGHT*ROBOT_BODY_HEIGHT + ROBOT_BODY_LENGTH*ROBOT_BODY_LENGTH)); 
const Real ROBOT_BODY_INERTIA_YAW = (ROBOT_BODY_MASS / 12 * (ROBOT_BODY_WIDTH*ROBOT_BODY_WIDTH + ROBOT_BODY_LENGTH*ROBOT_BODY_LENGTH)); 


const Real SUB_NODE_START_X = 24; // one less than Matlab index (since Matlab starts from 1 while c++ with 0). 
const Real SUB_NODE_START_Y = 14; //23; //14; //23 // one less than Matlab index (since Matlab starts from 1 while c++ with 0). 

// Robot's goal pose
const Real POSE_ROBOT_GOAL_X = 3.0; // [m]
const Real POSE_ROBOT_GOAL_Y = 3.0; // [m]
const Real POSE_ROBOT_GOAL_Z = 0.0; // [m]
const Real POSE_ROBOT_GOAL_ROLL = 0.0; // [rad]
const Real POSE_ROBOT_GOAL_PITCH = 0.0; // [rad]
const Real POSE_ROBOT_GOAL_YAW = 0.0; // [rad]

const Real SUB_NODE_GOAL_X = 24; // 25; // one less than Matlab index (since Matlab starts from 1 while c++ with 0). 
const Real SUB_NODE_GOAL_Y = 26; // 27; // one less than Matlab index (since Matlab starts from 1 while c++ with 0). 

// Terrain boundary safety margin
const Real MARGIN_SAFETY_BOUNDARY_TERRAIN = 0.1; // planning will be executed within 0.1m smaller than each boundary dimensions. For instance, (xmin+0.1,xmax-0.1, ymin+0.1, ymax-0.1)

// A*
const Real INCR_ANG_YAW = M_PI/4; // [rad]
const Real WEIGHT_COST_G = 0.1; 
const Real WEIGHT_COST_H = 1 - WEIGHT_COST_G; 
const Real WEIGHT_COST_STABILITY = 1.0; //1.0/3.0; //0.333; 
const Real WEIGHT_COST_YAW = 0.0; //1.0/3.0; //0.333; 
const Real WEIGHT_COST_DISTANCE = 0.0;// 1.0/3.0; //0.333; 
const int NUM_ACTION_PRIMITIVES = 3; 
const Real TOL_CONTACT_CRITERION = 1.5e-2; // [m] +/- tolerance for contact criterion.
const Real DIST_MAX_ACTION_IN_CELLS = sqrtf(2.0); // [cell]
const Real VEL_LINEAR_MAX_BODY = 0.05; // [m/s] 
const Real VEL_ANG_MAX_BODY = (1.0 * M_PI / 180.0); // [rad/s]
const Real VEL_ANG_MAX_FLIPPER = (90.0 * M_PI / 180.0); // [rad/s]
const Real TIME_INCR_TRAJ = 0.04; // [s], sampling time corresponding to 25Hz. 
const Real TOL_ERR_NUMERICAL_TRAJ = 1e-5; 
const int NUM_INTERPOLATION_TRAJ = 500; 

// GRADIENT METHOD FOR FLIPPER MOTION PLANNING
const Real TOL_CONV_GRAD_METHOD  = 1e-3; // [1/rad], Delta(tipover_stability) / Delta(flipper angle)
const Real STEP_SIZE_GRAD_METHOD = 1e-1; // [rad], 1e-1rad == 3 deg.

// Tip-over stability measure
const Real TIP_OVER_STABILITY_MEASURE_MAXIMUM = 308.0874662276026; // The minimum force angle value that corresponds to the maximum stability case (on a flat surface).

// TOL_NUM_ERR_FLOOR
const Real TOL_NUM_ERR_FLOOR = 1e-5; // I found a case where if this value is 1e-3 instead of solving the numerical error problem it adds more problems! Hence I lowered this value to 1e-5.  

// For RRTStar
const Real DISCRETIZATION_STEP = 0.1; // 0.01, // [m]
const int NUM_MAX_NEAR_NODES = 5; // to limit the number of near nodes to consider in order to make the algorithm faster.
const int NUM_MAX_UNACCEPTED_CONSECUTIVE_SAMPLES = 100; // [-]

// For TRRT/BITRRT
const int NUM_MAX_SAMPLES_FOR_COST_RANGE_COMPUTATION = 50; // [-]

const int OK  = 1; 
const int ERROR = 0;

const Real COEFF_FRICTION_TERRAIN = 1.0; 

const Real HEIGHT_DEFAULT_TO_FILL_THE_GAPS = -1.0; // [m], this is the height value assigned to holes or to those points which are added to make the map be square. 

// For the sequence of segmented real local maps: used in fraudo_planning.cpp and read_segmented_real_local_maps.cpp
const int NUM_LOCAL_MAPS_TO_KEEP_TRACK = 4; // 2

// For the flipper motion planning
const int IND_FLIPPER_MOTION_ANTICIPATION = 1; 

// For smoothing path
const int NUM_ITERATIONS_FOR_SMOOTHING_PATH = 30; //15
const Real DIST_STEP_POSE_ESTIMATION_FOR_SMOOTHING_PATH = 0.10; // [m]

// For sampling-based motion planning algorithms
const int NUM_MAX_ITERATIONS = 1e6; //5e5; //1e6; //1e5; //1e3; // 1e2; // 1e1

const Real STEP_SIZE_INTERP_ANG_YAW = 0.17453292519943295; // [rad], equivalent to 10deg.

// for bidirectional trees, the maximum number of pairs of two-tree-connecting nodes from which the pair that gives the smallest-cost-valued path is selected. 
const int NUM_MAX_PAIR_TWO_TREE_CONNECTING_NODES = 10; 

const int NUM_MAX_ITERA_COLLISION_CHECKING = 20; // for collision checking


enum RRT_RELATED_TREE_EXTENSION_RESULT {REACHED, ADVANCED, TRAPPED}; // REACHED:0, ADVANCED:1, TRAPPED:2

enum ROBOT_MODEL {POINT_MASS, RIGID_BODY}; 

enum GRAPH_TYPE {START_TREE, GOAL_TREE};

enum YAW_MOTION_START_TYPE {NO_YAW_MOTION_START, FWD_SHORT_START, FWD_LONG_START, BWD_SHORT_START, BWD_LONG_START}; 
enum YAW_MOTION_END_TYPE {NO_YAW_MOTION_END, YAW_SHORT_MOTION_END, YAW_LONG_MOTION_END}; 
enum CONFIG_CONNECTION_TYPE {TREE_EXTENSION, TWO_TREES_CONNECTION}; 

enum COST_FUNCTION_TYPE {MECHANICAL_WORK, TIP_OVER_STABILITY, ENERGY_CONSUMPTION, TIP_OVER_STABILITY_G1, TIP_OVER_STABILITY_G2, TIP_OVER_STABILITY_G3}; 

enum STUDY_TYPE {ALGORITHM_TYPE_ANALYSIS, COST_TYPE_ANALYSIS, POSE_ESTIMATION_ANALYSIS}; 

enum PLANNER_ID {ID_RRT,ID_TRRT,ID_BIRRT,ID_BITRRT}; 

// STRUCTS
struct Body{
	public: 			
		Eigen::Matrix<Real,3,NUM_CONTACT_BODY> pt_contact_body_frame, pt_contact_global_frame;
		Eigen::Matrix<Real,3,NUM_BOUNDING_BOX_PTS> vertex_body_body_frame, vertex_body_global_frame; 
		Eigen::Matrix<Real,3,1> centroid_body_frame, centroid_global_frame; 
}; 

struct Track{
	public:		
		Eigen::Vector3f pt_center_body_frame, pt_center_global_frame; 
		Eigen::Matrix<Real,3,NUM_CONTACT_TRACK> pt_contact_body_frame;//, pt_contact_global_frame;
		Eigen::Matrix<Real,NUM_CONTACT_TRACK,1> id_active_contact; 		
		
		Eigen::Matrix<Real,3,NUM_BOUNDING_BOX_PTS> vertex_body_track_frame, vertex_body_body_frame, vertex_body_global_frame; 
		Eigen::Matrix<Real,3,1> centroid_body_frame, centroid_global_frame;
}; 

struct Flipper{
	public: 		
		Eigen::Vector3f pt_fixed_body_frame, pt_fixed_global_frame; 
		
		#if NUM_CONTACT_FLIPPER > 0
			Eigen::Matrix<Real,3,NUM_CONTACT_FLIPPER> pt_contact_flipper_frame, pt_contact_body_frame;//, pt_contact_global_frame;		
		#else // just a dummy vector in order to avoid the compilation error. 
			Eigen::Matrix<Real,3,1> pt_contact_flipper_frame, pt_contact_body_frame;//, pt_contact_global_frame;		
		#endif
		
		Eigen::Matrix<Real,3,NUM_BOUNDING_BOX_PTS> vertex_body_virtual_flipper_frame, vertex_body_virtual_body_frame, vertex_body_virtual_global_frame; // definition of the virtual flipper for collision detection with the convex hulls that define segmented planes. 
		
		Eigen::Matrix<Real,3,NUM_BOUNDING_BOX_PTS> vertex_body_flipper_frame, vertex_body_body_frame, vertex_body_global_frame; 
		Eigen::Matrix<Real,4,1> centroid_flipper_frame, centroid_body_frame, centroid_global_frame; // for homogeneous transformations
}; 

struct Arm{
	public: 
		Eigen::Matrix<Real,4,1> origin_link0_body_frame; 
								
		Eigen::Matrix<Real,4,NUM_BOUNDING_BOX_PTS> vertex_link0_link0_frame, vertex_link0_body_frame, vertex_link0_global_frame; // dimension 4 is assigned to facilitate homogeneous transformations.
		Eigen::Matrix<Real,4,NUM_BOUNDING_BOX_PTS> vertex_link1_link1_frame, vertex_link1_body_frame, vertex_link1_global_frame; // dimension 4 is assigned to facilitate homogeneous transformations.
		Eigen::Matrix<Real,4,NUM_BOUNDING_BOX_PTS> vertex_link2_link2_frame, vertex_link2_body_frame, vertex_link2_global_frame; // dimension 4 is assigned to facilitate homogeneous transformations.
		Eigen::Matrix<Real,4,NUM_BOUNDING_BOX_PTS> vertex_link3_link3_frame, vertex_link3_body_frame, vertex_link3_global_frame; // dimension 4 is assigned to facilitate homogeneous transformations.
		
		Eigen::Matrix<Real,3,1> centroid_link0_body_frame, centroid_link0_global_frame;
		Eigen::Matrix<Real,3,1> centroid_link1_body_frame, centroid_link1_global_frame;
		Eigen::Matrix<Real,3,1> centroid_link2_body_frame, centroid_link2_global_frame;
		Eigen::Matrix<Real,3,1> centroid_link3_body_frame, centroid_link3_global_frame;
};


struct TrajectoryExtended{
	Real t; // time 
	Eigen::Matrix<Real,3,1> pose_horizontal; // x, y, yaw
	Eigen::Matrix<Real,3,1> pose_other_than_horiz; // z, roll, pitch
	Real vel_linear_body;
	Real vel_ang_body;
	Real ang_flipper; 
}; 

typedef Real coord;

typedef Real coord_t;    // coordinate type
typedef Real coord2_t;  // must be big enough to hold 2*max(|coordinate|)^2

struct Point {
    coord_t x, y, z;
    int ind; 
 
	Point(){}
	Point(coord_t x, coord_t y) : x(x), y(y) {}
	Point(coord_t x, coord_t y, coord_t z) : x(x), y(y), z(z) {}
	Point(coord_t x, coord_t y, coord_t z, int ind) : x(x), y(y), z(z), ind(ind) {}
    bool operator <(const Point &p) const {
		return x < p.x || (x == p.x && y < p.y);
    }
};


struct Point2D {
	coord_t x, y;
	Point2D(){}
	Point2D(coord_t x, coord_t y) : x(x), y(y) {}
 
    bool operator <(const Point2D &p) const {
		return x < p.x || (x == p.x && y < p.y);
    }
};

struct Point3D {
	Real x, y, z;
	Point3D(){}
	Point3D(Real x, Real y, Real z) : x(x), y(y), z(z) {}
    bool operator <(const Point3D &p) const { // I only worry about x and y directions.
		return ((x < p.x) || (x == p.x && y < p.y));
    }
};

// My customized point data type. See the following
// http://pointclouds.org/documentation/tutorials/adding_custom_ptype.php#how-to-add-a-new-pointt-type
// http://www.pcl-users.org/build-libraries-with-Custom-Type-td3944305.html
struct PointXYZRGBNormalSegmented{
	PCL_ADD_POINT4D; // preferred way of adding a XYZ+padding
	PCL_ADD_NORMAL4D; 
	Real rgb; 
	Real curvature; 
	// Real slope; 
	Real map_traversability; 
	Real id_segmented_plane; 
	Real id_pt; 
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW	
}EIGEN_ALIGN16; 

POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZRGBNormalSegmented,
								  (Real, x, x)
								  (Real, y, y)
								  (Real, z, z)
								  (Real, normal_x, normal_x)
								  (Real, normal_y, normal_y)
								  (Real, normal_z, normal_z)
								  (Real, rgb, rgb)
								  (Real, curvature, curvature)
								  (Real, map_traversability, map_traversability)
								  (Real, id_segmented_plane, id_segmented_plane)
								  (Real, id_pt, id_pt)
								  )

struct SimulationParamInput{
	std::string type_terrain; 
	std::string algorithm_planning; 
	std::string type_cost; 
	std::string type_study; 
	int id_trial; 	
	int seed; 	
	int do_store_results;
	Real trate_trrt; 	
	Real time_sampling_lcp; 
	Real distance_max_refinement;
	Real ratio_max_refinementNodes_to_totalNodes;
}; 
									  

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AUXILIARY FUNCTIONS
Eigen::Matrix<double,NUM_TOTAL_CONTACT,1> lcpSolverWithLemke(Eigen::Matrix<double,NUM_TOTAL_CONTACT,NUM_TOTAL_CONTACT> mat_B, Eigen::Matrix<double,NUM_TOTAL_CONTACT,1> vect_b, int& err); 

Real mod(Real a, Real N); 

// 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
// Returns a positive value, if OAB makes a counter-clockwise turn,
// negative for clockwise turn, and zero if the points are collinear.
coord2_t cross(const Point2D &O, const Point2D &A, const Point2D &B); 
coord2_t cross(const Point3D &O, const Point3D &A, const Point3D &B); 
 
// Returns a list of points on the convex hull in counter-clockwise order.
// Note: the last point in the returned list is the same as the first one.
std::vector<Point2D> convexHull(std::vector<Point2D> P); 
std::vector<Point3D> convexHull(std::vector<Point3D> P); 

bool waitForSubscribers(ros::Publisher & pub, ros::Duration timeout); 

Real computeDistance2D(Real x1, Real y1, Real x2, Real y2); 
Real computeDistance3D(Real x1, Real y1, Real z1, Real x2, Real y2, Real z2); 

int findIndActionPrimitive(Real ang);

Real modNPiPi(Real ang);

Real clampValueOnCircle(Real x, Real xmin, Real xmax); 

Real differenceOnCircle(Real x1, Real x2, Real xmin, Real xmax); 

Real deg2rad(Real deg); 
Real rad2deg(Real rad); 

void setPC2Channels(sensor_msgs::PointCloud2 &pc2_out); 

Real generateRandNumUniform(Real val_min, Real val_max); 

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

timespec diff(timespec start, timespec end); 

void setCloudChannels(sensor_msgs::PointCloud2 &pc2_out); 


// } // end of the namespace fraudo

#endif
