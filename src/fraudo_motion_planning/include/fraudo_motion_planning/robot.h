/**
 * @file robot.h
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         June 24 2013; 
 * Lastly modified: April 30 2015; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2015>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#ifndef ROBOT_H_
#define ROBOT_H_

#include <fraudo_motion_planning/aux.h>
#include <fraudo_motion_planning/terrain.h>

// namespace fraudo{


class Robot{
	
	template<class TConfig> friend class Planner; 	
	
	private:
		Body body_; 
		Flipper flipper_left_, flipper_right_; 
			
		Terrain* map_elev_curr_; 

		Eigen::Matrix<Real,3,NUM_TOTAL_CONTACT> pt_terrain_contact_; 		
				Eigen::Matrix<bool,NUM_TOTAL_CONTACT,1> is_contact_active_; //id_active_contact_; 
		int num_contact_active_; // total number of active contact points at a given instant of time.

		Eigen::Matrix<Real,3,NUM_TOTAL_CONTACT> pt_robot_potential_contact_body_frame_, pt_robot_potential_contact_global_frame_; 
		
		std::vector<Eigen::Matrix3d,Eigen::aligned_allocator<Eigen::Matrix3d> > jacobian_;
		Eigen::Matrix<double,3,NUM_TOTAL_CONTACT> vect_normal_terrain_; 
				
		Eigen::Matrix<Real,4,4> mat_bspline_terrain_, mat_transpose_bspline_terrain_; 
		
		Eigen::Matrix3d inertia_mat_, inertia_mat_inv_; 
		Eigen::Vector3d force_gravitational_;  // for pose estimation, [z,roll,pitch]
		
		
		ros::NodeHandle n_; // This node handler will use the namespace of the node. 
		                    // Be aware that I also declared a node handler in the Terrain class. 
		                    // I believe both of them will refer to the same handler with the same namespace, 
		                    // although the assigned memory address will be different.
		
		geometry_msgs::TransformStamped tf_baselink_wrt_map_; 
		sensor_msgs::JointState state_joint_; 
		
		tf::TransformBroadcaster tf_broadcaster_;
		
		ros::Publisher pub_joint_; 
		ros::Publisher pub_marker_;
		ros::Publisher pub_body_vertices_marker_; 
		
		std::string name_space_; 
		
		Real time_sampling_lcp_; 
									
	public: 
		Robot(const std::string& name_space);
		Robot(); 		
		~Robot(); 

		Track track_left_, track_right_; 
		Eigen::Matrix<Real,NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1> pose_; 
		
				
		void updatePoseRobot(); 
		void updatePoseRobot(Eigen::Matrix<Real,NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1> pose); 		
		
		Eigen::Matrix<Real,3,1> homogTransfFromBody2Global(Eigen::Matrix<Real,3,1> coords_body, Eigen::Matrix<Real,NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1> pose); 
		
		void homogTransfPtContactFromFlipper2Body(); 
		
		int estimatePose(); 
		int findPtsTerrainContact(); 
		int computeJacobianAndVectNormalTerrain(); 		
		
		void publishJointStateAndBroadcastTF(); 
		void printRobotState();
		
		void displayPosition(Real x, Real y, Real z, int id);
		void displayBodyVertices(Eigen::Matrix<Real,3,NUM_BOUNDING_BOX_PTS> vertex_body); 

		
		void updateElevMap(Terrain* map_elev_curr);
		
		Eigen::Matrix<Real,NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1> getPose(){ return pose_; }
		Real getPose(int ind){ return pose_[ind]; }
		
		Real getBodyPtContactGlobalFrame(int coord, int ind_pt){ return body_.pt_contact_global_frame(coord,ind_pt); }
		
		void setLCPSamplingTime(Real val){ time_sampling_lcp_ = val; }
		Real getLCPSamplingTime(){ return time_sampling_lcp_; }
		
		int defineArmCoMWorkSpace(); 
		
		Eigen::Matrix<Real,NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1> initRobotPose(); 
		
		Eigen::Matrix<Real,3,1> fwdKinematicsArmCoM(Eigen::Matrix<Real,4,1> ang_joint_link_arm); 
}; 

// } // end of namespace fraudo
#endif
