/**
 * @file robotConfig.h
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         April 28 2014; 
 * Lastly modified: March 20 2015; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2015>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#ifndef ROBOT_CONFIG_H_
#define ROBOT_CONFIG_H_

#include <fraudo_motion_planning/aux.h>
#include <fraudo_motion_planning/terrain.h>
#include <fraudo_motion_planning/robot.h>

/** @ingroup Config
 *  The class Config defines the configuration that characterizes the configuration of a robot. 
 */
class Config
{
	protected: 
		static int num_config_dimensions_; // static b/c all instations of this class objects will have the same number of configuration dimensions; 
		static int num_sample_dimensions_; // static b/c all instations of this class objects will have the same number of configuration dimensions; constant b/c this value will be assigned at the moment of the first instantiation and will never be changed.
		
	public: 

		Eigen::Matrix<Real,Eigen::Dynamic,1> q_; // a configuration from the configuration space

		Config() {}; 
		
		Config(const Config& config){
			this->q_ = config.q_;
		}
		
		Eigen::Matrix<Real,Eigen::Dynamic,1> getCoords(){ return q_; }; 
		void setCoords(Eigen::Matrix<Real,Eigen::Dynamic,1> q){ q_ = q; };
		int getNumConfigDimensions() { return this->num_config_dimensions_;};
		int getNumSampleDimensions() { return this->num_sample_dimensions_;};
		
		bool operator==(const Config& rhs) { 
			for(int i=0; i <num_config_dimensions_; ++i){
				if( (this->q_[i] - rhs.q_[i]) > 1e-4){ // numerical error tolerance
					return false; 
				}
			}
			return true; 			
		}; 
		
		bool operator!=(const Config& rhs) { 
			for(int i=0; i <num_config_dimensions_; ++i){
				if( (this->q_[i] - rhs.q_[i]) > 1e-4){ // numerical error tolerance 
					return true; 
				}
			}
			return false;
		}; 		
}; 

/**  @ingroup Graph
 *  The class TrackedMobileRobotConfig defines the configuration that characterizes the configuration of a 3D mobile robot. 
 */
class TrackedMobileRobotConfig : public Config
{
	private: 
	
	public: 
		TrackedMobileRobotConfig(){		
			this->q_.resize(NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1); // x, y, z, roll, pitch, yaw, ang_flipper
			this->q_ << POSE_ROBOT_INIT_X, POSE_ROBOT_INIT_Y, POSE_ROBOT_INIT_Z, POSE_ROBOT_INIT_ROLL, POSE_ROBOT_INIT_PITCH, POSE_ROBOT_INIT_YAW, ANG_FLIPPER_INIT; // x, y, z, roll, pitch, yaw, ang_flipper								
			
			this->q_[2] = 0.0; 
		};
		
		TrackedMobileRobotConfig(const TrackedMobileRobotConfig& config) : Config(config)
		{			
		};
		
}; 


/**  @ingroup Graph
 *  The class TrackedMobileRobotConfig defines the configuration that characterizes the configuration of a 3D mobile robot. 
 */
class TrackedMobileRobotWithArmConfig : public Config
{
	private: 
	
	public: 
		TrackedMobileRobotWithArmConfig(){		
						
			this->q_.resize(NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1); // x, y, z, roll, pitch, yaw, ang_flipper, ang0_arm, ang1_arm, ang2_arm, ang3_arm
			this->q_ << POSE_ROBOT_INIT_X, POSE_ROBOT_INIT_Y, POSE_ROBOT_INIT_Z, 
			            POSE_ROBOT_INIT_ROLL, POSE_ROBOT_INIT_PITCH, POSE_ROBOT_INIT_YAW, 
			            ANG_FLIPPER_INIT, 
			            ANG0_ARM_INIT, ANG1_ARM_INIT, ANG2_ARM_INIT, ANG3_ARM_INIT, 
			            0.0,0.0,0.0; // x, y, z, roll, pitch, yaw, ang_flipper, ang0_arm, ang1_arm, ang2_arm, ang3_arm, Xa, Ya, Za. The last three elements correspond to the CoM of the arm. 
			
			this->q_[2] = 0.0; 
		};
		
		TrackedMobileRobotWithArmConfig(const TrackedMobileRobotWithArmConfig& config) : Config(config)
		{					
		};		
}; 

#endif
