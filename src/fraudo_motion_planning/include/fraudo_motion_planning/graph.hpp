/**
 * @file graph.hpp
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         April 28 2014; 
 * Lastly modified: September 15 2014; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2013>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#ifndef GRAPH_H_
#define GRAPH_H_

#include <fraudo_motion_planning/robotConfig.h>

template<class TConfig> class GraphEdge; 
template<class TConfig> class GraphNode; 
template<class TConfig> class Graph;

/** @ingroup Graph 
 *  The class GraphNode defines the configuration that characterizes the configuration of a 3D mobile robot. 
 */
template<class TConfig> class GraphNode{
	friend class Graph<TConfig>; 
	
	private:
		int id_;
		
		Real cost_from_parent_; 
		Real cost_from_start_; 
		
		GraphNode<TConfig> *node_parent_;
		GraphEdge<TConfig> *edge_to_parent_;
		
		int num_parents_;
		Real radius_;
		
	public:
		
		TConfig config_; 
		
		std::list<GraphEdge<TConfig> *> edges_;
		
		GraphNode(); 
		GraphNode(TConfig config_in); 
		
		void printNodeInfo(); 
		int getID(){return id_;}
		Real getCostFromStart(){return cost_from_start_;}
		Real getCostFromParent(){return cost_from_parent_;}
		void setCostFromStart(Real cost){ cost_from_start_ = cost; }
		void setCostFromParent(Real cost){ cost_from_parent_ = cost; }
		void setNodeParent(GraphNode<TConfig>* node_parent){ node_parent_ = node_parent; }
		void setEdgeToParent(GraphEdge<TConfig>* edge_to_parent){ edge_to_parent_ = edge_to_parent; }
		void addEdge(GraphEdge<TConfig>* edge);
		void getEdges(std::vector<GraphEdge<TConfig> * >& edges);
		GraphNode<TConfig>* getParentNode(){ return node_parent_; }	
		GraphEdge<TConfig>* getEdgeToParentNode(){ return edge_to_parent_; }
		
		void setNumParents(int num_parents_in){ num_parents_ = num_parents_in; }	
		int getNumParents() { return num_parents_; }
		
		void setRadius(Real radius) {radius_ = radius; }
		Real getRadius(){return radius_;}
		
		GraphNode<TConfig>& operator=(const GraphNode<TConfig>& rhs) { 
			this->id_ = rhs.id_; 
			this->cost_from_start_ = rhs.cost_from_start_;
			this->cost_from_parent_ = rhs.cost_from_parent_;
			this->node_parent_ = rhs.node_parent_; 
			this->edge_to_parent_ = rhs.edge_to_parent_;
			this->config_ = rhs.config_; 
			this->num_parents_ = rhs.num_parents_; 
			this->radius_ = rhs.radius_; 
			
			this->edges_.clear(); 
			this->edges_.resize(rhs.edges_.size()); 
			std::copy(rhs.edges_.begin(), rhs.edges_.end(), this->edges_.begin()); 			
			
			return (*this); 
		}; 
}; 

/** @ingroup Graph 
 *  The class GraphEdge defines the configuration that characterizes the configuration of a 3D mobile robot. 
 */
template<class TConfig> class GraphEdge{
	friend class Graph<TConfig>; 
	friend class GraphNode<TConfig>; 
	
	private: 
		GraphNode<TConfig>* node_start_; 
		GraphNode<TConfig>* node_end_; 
		
	public:
		GraphEdge(); 
		GraphNode<TConfig>* getStartNode(){ return node_start_;} 
		GraphNode<TConfig>* getEndNode(){ return node_end_;}
		
		GraphEdge<TConfig>& operator=(const GraphEdge<TConfig>& rhs) { 
			this->node_start_ = rhs.node_start_; 
			this->node_end_ = rhs.node_end_;
			
			return (*this); 
		}
};
		

/** @ingroup Graph 
 *  The class Graph defines the configuration that characterizes the configuration of a 3D mobile robot. 
 */		
template<class TConfig> class Graph{ 
	
	public: 
		typename std::list<GraphNode<TConfig> * > nodes_; 
		typename std::list<GraphEdge<TConfig> * > edges_; 
		GRAPH_TYPE graph_type_; // identifies if the root node of the considered tree (graph) is the start or goal node.
		
		int num_samples_rejected_by_high_pitch_; 
		int num_samples_rejected_by_high_roll_; 	
						
		Graph(){}; 
		Graph(int graph_type) : graph_type_(graph_type){}; 
		
		Graph(Graph& graph){
			this->nodes_.clear(); 
			this->nodes_.resize(graph.nodes_.size()); 
			std::copy(graph.nodes_.begin(), graph.nodes_.end(), this->nodes_.begin()); 
			
			this->edges_.clear(); 
			this->edges_.resize(graph.edges_.size()); 
			std::copy(graph.edges_.begin(), graph.edges_.end(), this->edges_.begin()); 			
			
			this->graph_type_ = graph.graph_type_; 

		}; 
		~Graph(){ this->clear(); }; 

		int init(); 
		
		GraphNode<TConfig>* getNearestNode(Real (*distanceFunction)(const TConfig&, const TConfig&), TConfig config_in) const; 
		int getNearNodes(Real (*distanceFunction)(const TConfig&, const TConfig&), TConfig& config_in, const Real radius_in, std::vector<GraphNode<TConfig> * > & nodes_near_out) const;  
				
		GraphNode<TConfig>* addNode(const TConfig config_in); 
		void addNode(GraphNode<TConfig>* node_in);
		void removeLastlyAddedNode(); 
		
		inline int getNumNodes(){ return nodes_.size();}
		inline int getNumEdges(){ return edges_.size();} 
				
		int clear();
		GraphEdge<TConfig>* addEdge(GraphNode<TConfig>* node_start, GraphNode<TConfig>* node_end);		
		int removeEdge(GraphEdge<TConfig>* edge);
		
		int searchForMinimumCostPath_dijkstra(GraphNode<TConfig>* node_start, GraphNode<TConfig>* node_end, std::vector<TConfig>& configs_path_optimal);
		int extractPath(GraphNode<TConfig>* node_start, GraphNode<TConfig>* node_best_in_goal_set, TConfig & config_goal, std::vector<TConfig>& config_path_optimal);
		
		Graph<TConfig>& operator=(const Graph<TConfig>& rhs) { 
			this->nodes_.clear(); 
			this->nodes_.resize(rhs.nodes_.size()); 
			std::copy(rhs.nodes_.begin(), rhs.nodes_.end(), this->nodes_.begin()); 
			
			this->edges_.clear(); 
			this->edges_.resize(rhs.nodes_.size()); 
			std::copy(rhs.edges_.begin(), rhs.edges_.end(), this->edges_.begin()); 			
			
			this->graph_type_ = rhs.graph_type_; 		
			
			return (*this); 
		};
}; 


/**
 * This clear() function clears the entire graph.
 * @return The error status of the function is returned as of an integer value.
*/
template<class TConfig> int Graph<TConfig>::clear()
{
	typename std::list<GraphNode<TConfig> * >::iterator inode;
	typename std::list<GraphEdge<TConfig> * >::iterator iedge;

	for(inode=nodes_.begin(); inode!=nodes_.end(); ++inode){
		delete (*inode);
	}

	for(iedge=edges_.begin(); iedge!=edges_.end(); ++iedge){
		delete (*iedge);
	}

	nodes_.clear();
	edges_.clear();

	return OK;
}

/**
 * This clear() function clears the entire graph.
 * @return The error status of the function is returned as of an integer value.
*/
template<class TConfig> int Graph<TConfig>::init()
{
	this->clear(); 		
	
	num_samples_rejected_by_high_pitch_ = 0; 
	num_samples_rejected_by_high_roll_ = 0; 
	
	return OK;
}

/**
 * This getNearestNode() function finds the node that belongs to the tree and is the nearest to the newly sampled node in the configuration space.
 * @param state_in A newly sampled state.
 * @return The function returns the updated set of nodes that form a tree.
*/
template<class TConfig> GraphNode<TConfig> * Graph<TConfig>::getNearestNode(Real (*distanceFunction)(const TConfig&, const TConfig&), TConfig config_in) const
{
	Real dist, dist_min = 0.0;
	GraphNode<TConfig>* result = NULL;
	typename std::list<GraphNode<TConfig> * >::const_iterator inode;

	for(inode=nodes_.begin(); inode!=nodes_.end(); ++inode){
		dist = distanceFunction(config_in,(*inode)->config_);		

		if ( inode==nodes_.begin() || (dist < dist_min) ){
			result = *inode;
			dist_min = dist;
		}
	}

	return result;
}

/**
 * This addNode() function adds a new node to the tree.
 * @param state_in The state of a new node. 
 * @return The updated set of the nodes that define the tree is returned.
*/
template<class TConfig> GraphNode<TConfig> * Graph<TConfig>::addNode(const TConfig config_in)
{
	GraphNode<TConfig>* node_new = NULL;
	node_new = new GraphNode<TConfig>(config_in);
	node_new->id_ = nodes_.size();

	nodes_.push_back(node_new);

	return node_new;
}

/**
 * This addNode() function adds a new node to the tree.
 * @param node_in The new node to be included to the tree. 
*/
template<class TConfig> void Graph<TConfig>::addNode(GraphNode<TConfig> * node_in)
{
	// The node_in is dynamically allocated prior the call of this function.
	// An ID is given to the node according to the current tree status and the node is added to the tree.
	node_in->id_ = nodes_.size();

	nodes_.push_back(node_in);
}

/**
 * This removeLastlyAddedNode() function removes the lastly-added node from the tree.
*/
template<class TConfig> void Graph<TConfig>::removeLastlyAddedNode()
{
	nodes_.pop_back();
}

/**
 * This getNearNodes() function gets the nodes that are within a neighborhood ball defined around a node. 
 * @param state_in The state of the node for which its neighboring nodes are of interest.
 * @param radius_in The radius of the neighborhood ball.
 * @param nodes_near_out A reference to the container for the neighboring nodes.  
 * @return The error status of the function is returned as of an integer value.
*/
template<class TConfig> int Graph<TConfig>::getNearNodes(Real (*distanceFunction)(const TConfig&, const TConfig&), TConfig& config_in, const Real radius_in, std::vector<GraphNode<TConfig> * >& nodes_near_out) const
{
	Real dist;
	typename std::list<GraphNode<TConfig> * >::const_iterator inode;

	nodes_near_out.clear();

	for(inode = nodes_.begin(); inode!=nodes_.end(); ++inode){
		dist = distanceFunction(config_in,(*inode)->config_);		

		if(dist < radius_in){
			nodes_near_out.push_back(*inode);			
		}
	}

	return OK;
}


/**
 * This addEdge() function adds a new edge to the tree.
 * @param node_start The start node of the edge to be added.
 * @param node_end The end node of the edge to be added.
 * @return The updated set of the edges that define the tree is returned.
*/
template<class TConfig> GraphEdge<TConfig>* Graph<TConfig>::addEdge(GraphNode<TConfig> * node_start, GraphNode<TConfig> * node_end)
{
	// 1. Check first that the two nodes belong to the graph.
	typename std::list<GraphNode <TConfig> * >::iterator inode;
	int i=0,j=0;
	for(inode=nodes_.begin(); inode!=nodes_.end(); ++inode){
		if( (*inode)==node_start){ i++; }
		if( (*inode)==node_end){ j++; }
	}
	if (i+j != 2){
		std::cout << "Input nodes do not belong to the graph." << std::endl;
		return NULL;
	}

	// Generate an edge and add it to the graph
	GraphEdge<TConfig> * edge = new GraphEdge<TConfig>;
	edge->node_start_ = node_start;
	edge->node_end_ = node_end;

	edges_.push_back(edge);

	node_start->addEdge(edge);
	node_end->addEdge(edge);

	return edge;
}

/**
 * This removeEdge() function removes an edge from the tree.
 * @param edge A pointer to the edge to be removed from the tree.
*/
template<class TConfig> int Graph<TConfig>::removeEdge(GraphEdge<TConfig> * edge)
{
	typename std::list<GraphEdge<TConfig> * >::iterator iedge;
	std::vector<GraphNode<TConfig> * > nodes;

	// remove the edge from the graph's edge list
	iedge = find(edges_.begin(), edges_.end(), edge);
	if(iedge == edges_.end()){
		std::cout << "The edge to be removed does not belong to the graph." << std::endl;
		return ERROR;
	}

	// because the graph can have more than one same edge(?)
	iedge = edges_.begin();
	while( iedge!=edges_.end() ){
		if ((*iedge) == edge){
			iedge = edges_.erase(iedge);
		}
		++iedge;
	}

	// also remove the edge from the edge list of its two nodes
	nodes.push_back(edge->node_start_);
	nodes.push_back(edge->node_end_);
	for(int i=0; i<(int)nodes.size(); ++i){
		iedge = nodes[i]->edges_.begin();
		while(iedge!=nodes[i]->edges_.end()){
			if ((*iedge) == edge){
				iedge = nodes[i]->edges_.erase(iedge);
				continue;
			}
			++iedge;
		}
	}

	delete edge;

	return OK;
}

/**
 * The basic constructor for the class GraphNode.
*/
template<class TConfig> GraphNode<TConfig>::GraphNode()
{
	id_ = 0;
	cost_from_start_ = 0.0;
	cost_from_parent_ = 0.0;
	num_parents_ = 0; 
	radius_ = 0.0f; 
	node_parent_ = NULL;
	edge_to_parent_ = NULL;
}

/**
 * The constructor for the class GraphNode that initializes its state, a member variable.
*/
template<class TConfig> GraphNode<TConfig>::GraphNode(TConfig config_in) : config_(config_in) // The definition of the data type TConfig is already given during the time instant when the class GraphNode is defined. 
{
	id_ = 0;
	cost_from_start_ = 0.0;
	cost_from_parent_ = 0.0;
	num_parents_ = 0; 
	radius_ = 0.0f; 
	node_parent_ = NULL;
	edge_to_parent_ = NULL;
}

/**
 * This printNodeInfo() function displays the information about a node.
*/
template<class TConfig> void GraphNode<TConfig>::printNodeInfo()
{
	std::cout << "ID: " << this->id_ << std::endl
	     << "Cost from start: " << this->cost_from_start_ << std::endl
	     << "Cost from parent: " << this->cost_from_parent_ << std::endl
		 << "Number of parents: " << this->num_parents_ << std::endl
		 << "Number of edges: " << this->edges_.size() << std::endl
		 << "radius: " << this->radius_ << std::endl; 
	
	std::cout << "state: " << std::endl;	 
	for(int i=0; i<this->state_.getNumDimensions(); ++i){
		std::cout << "\t" << this->config_.q_[i] << std::endl; 
	}			
}

/**
 * This addEdge() function adds an edge to a node.
 * @param edge A pointer to an edge to be associated with a node.
*/
template<class TConfig> void GraphNode<TConfig>::addEdge(GraphEdge<TConfig> *edge)
{
	edges_.push_back(edge);
}

/**
 * This getEdges() function gets the edges associated to a node.
 * @param edges A reference to a vector of edges associated to a node.
*/
template<class TConfig> void GraphNode<TConfig>::getEdges(std::vector<GraphEdge<TConfig> * > & edges)
{
	edges.resize(edges_.size());
	copy(edges_.begin(), edges_.end(), edges.begin());
}


/**
 * The basic constructor for the class GraphEdge.
*/
template<class TConfig> GraphEdge<TConfig>::GraphEdge()
{
	node_start_ = NULL;
	node_end_ = NULL;
}

#endif
