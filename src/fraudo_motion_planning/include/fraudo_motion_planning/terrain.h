/**
 * @file terrain.h
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         June 24 2013; 
 * Lastly modified: September 15 2014; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2013>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#ifndef TERRAIN_H_
#define TERRAIN_H_

#include <fraudo_motion_planning/aux.h>

// namespace fraudo{

/**
 The class Terrain is used to define and work with terrains, particularly with elevation maps. 
*/
class Terrain{
		
	friend class Robot; 
	template<class TConfig> friend class Planner; 
	
	private: 
		
		Real coeff_friction_; /*! defines the friction coefficient of the terrain.*/ 
				
		Real size_; 
				
		Eigen::Matrix<int,Eigen::Dynamic,Eigen::Dynamic> map_traversability_; 
		
		Eigen::MatrixXi id_plane_; 
		
		Eigen::Matrix<Real,Eigen::Dynamic,Eigen::Dynamic> X_ext_, Y_ext_, Z_ext_; 
						
		int ind_node_init_;
		Eigen::Matrix<int,2,1> sub_node_start_; //, sub_node_goal_; // matrix's subindices, equivalent to ind_node_init_ and ind_node_goal_ 				
		Eigen::Matrix<Real,NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1> pose_robot_start_; //, pose_robot_goal_;		
		
		std_msgs::Header header_; 
		
		sensor_msgs::PointCloud2 pc2_elev_map_, pc2_conv_hull_elev_map_2D_;
		std::vector<Point2D> pc_conv_hull_2d_elev_map_vect_; 
		
		Real dist_max_map_in_cells_; // [cell]
		
		int num_bytes_each_pc2_; 
		
		Eigen::Matrix<Real,4,4> mat_bspline_terrain_, mat_transpose_bspline_terrain_; 		
				
		bool is_map_static_global_; 
						
	public:
	
		int num_total_obstacles_; 
		std::vector<std::vector<Eigen::Matrix<Real,3,1> > > obstacle_; // an array of obstacles with each obstacle element having the coordinates of its contouring points (sorted counterclockwisely). 		
	
		Eigen::Matrix<Real,Eigen::Dynamic,Eigen::Dynamic> X_, Y_, Z_, slope_terrain_; // I made these matrices be public for debugging purpose.
		
		Eigen::Matrix<int,Eigen::Dynamic,Eigen::Dynamic> map_label_terrain_; 		
		
		int num_samples_x_, num_samples_y_;// I made these matrices be public for debugging purpose.
		Real xmin_, xmax_, ymin_, ymax_;// I made these matrices be public for debugging purpose.
		Real res_cell_x_, res_cell_y_;
		
		int ind_label_region_startConfig_belongsTo_; 
		
		int num_segmented_planes_; 
		std::vector<std::vector<Eigen::Matrix<Real,3,1> > > coords_convex_hulls_segmented_planes_; 		
		
		Terrain(); 		
		Terrain(const bool is_map_static_global); 
		~Terrain(); 
				
		void updateElevMap(const fraudo_msgs::SegmentedScene::ConstPtr& msg_scene_segmented); 
		
		int sub2ind(int sub_x, int sub_y) const;
		void ind2sub(int ind, int* sub_x, int* sub_y) const;
		Eigen::Matrix<int,2,1> ind2sub(int ind) const;
				
		void extendTerrainMap();

		void defineTerrainConvHull();
		
		bool isRobotInsideTerrainConvHull(const Point2D pt) const;
		
		void setTimeStamp(const ros::Time time_new);
		ros::Time getTimeStamp();
		
		Eigen::Matrix<Real,NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT,1> getRobotStartPose();
		
		fraudo_msgs::SegmentedScene convertElevMap2SegmentedScene(const fraudo_msgs::ElevationMap::ConstPtr& msg_elev_map);
		
		sensor_msgs::PointCloud2& getPC2ElevMap(); 
		sensor_msgs::PointCloud2& getPC2ConvHullElevMap2D(); 
		
		// for both synthetic and real maps		
		void updateElevMap(const sensor_msgs::PointCloud2& pc2_elev_map);		
		void setRobotStartPose(const geometry_msgs::PoseStamped& poseStamped_start); 
	
		Real estimateTerrainHeight(Real x_in, Real y_in);
		
		void segmentTerrainIntoPlanes();		
		
		void setNumSamplesX(int num_samples_x) { num_samples_x_ = num_samples_x; }
		void setNumSamplesY(int num_samples_y) { num_samples_y_ = num_samples_y; }
		void setResCellX(Real res_cell_x) { res_cell_x_ = res_cell_x; }
		void setResCellY(Real res_cell_y) { res_cell_y_ = res_cell_y; }
		void setXmin(Real xmin) { xmin_ = xmin; }
		void setXmax(Real xmax) { xmax_ = xmax; }
		void setYmin(Real ymin) { ymin_ = ymin; }
		void setYmax(Real ymax) { ymax_ = ymax; }
		
		bool getIsMapStaticGlobal(){return is_map_static_global_;}
		
	private:
		void setBasicPC2Channels(sensor_msgs::PointCloud2 &pc2_out); // without label
		void setPC2Channels(sensor_msgs::PointCloud2 &pc2_out); // with label
		
}; 

// } // end of the namespace fraudo
	
#endif
