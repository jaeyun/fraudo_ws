/**
 * @file planner.hpp
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         July 11 2013; 
 * Lastly modified: April 21 2015; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2015>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#ifndef PLANNER_H_
#define PLANNER_H_

#include <fraudo_motion_planning/graph.hpp>

// namespace fraudo{

const Real g_size_node_display = 0.1; 

template<class TConfig> class Planner; 

///  
///   The class trajectory defines a time-dependent function obtained from the path searched by using a planner. 
///
template<class TConfig> class Trajectory{
	
	friend class Planner<TConfig>;
				
	public: 

		int num_data_; 
	
		std::vector<Eigen::Matrix<Real,10,1> > data_; // x_,y_,z_,roll_,pitch_,yaw_,ang_flipper_,vel_linear_body_,vel_ang_body_,time_
				
		Trajectory(){
			num_data_ = 0; 
		}				
			 
		Trajectory(int num_samples): num_data_(num_samples){} 
		~Trajectory(){} 		
};


/** 
 *  The class Planner defines the planner used to search for an optimal path for the traversal of the Cameleon over uneven terrains. This class serves as the base for various specific planners such as the class PlannerInitial and PlannerRRTStar. 
 */
template<class TConfig> class Planner
{		
	protected:	
		
		Terrain* map_elev_curr_;
		Terrain* map_elev_last_; // for a sequence of real segmented real local maps.
		Robot* robot_; 
		Robot* robot_goal_;
				
		bool is_path_optimal_found_;
		bool is_map_static_global_; 
		
		std::vector<TrajectoryExtended> trajectory_; 
		
		ros::NodeHandle n_; 
				
		int num_samples_traj_;
		
		int sub_node_init_x_, sub_node_init_y_;
		int sub_node_goal_x_, sub_node_goal_y_, ind_node_goal_; 		
		TConfig config_start_, config_goal_; 

		
		Eigen::Matrix<Real,4,NUM_TOTAL_CONTACT> boundary_terrain_domain_; // xmin, xmax, ymin, ymax
		
		ros::Publisher pub_traj_; 		
		ros::Publisher pub_marker_startAndGoal_; 
		
		ros::Publisher pub_marker_point3d_; 			
		ros::Publisher pub_marker_line_; 			
		ros::Publisher pub_marker_obstacle_contours_; 			
		ros::Publisher pub_marker_line_gradient_;
		ros::Publisher pub_marker_config_solution_; 
		ros::Publisher pub_marker_config_smoothed_solution_; 

		
		int ind_switch_traj_prev_; // index of the previous trajectory from which the previous trajectory is going to be replaced by the new one. ( Assigned in PlannerRRTStar::findNewRobotInitPose() and used in Planner::publishTrajectoryMsg() ).
		int num_traj_; 
		
		int num_traj_interpolation_pts_; 
		
		int num_config_dimensions_; // dimensionality of the configuration space.
		int num_sample_dimensions_; // dimension of the sampling space. I've distinguished the sampling space from the configuration space since in the current problem I only sample along the x- and y- directions while the robot configuration has seven dimensions including the flippers' angle. 
		
		Real size_step_cspace_discretization_; // the step size for the discretization of the configuration space. 
		
		std::vector<int> ind_sample_dims_in_config_dims_; // this array indicates the indices of the configuration dimensions that are sampled. 		
		Eigen::Matrix<Real,Eigen::Dynamic,2> bounds_sample_dims_; // first column: lower bound; second column: upper bound
		
		bool do_collision_check_; 
		ROBOT_MODEL robot_model_; // enum with two possible values: POINT_MASS, RIGID_BODY
						
		
		std::vector<TConfig> config_solution_;
		std::vector<TConfig> config_smoothed_solution_; 
		
		
		// for collisionOccurredBW2DGenericPolygonsAndPoint()
		std::vector<std::vector<Eigen::Matrix<Real,3,1> > > gradient_edge_obstacle_; 
		std::vector<std::vector<Eigen::Matrix<Real,3,1> > > midPoint_edge_obstacle_; 
		std::vector<Real> discriminant_edge_obstacle_; 
		std::vector<Eigen::Matrix<Real,3,1> > diff_bw_config_and_midPointEdgeObstacle_;
		
		int ind_region_startConfig_belongsTo_; // index of the classified region to which the start configuration belongs. 
				
		// for measureTipOverStability()
		std::vector<Point> pt_convex_hull_vect_; 		
		
		int num_total_obstacles_contouring_pts_; // sum of all the contouring points of all the obstacles present in the map. 		
		int max_size_obstacle_countering_pts_; 
		
		bool is_robot_motion_forward_; // I need this variable to make a decision whether the connection b/w two nodes is possible, especially for detecting if the robot tries to climb a stair backward. 
		
		PLANNER_ID id_planner_; 
							
		void reserveMemoryForVectorsInvolvedInCollisionChecking(const std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& obstacle); // reserve the memory for the vectors involved in collision checking ahead in order to reduce the execution time (based on the results obtained from various profilers).
						
		Real measureTipOverStability(void); 
		Real measureTipOverStability(const Robot& robot); 
		Real searchOptimalFlipperAngle(void); 
		void saveGeneratedTrajectory(const char* name_file) const; 
		void printTrajectory(const TrajectoryExtended& trajectory) const; 
				
		std::pair<Eigen::Matrix<Real,Eigen::Dynamic,1>, Real> getStateFromTrajectory(int ind_traj); 		
		
		Real (*distanceFunction_)(const TConfig&, const TConfig&); // this function returns the distance b/w two configurations. Its definition comes defined from its caller. 
		
		void defineElevMapBounds(Terrain* map_elev_curr); 
		
		int evaluateSampleAgainstSampleBounds(TConfig& config); 
		int interpolateConfigLinearly( const TConfig& config_start, const TConfig& config_end, const Real alpha, TConfig& config_interpolated); 		
		int interpolateSampleLinearly( const TConfig& config_start, const TConfig& config_end, const Real alpha, TConfig& config_interpolated); 
		
		// I am returning the squared distance and not the actual distance to optimize the execution time (since the sqrt() operation takes excessive time).
		Real distanceBWLineSegmentAndPoint(Eigen::Matrix<Real,3,1> pt0_segment, Eigen::Matrix<Real,3,1> pt1_segment, Eigen::Matrix<Real,3,1> pt);
		
		int smoothPath(int num_iterations_for_smooting_path, std::vector<TConfig>& configs_path_optimal, std::vector<TConfig>& config_smoothed_solution); 
						
		int isTraversableBWTwoConfigs(TConfig& config0, TConfig& config1, CONFIG_CONNECTION_TYPE config_connection_type, GRAPH_TYPE type_tree); // config1 is not made constant b/c its yaw angle might change if robot backward motion is chosen.		
		
		void displayStartAndGoalConfigs(); 		
		void displayPoint3D(std::vector<Point3D>& vect_point3d);
		void displayPoint3D(TConfig& config);
		void displayLines(std::vector<Eigen::Matrix<Real,3,1> >& vect_point3d); 
		void displayObstacleContours(); 
		void displayLineGradients(std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& vect_point3d_in, std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& vect_gradient_in);		
		void displayConfigSolution();
		void displayConfigSmoothedSolution();
		
		void publishTrajectoryMsg(Trajectory<TConfig> & traj, int id_trajectory); // had to include id_trajectory as an argument because this counter is not shared b/w PlannerInitial and PlannerRRTStar
		
		void generateSmoothTrajectoryFromPath(Trajectory<TConfig> & traj); 
		void splineDeBoor(Trajectory<TConfig>& traj); 
				
		
		bool collisionOccurredAlongTwoConfigs(const TConfig& config0, const TConfig& config1, TConfig& config_lastly_valid); 
		bool collisionOccurredBWTwoConfigsWithRigidBodyRobot(const TConfig& config0, const TConfig& config1, YAW_MOTION_START_TYPE yaw_motion_config0, YAW_MOTION_END_TYPE yaw_motion_config1); // config1 is not made constant b/c its yaw angle might change if robot backward motion is chosen.
		bool collisionOccurredBW2DGenericPolygonsAndPoint(const std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& obstacle, const TConfig& config);
		
		bool collisionOccurredBasedOnTerrainLabeling(const TConfig& config); 
		
		int updateHorizontalRobotConfig(const TConfig& config, Robot* robot); // updated the member variable robot_ with its contact points updated. 
		int updateFullRobotConfig(const TConfig& config, Robot* robot); // fully update the robot's configuration 
		int updateFullRobotConfigFromItsHorionzontalConfig(const TConfig& config, Robot* robot); // fully update the robot's configuration 
		
		int getNumSamplesTraj(void);
		
		void setRobotStartPose(const geometry_msgs::PoseStamped& poseStamped_start); 
		void setRobotGoalPose(const geometry_msgs::PoseStamped& poseStamped_goal); 
		
		Eigen::Matrix<Real,Eigen::Dynamic,1> getRobotStartPose();
		Eigen::Matrix<Real,Eigen::Dynamic,1> getRobotGoalPose();
		
		void planFlipperMotionFromPitchPath(std::vector<TConfig>& config_solution);	
		int planFlipperMotionBasedOnPitchMotionOnly(std::vector<TConfig>& config_solution); 
		
		int identifyRegionContainingStartConfig(const std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& obstacle, const TConfig& config);
		
	public:	
		
		Planner();		
		Planner(Robot* robot, Robot* robot_goal, bool is_map_synthetic, Real size_step_cspace_discretization, Real (*distanceFunction)(const TConfig&, const TConfig&)=NULL);
		~Planner(); 
		
		virtual int generateTrajectory(Terrain* map_elev_curr, const geometry_msgs::PoseStamped& poseStamped_start, const geometry_msgs::PoseStamped& poseStamped_goal, const SimulationParamInput& param_input_simulation) = 0;	// pure virtual
		virtual int studyInfluencePtsContactTimeSampling(Terrain* map_elev_curr, const geometry_msgs::PoseStamped& poseStamped_start, const geometry_msgs::PoseStamped& poseStamped_goal, const SimulationParamInput param_input_simulation) = 0; // pure virtual
				
		void setIndSampleDimsInConfigDims(std::vector<int> ind_sample_dims_in_config_dims);  		
		void setBoundsSampleDims(Eigen::Matrix<Real,Eigen::Dynamic,2> bounds_sample_dims); // first column: lower bound; second column: upper bound
};


/**
* The basic constructor for the class Planner.
*/
template<class TConfig> Planner<TConfig>::Planner()
{
	pub_marker_startAndGoal_ = n_.advertise<visualization_msgs::Marker>("start_and_goal_markers",1); 
	pub_marker_point3d_ = n_.advertise<visualization_msgs::Marker>("points3d_markers",1); 
	pub_marker_line_ = n_.advertise<visualization_msgs::Marker>("line_markers",1); 
	pub_marker_obstacle_contours_ = n_.advertise<visualization_msgs::Marker>("obstacle_contour_markers",1); 
	pub_marker_line_gradient_ = n_.advertise<visualization_msgs::Marker>("line_gradient_markers",1); 
	pub_marker_config_solution_ = n_.advertise<visualization_msgs::Marker>("config_solution_markers",1); 
	pub_marker_config_smoothed_solution_ = n_.advertise<visualization_msgs::Marker>("config_smoothed_solution_markers",1); 
	pub_traj_ = n_.advertise<fraudo_msgs::Trajectory>("fraudo_trajectory",1);
	
	 
	num_traj_ = 0; 
	ind_switch_traj_prev_ = -1; // default value to indicate that no index of the new start node in the previous trajectory has been found 
	
	// just to get its numbers of configuration dimensions and sample dimensions.
	TConfig config; 
		
	num_config_dimensions_ = config.getNumConfigDimensions(); // x, y, z, roll, pitch, yaw and ang_flipper. Only x and y will be sampled. The yaw angle of the new configuration will depend on the location of the reference configuration (the mincost near node). The flipper angle will be assumed to be such that it makes contact with the flat surface. And, this will be changed later if either collision occurs with the virtual flippers or large amount of pitch motion occurs.  (z, roll, pitch) values will be assigned later when the corresponding pose will be evaluated. This will be done while the associated cost is estimated. 
	num_sample_dimensions_ = config.getNumSampleDimensions(); // only along the x- and y- directions are sampled
		
	num_traj_interpolation_pts_ = 0; 
	
	do_collision_check_ = 1; 
	robot_model_ = RIGID_BODY; // enum with two possible values: POINT_MASS, RIGID_BODY
	
	// for measureTipOverStability()
	pt_convex_hull_vect_.resize(5); // as one can see in the definition of the function measureTipOverStability(), the maximum number of pt_convex_hull_vect_'s elements is up to 5. 
		
	is_robot_motion_forward_ = true; 
}

/**
 * The constructor for the class Planner with the possibility to initialize the member variables such as the associated class object Robot and is_map_static_global. 
*/
template<class TConfig> Planner<TConfig>::Planner(Robot* robot, Robot* robot_goal, bool is_map_static_global, Real size_step_cspace_discretization, Real (*distanceFunction)(const TConfig&, const TConfig&)) 
	: robot_(robot), is_map_static_global_(is_map_static_global), size_step_cspace_discretization_(size_step_cspace_discretization), robot_goal_(robot_goal)
{	
	TConfig config; 
			
	num_config_dimensions_ = config.getNumConfigDimensions(); // x, y, z, roll, pitch, yaw and ang_flipper. Only x and y will be sampled. The yaw angle of the new configuration will depend on the location of the reference configuration (the mincost near node). The flipper angle will be assumed to be such that it makes contact with the flat surface. And, this will be changed later if either collision occurs with the virtual flippers or large amount of pitch motion occurs.  (z, roll, pitch) values will be assigned later when the corresponding pose will be evaluated. This will be done while the associated cost is estimated. 
	num_sample_dimensions_ = config.getNumSampleDimensions(); // only along the x- and y- directions are sampled
	
	for(int i=0; i<num_config_dimensions_; ++i){
		config_goal_.q_(i) = NAN; 
	} 

	pub_marker_startAndGoal_ = n_.advertise<visualization_msgs::Marker>("start_and_goal_markers",1); 	
	pub_marker_point3d_ = n_.advertise<visualization_msgs::Marker>("points3d_markers",1); 
	pub_marker_line_ = n_.advertise<visualization_msgs::Marker>("line_markers",1); 
	pub_marker_obstacle_contours_ = n_.advertise<visualization_msgs::Marker>("obstacle_contour_markers",1); 
	pub_marker_line_gradient_ = n_.advertise<visualization_msgs::Marker>("line_gradient_markers",1); 
	pub_marker_config_solution_ = n_.advertise<visualization_msgs::Marker>("config_solution_markers",1); 
	pub_marker_config_smoothed_solution_ = n_.advertise<visualization_msgs::Marker>("config_smoothed_solution_markers",1); 
	pub_traj_ = n_.advertise<fraudo_msgs::Trajectory>("fraudo_trajectory",1); 
	
	num_traj_ = 0; 
	ind_switch_traj_prev_ = -1;  // default value to indicate that no index of the new start node in the previous trajectory has been found 	
		
	num_traj_interpolation_pts_ = 0; 
	
	do_collision_check_ = 1; 	
	robot_model_ = RIGID_BODY; // enum with two possible values: POINT_MASS, RIGID_BODY	
	
	if (distanceFunction != NULL){
		distanceFunction_ = distanceFunction; 
	}else{
		ROS_INFO_STREAM("Planner.hpp:: Distance function is not defined."); 
		distanceFunction = NULL;
	}
	
	// for measureTipOverStability()
	pt_convex_hull_vect_.resize(5); // as one can see in the definition of the function measureTipOverStability(), the maximum number of pt_convex_hull_vect_'s elements is up to 5. 
	
	is_robot_motion_forward_ = true;
}


template<class TConfig> Planner<TConfig>::~Planner()
{
	
}

/**
 * This measureTipOverStability function() measures the tip-over stability of the robot for a given pose.
*/
template<class TConfig> Real Planner<TConfig>::measureTipOverStability()
{
	Real tip_over_stability_measure, tip_over_stability_measure_normalized; 
	int i;
		
	int num_half_total_contact = NUM_TOTAL_CONTACT/2;
	int min_left=num_half_total_contact-1, min_right = NUM_TOTAL_CONTACT-1; 
	int max_left=0, max_right = num_half_total_contact; 
	for( i=0 ; i<NUM_TOTAL_CONTACT ; ++i){
		// To discern if a point is part of the construction of the convex hull
		// make sure that is_contact_active_ is updated properly. This is updated in the function robot_->estimatePose() which is done inside update_list_open_and_list_closed()
		if ( i < num_half_total_contact ){
			if ( i < min_left )
				min_left = i; 
			if	( i > max_left )
				max_left = i;
		}else{
			if ( i < min_right )
				min_right = i; 
			if ( i > max_right )
				max_right = i; 
		}
	}	
				
	int size_convex_hull = 0; 
	// clock-wisely oriented convex hull 
	if (min_left != max_left){ // two points on the left side	
		pt_convex_hull_vect_[size_convex_hull] = Point(robot_->pt_robot_potential_contact_global_frame_(0,min_left),
							 						   robot_->pt_robot_potential_contact_global_frame_(1,min_left),
						 							   robot_->pt_robot_potential_contact_global_frame_(2,min_left),
					 								   min_left); 
		 
		pt_convex_hull_vect_[size_convex_hull+1] = Point(robot_->pt_robot_potential_contact_global_frame_(0,max_left),
														 robot_->pt_robot_potential_contact_global_frame_(1,max_left),
														 robot_->pt_robot_potential_contact_global_frame_(2,max_left),
														 max_left);
		size_convex_hull += 2; 
	}else{
		pt_convex_hull_vect_[size_convex_hull] = Point(robot_->pt_robot_potential_contact_global_frame_(0,min_left),
													   robot_->pt_robot_potential_contact_global_frame_(1,min_left),
													   robot_->pt_robot_potential_contact_global_frame_(2,min_left),
													   min_left); 
		size_convex_hull++; 
	}

	if (min_right != max_right){ // two points on the right side
		pt_convex_hull_vect_[size_convex_hull] = Point(robot_->pt_robot_potential_contact_global_frame_(0,min_right),
													   robot_->pt_robot_potential_contact_global_frame_(1,min_right),
													   robot_->pt_robot_potential_contact_global_frame_(2,min_right),
													   min_right); 
		pt_convex_hull_vect_[size_convex_hull+1] = Point(robot_->pt_robot_potential_contact_global_frame_(0,max_right),
														 robot_->pt_robot_potential_contact_global_frame_(1,max_right),
														 robot_->pt_robot_potential_contact_global_frame_(2,max_right),
														 max_right);
		size_convex_hull += 2;
	}else{
		pt_convex_hull_vect_[size_convex_hull] = Point(robot_->pt_robot_potential_contact_global_frame_(0,min_right),
													   robot_->pt_robot_potential_contact_global_frame_(1,min_right),
													   robot_->pt_robot_potential_contact_global_frame_(2,min_right),
													   min_right); 
		size_convex_hull++;
	}

	// Repeat the first point to close the loop
	pt_convex_hull_vect_[size_convex_hull] = Point(robot_->pt_robot_potential_contact_global_frame_(0,min_left),
											       robot_->pt_robot_potential_contact_global_frame_(1,min_left),
												   robot_->pt_robot_potential_contact_global_frame_(2,min_left),
												   min_left);
	size_convex_hull++;

	Eigen::Matrix<Real,3,5> pt_convex_hull; // maximum number of size_convex_hull can be up to 5. I tried to avoid to use Eigen::Dynamic in order to make to code execution be more efficient.  
	for(int i=0; i<size_convex_hull; ++i){
		pt_convex_hull.col(i) << pt_convex_hull_vect_[i].x, pt_convex_hull_vect_[i].y, pt_convex_hull_vect_[i].z; 		
	}
	
	// I tried to avoid to use Eigen::Dynamic in order to make to code execution be more efficient. 
	Eigen::Matrix<Real,3,4> a; 
	Eigen::Matrix<Real,3,4> a_hat;
	Eigen::Matrix<Real,3,4> b;
	Eigen::Matrix<Real,3,4> c;
	Eigen::Matrix<Real,3,4> l;
	Eigen::Matrix<Real,3,4> l_hat;

	for(i=0 ; i<size_convex_hull-1 ; ++i){
		a.col(i) = pt_convex_hull.col(i+1) - pt_convex_hull.col(i); 
		a_hat.col(i) = (a.col(i)).normalized(); 
		
		b.col(i) = pt_convex_hull.col(i+1) - (robot_->pose_).topRows(3); // robot_->com_body_global_frame == robot_->pose_
		
		c.col(i) = (a_hat.col(i) * a_hat.col(i).transpose()) * b.col(i);		
		l.col(i) = b.col(i) - c.col(i); 
		
		// squaredNorm() is used instead of norm() for efficiency. norm() involves sqrt() which is not efficient. 
		if (fabs((l.col(i)).norm()) < 1e-7){ // taking into account possible numerical error
			std::cout << "Something wrong with the vector normal to a tip-over axis" << std::endl;
			exit(-1); 
		}
		
		l_hat.col(i) = (l.col(i)).normalized(); // (l.col(i)).normalize(); 
	}
	
	Eigen::Matrix<Real,3,1> force_net, moment_net; 
	
	// NOTE: At this time I am going to assume that the only force is the gravitational force. 
	force_net << 0.0, 0.0, -ACCEL_GRAVITY * ROBOT_BODY_MASS; // mass corresponding to body only and located at the body CoM. 
	
	// NOTE: At this time I am NOT going to consider the moment.
	moment_net.setZero(); 
	
	Real my_sigma; 
	
	// I tried to avoid to use Eigen::Dynamic in order to make to code execution be more efficient. 4 is the maximum number of possible convex hull points. 
	Eigen::Matrix<Real,3,4> force_tip_over, moment_tip_over, force_couple_tip_over, force_tip_over_total, force_tip_over_total_hat; 		
	Eigen::Matrix<Real,4,1> theta, alpha; 
	
	Eigen::Matrix3f mat_identity;
	mat_identity.setIdentity();  
	for(i=0 ; i<size_convex_hull-1 ; ++i){
		force_tip_over.col(i) = (mat_identity - ((a_hat.col(i)) * (a_hat.col(i)).transpose())) * force_net; 
		moment_tip_over.col(i) = (a_hat.col(i) * a_hat.col(i).transpose()) * moment_net; 
		force_couple_tip_over.col(i) = ((l_hat.col(i)).cross(moment_tip_over.col(i))) / l.col(i).norm();
		force_tip_over_total.col(i) = force_tip_over.col(i) + force_couple_tip_over.col(i); 
		force_tip_over_total_hat.col(i) = (force_tip_over_total.col(i)).normalized(); 
		if ( ((l_hat.col(i)).cross(force_tip_over_total_hat.col(i))).dot(a_hat.col(i)) < 0 )
			my_sigma = 1.0; 
		else 
			my_sigma = -1.0; 
		
		theta(i) = my_sigma * acos( (force_tip_over_total_hat.col(i)).dot(l_hat.col(i)) ); 
		alpha(i) = theta(i) * force_net.norm();		
	}
	
	tip_over_stability_measure = (theta.head(size_convex_hull-1)).minCoeff() * force_net.norm(); 
	tip_over_stability_measure_normalized = tip_over_stability_measure / TIP_OVER_STABILITY_MEASURE_MAXIMUM; 
	
	if( tip_over_stability_measure_normalized > 1.0 ) tip_over_stability_measure_normalized = 1.0; 
	else if( tip_over_stability_measure_normalized < -1.0 ) tip_over_stability_measure_normalized = -1.0; 
			
	return tip_over_stability_measure_normalized; 	
}



/**
 * This measureTipOverStability function() measures the tip-over stability of the robot for a given pose.
 * BE CAREFUL! 
 * This function returns the raw stability measure, that is, not the normalized value, nor saturated value. 
 * This is different from the overriden function without any argument. 
*/
template<class TConfig> Real Planner<TConfig>::measureTipOverStability(const Robot& robot)
{
	Real tip_over_stability_measure, tip_over_stability_measure_normalized; 
	int i;
		
	int num_half_total_contact = NUM_TOTAL_CONTACT/2;
	int min_left=num_half_total_contact-1, min_right = NUM_TOTAL_CONTACT-1; 
	int max_left=0, max_right = num_half_total_contact; 
	for( i=0 ; i<NUM_TOTAL_CONTACT ; ++i){
		// To discern if a point is part of the construction of the convex hull
		// make sure that is_contact_active_ is updated properly. This is updated in the function robot.estimatePose() which is done inside update_list_open_and_list_closed()
		if ( i < num_half_total_contact ){
			if ( i < min_left )
				min_left = i; 
			if	( i > max_left )
				max_left = i;
		}else{
			if ( i < min_right )
				min_right = i; 
			if ( i > max_right )
				max_right = i; 
		}
	}	
			
	
	int size_convex_hull = 0; 
	// clock-wisely oriented convex hull 
	if (min_left != max_left){ // two points on the left side	
		pt_convex_hull_vect_[size_convex_hull] = Point(robot.pt_robot_potential_contact_global_frame_(0,min_left),
							 						   robot.pt_robot_potential_contact_global_frame_(1,min_left),
						 							   robot.pt_robot_potential_contact_global_frame_(2,min_left),
					 								   min_left); 
		 
		pt_convex_hull_vect_[size_convex_hull+1] = Point(robot.pt_robot_potential_contact_global_frame_(0,max_left),
														 robot.pt_robot_potential_contact_global_frame_(1,max_left),
														 robot.pt_robot_potential_contact_global_frame_(2,max_left),
														 max_left);
		size_convex_hull += 2; 
	}else{
		pt_convex_hull_vect_[size_convex_hull] = Point(robot.pt_robot_potential_contact_global_frame_(0,min_left),
													   robot.pt_robot_potential_contact_global_frame_(1,min_left),
													   robot.pt_robot_potential_contact_global_frame_(2,min_left),
													   min_left); 
		size_convex_hull++; 
	}

	if (min_right != max_right){ // two points on the right side
		pt_convex_hull_vect_[size_convex_hull] = Point(robot.pt_robot_potential_contact_global_frame_(0,min_right),
													   robot.pt_robot_potential_contact_global_frame_(1,min_right),
													   robot.pt_robot_potential_contact_global_frame_(2,min_right),
													   min_right); 
		pt_convex_hull_vect_[size_convex_hull+1] = Point(robot.pt_robot_potential_contact_global_frame_(0,max_right),
														 robot.pt_robot_potential_contact_global_frame_(1,max_right),
														 robot.pt_robot_potential_contact_global_frame_(2,max_right),
														 max_right);
		size_convex_hull += 2;
	}else{
		pt_convex_hull_vect_[size_convex_hull] = Point(robot.pt_robot_potential_contact_global_frame_(0,min_right),
													   robot.pt_robot_potential_contact_global_frame_(1,min_right),
													   robot.pt_robot_potential_contact_global_frame_(2,min_right),
													   min_right); 
		size_convex_hull++;
	}

	// Repeat the first point to close the loop
	pt_convex_hull_vect_[size_convex_hull] = Point(robot.pt_robot_potential_contact_global_frame_(0,min_left),
											       robot.pt_robot_potential_contact_global_frame_(1,min_left),
												   robot.pt_robot_potential_contact_global_frame_(2,min_left),
												   min_left);
	size_convex_hull++;
	
	Eigen::Matrix<Real,3,5> pt_convex_hull; // maximum number of size_convex_hull can be up to 5. I tried to avoid to use Eigen::Dynamic in order to make to code execution be more efficient.  
	for(int i=0; i<size_convex_hull; ++i){
		pt_convex_hull.col(i) << pt_convex_hull_vect_[i].x, pt_convex_hull_vect_[i].y, pt_convex_hull_vect_[i].z; 		
	}
	
	// I tried to avoid to use Eigen::Dynamic in order to make to code execution be more efficient. 
	Eigen::Matrix<Real,3,4> a; 
	Eigen::Matrix<Real,3,4> a_hat;
	Eigen::Matrix<Real,3,4> b;
	Eigen::Matrix<Real,3,4> c;
	Eigen::Matrix<Real,3,4> l;
	Eigen::Matrix<Real,3,4> l_hat;

	for(i=0 ; i<size_convex_hull-1 ; ++i){
		a.col(i) = pt_convex_hull.col(i+1) - pt_convex_hull.col(i); 
		a_hat.col(i) = (a.col(i)).normalized(); 
		b.col(i) = pt_convex_hull.col(i+1) - (robot.pose_).topRows(3); // robot.com_body_global_frame == robot.pose_
		
		c.col(i) = (a_hat.col(i) * a_hat.col(i).transpose()) * b.col(i);		
		l.col(i) = b.col(i) - c.col(i); 
		
		// squaredNorm() is used instead of norm() for efficiency. norm() involves sqrt() which is not efficient. 
		if (fabs((l.col(i)).norm()) < 1e-7){ // taking into account possible numerical error
			std::cout << "Something wrong with the vector normal to a tip-over axis" << std::endl;
			exit(-1); 
		}
		
		l_hat.col(i) = (l.col(i)).normalized(); // (l.col(i)).normalize(); 
	}
	
	Eigen::Matrix<Real,3,1> force_net, moment_net; 
	
	// NOTE: At this time I am going to assume that the only force is the gravitational force. 
	force_net << 0.0, 0.0, -ACCEL_GRAVITY * ROBOT_BODY_MASS; // mass corresponding to body only and located at the body CoM. 
	
	// NOTE: At this time I am NOT going to consider the moment.
	moment_net.setZero(); 
	
	Real my_sigma; 
	
	// I tried to avoid to use Eigen::Dynamic in order to make to code execution be more efficient. 4 is the maximum number of possible convex hull points. 
	Eigen::Matrix<Real,3,4> force_tip_over, moment_tip_over, force_couple_tip_over, force_tip_over_total, force_tip_over_total_hat; 		
	Eigen::Matrix<Real,4,1> theta, alpha; 
	
	Eigen::Matrix3f mat_identity;
	mat_identity.setIdentity();  
	for(i=0 ; i<size_convex_hull-1 ; ++i){
		force_tip_over.col(i) = (mat_identity - ((a_hat.col(i)) * (a_hat.col(i)).transpose())) * force_net; 
		moment_tip_over.col(i) = (a_hat.col(i) * a_hat.col(i).transpose()) * moment_net; 
		force_couple_tip_over.col(i) = ((l_hat.col(i)).cross(moment_tip_over.col(i))) / l.col(i).norm();
		force_tip_over_total.col(i) = force_tip_over.col(i) + force_couple_tip_over.col(i); 
		force_tip_over_total_hat.col(i) = (force_tip_over_total.col(i)).normalized(); 
		if ( ((l_hat.col(i)).cross(force_tip_over_total_hat.col(i))).dot(a_hat.col(i)) < 0 )
			my_sigma = 1.0; 
		else 
			my_sigma = -1.0; 
		
		theta(i) = my_sigma * acos( (force_tip_over_total_hat.col(i)).dot(l_hat.col(i)) ); 
		alpha(i) = theta(i) * force_net.norm();		
	}
	
	tip_over_stability_measure = (theta.head(size_convex_hull-1)).minCoeff() * force_net.norm(); 
	tip_over_stability_measure_normalized = tip_over_stability_measure / TIP_OVER_STABILITY_MEASURE_MAXIMUM; 
			
	return tip_over_stability_measure; 	
}


/**
 * This searchOptimalFlipperAngle function() computes an optimal flipper angle .
 * Requires: robot_->pose_, robot_->ang_flipper_, and terrain
 * Returns: the optimal flipper angle that improves the tip-over stability. 
*/
template<class TConfig> Real Planner<TConfig>::searchOptimalFlipperAngle()
{
	Real ang_flipper_curr = robot_->pose_(6); // now on the ang_flipper_ is merged into pose_ with index 6.
	Real stab_pos_perturb = NAN, stab_neg_perturb = NAN, grad_tipover_stab = NAN; 
	
	bool is_optimal_ang_flipper_found = false; 
	
	int count = 0; 
			
	Real perturbation = 1e-3; 
		
	do{	
		// For positive perturbation to the flipper angle
		robot_->pose_.segment(2,3) << POSE_ROBOT_INIT_Z, POSE_ROBOT_INIT_ROLL, POSE_ROBOT_INIT_PITCH; // recover the default z, roll, and pitch values. x,y,yaw are fixed, hence do not change these values.
		robot_->pose_(6) = ang_flipper_curr + perturbation; // update the flipper's angle, with about 0.573 deg of perturbation // now on the ang_flipper_ is merged into pose_ with index 6.
		robot_->updatePoseRobot(); // Requires: pose_ and ang_flipper_. Updates: body_.pt_contact_global_frame, pt_robot_potential_contact_global_frame_ and pt_robot_potential_contact_body_frame_
		
		// since only the flippers' contact points are changed, I only need to make sure that these points are in the domain. But, I do check for all points b/c it is easier doing in this way. 
		if( (robot_->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(0).array()).any() && 
			(robot_->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(1).array()).any() && 
			(robot_->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(2).array()).any() && 
			(robot_->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(3).array()).any() ){
		
			robot_->findPtsTerrainContact(); // to find out the height of the terrain corresponding to all of the potential contact points.
			
			Eigen::Matrix<Real,1,NUM_TOTAL_CONTACT> height_potential_contact_pts = robot_->pt_robot_potential_contact_global_frame_.bottomRows(1) - robot_->pt_terrain_contact_.bottomRows(1); 
			Real height_lowest_potential_contact_pt = height_potential_contact_pts.minCoeff();
			robot_->pose_(2) = robot_->pose_(2) - height_lowest_potential_contact_pt; 
						
			robot_->updatePoseRobot(); // Requires: pose_ and ang_flipper_. Updates: body_.pt_contact_global_frame, pt_robot_potential_contact_global_frame_ and pt_robot_potential_contact_body_frame_
			robot_->estimatePose(); // updated the pose of the robot. 
						
			if( (robot_->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(0).array()).any() && 
				(robot_->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(1).array()).any() && 
				(robot_->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(2).array()).any() && 
				(robot_->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(3).array()).any() ){
			
				stab_pos_perturb = measureTipOverStability(); 
			}else{
				break; 
			}
		}else{
			break;
		}
		
		// For negative perturbation to the flipper angle
		robot_->pose_.segment(2,3) << POSE_ROBOT_INIT_Z, POSE_ROBOT_INIT_ROLL, POSE_ROBOT_INIT_PITCH; // recover the default z, roll, and pitch values. x,y,yaw are fixed, hence do not change these values.
		robot_->pose_(6) = ang_flipper_curr - perturbation; // update the flipper's angle, with about 0.573 deg of perturbation // now on the ang_flipper_ is merged into pose_ with index 6.
		robot_->updatePoseRobot(); // Requires: pose_ and ang_flipper_. Updates: body_.pt_contact_global_frame, pt_robot_potential_contact_global_frame_ and pt_robot_potential_contact_body_frame_
		
		// since only the flippers' contact points are changed, I only need to make sure that these points are in the domain. But, I do check for all points b/c it is easier doing in this way. 
		if( (robot_->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(0).array()).any() && 
			(robot_->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(1).array()).any() && 
			(robot_->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(2).array()).any() && 
			(robot_->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(3).array()).any() ){
		
			robot_->findPtsTerrainContact(); // to find out the height of the terrain corresponding to all of the potential contact points.
			
			Eigen::Matrix<Real,1,NUM_TOTAL_CONTACT> height_potential_contact_pts = robot_->pt_robot_potential_contact_global_frame_.bottomRows(1) - robot_->pt_terrain_contact_.bottomRows(1); 
			Real height_lowest_potential_contact_pt = height_potential_contact_pts.minCoeff();
			robot_->pose_(2) = robot_->pose_(2) - height_lowest_potential_contact_pt; 			
			
			robot_->updatePoseRobot(); // Requires: pose_ and ang_flipper_. Updates: body_.pt_contact_global_frame, pt_robot_potential_contact_global_frame_ and pt_robot_potential_contact_body_frame_
			robot_->estimatePose(); // updated the pose of the robot. 
						
			if( (robot_->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(0).array()).any() && 
				(robot_->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(1).array()).any() && 
				(robot_->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(2).array()).any() && 
				(robot_->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(3).array()).any() ){
			
				stab_neg_perturb = measureTipOverStability(); 
			}else{
				break; 
			}
		}else{
			break;
		}	
		
		// compute the gradient
		grad_tipover_stab = (stab_pos_perturb - stab_neg_perturb) / (2*perturbation); 
		
		if (fabsf(grad_tipover_stab) < TOL_CONV_GRAD_METHOD){
			is_optimal_ang_flipper_found = true; // the flipper angle is the one for which its gradient is within the tolerance. Hence it is NOT the one that takes the last gradient computation!
		}else{
			ang_flipper_curr = ang_flipper_curr + 5e-1 * grad_tipover_stab; // converges to a worse stability value ; average number of iterations = 4
		}
		count++; 
	}while(!is_optimal_ang_flipper_found && count < 10); 	
		
	return ang_flipper_curr; 
}
	
/**
 * This saveGeneratedTrajectory function() saves the generated trajectory.
 * @param name_file The name of the file in which the generated trajectory is saved. 
*/
template<class TConfig> void Planner<TConfig>::saveGeneratedTrajectory(const char* name_file) const
{
	std::ofstream myfile; 
	myfile.open(name_file); 
	
	for(int i = 0 ; i < (int)trajectory_.size() ; ++i){	
		myfile << trajectory_[i].t << " " 
		       << trajectory_[i].pose_horizontal(0) << " " 
			   << trajectory_[i].pose_horizontal(1) << " " 
			   << trajectory_[i].pose_horizontal(2) << " " 
			   << trajectory_[i].ang_flipper << " " 
			   << trajectory_[i].vel_linear_body << " "
			   << trajectory_[i].vel_ang_body << std::endl; 
	}
	myfile.close(); 
}

/**
 * This getNumSamplesTraj function() returns the number of samples of the searched trajectory.
 * @return The member variable num_samples_traj_ is returned. 
*/
template<class TConfig> int Planner<TConfig>::getNumSamplesTraj(void)
{
	return num_samples_traj_; 
}

/**
 * This getStateFromTrajectory function() returns a particular state of the searched trajectory.
 * @param count_traj The index of the trajectory for which the robot's pose and the flipper angle are interested.
 * @return This function returns the 6D pose of the robot and the corresponding flipper angle.  
*/
template<class TConfig> std::pair<Eigen::Matrix<Real,Eigen::Dynamic,1>, Real> Planner<TConfig>::getStateFromTrajectory(int count_traj)
{	
	std::pair<Eigen::Matrix<Real,Eigen::Dynamic,1>, Real> traj_curr; 
	Eigen::MatrixXf pose(num_config_dimensions_,1);
	
	traj_curr.first.resize(num_config_dimensions_,1);
	
	if (count_traj < num_samples_traj_){
		pose[0] = trajectory_[count_traj].pose_horizontal(0); 
		pose[1] = trajectory_[count_traj].pose_horizontal(1);
		pose[2] = trajectory_[count_traj].pose_other_than_horiz(0); 
		pose[3] = trajectory_[count_traj].pose_other_than_horiz(1); 
		pose[4] = trajectory_[count_traj].pose_other_than_horiz(2); 
		pose[5] = trajectory_[count_traj].pose_horizontal(2);		

		traj_curr.first = pose; 
		traj_curr.second = trajectory_[count_traj].ang_flipper;
	}else{
		pose.setZero(); 
		traj_curr.first = pose; 
		traj_curr.second = 0.0;
	}

	return traj_curr; 
}

/**
 * This getRobotStartPose function() returns the initial robot pose. 
 * @return This function returns the 6D initial pose of the robot.
*/
template<class TConfig> Eigen::Matrix<Real,Eigen::Dynamic,1> Planner<TConfig>::getRobotStartPose()
{
	return config_start_.q_; 	
}

/**
 * This getRobotGoalPose function() returns the goal robot pose. 
 * @return This function returns the 6D goal pose of the robot.
*/
template<class TConfig> Eigen::Matrix<Real,Eigen::Dynamic,1> Planner<TConfig>::getRobotGoalPose()
{
	return config_goal_.q_; 		
}


/**
 * This setRobotStartPose function() sets a new pose as the robot's initial pose.
*/
template<class TConfig> void Planner<TConfig>::setRobotStartPose(const geometry_msgs::PoseStamped& poseStamped_start)
{
	tf::Quaternion q; 
	double roll, pitch, yaw; 
	
	tf::quaternionMsgToTF(poseStamped_start.pose.orientation,q); 
	tf::Matrix3x3(q).getRPY(roll, pitch, yaw); 
	
	config_start_.q_ << poseStamped_start.pose.position.x, poseStamped_start.pose.position.y, poseStamped_start.pose.position.z, 
						roll, pitch, yaw, 
						ANG_FLIPPER_INIT;
}

/**
 * This setRobotGoalPose function() sets a new pose as the robot's goal pose.
*/
template<class TConfig> void Planner<TConfig>::setRobotGoalPose(const geometry_msgs::PoseStamped& poseStamped_goal)
{
	tf::Quaternion q; 
	double roll, pitch, yaw; 
	
	tf::quaternionMsgToTF(poseStamped_goal.pose.orientation,q); 
	tf::Matrix3x3(q).getRPY(roll, pitch, yaw); 
		
	config_goal_.q_ << poseStamped_goal.pose.position.x, poseStamped_goal.pose.position.y, poseStamped_goal.pose.position.z, 
					   roll, pitch, yaw, ANG_FLIPPER_INIT; 	
}


/**
 * This displayPoint3D function() displays an array of 3D points in RVIZ.
 * @param vect_point3d_in A reference to vector<Point3D>.
*/
template<class TConfig> void Planner<TConfig>::displayPoint3D(std::vector<Point3D>& vect_point3d_in)
{
	visualization_msgs::Marker marker; 
	marker.header.frame_id = "terrain_elev_map"; 
	marker.header.stamp = ros::Time(); 
	
	// set the namespace, type, action and id for this marker. This serves to create a unique ID. 
	marker.ns = "sample_locations"; 
	marker.type = visualization_msgs::Marker::SPHERE_LIST; 
	marker.action = visualization_msgs::Marker::ADD; 
	marker.id = 0; 

	marker.pose.position.x = 0.0; 
	marker.pose.position.y = 0.0; 
	marker.pose.position.z = 0.0; 

	marker.pose.orientation.x = 0.0; 
	marker.pose.orientation.y = 0.0; 
	marker.pose.orientation.z = 0.0; 
	marker.pose.orientation.w = 1.0; 

	marker.scale.x = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.y = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.z = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 

	marker.color.r = 0.0; 
	marker.color.g = 0.0; 
	marker.color.b = 1.0; 
	marker.color.a = 1.0; 


	// specify the color of the marker
	std_msgs::ColorRGBA color; 
	color.r = 0.1; 
	color.g = 0.8; 
	color.b = 0.1; 
	color.a = 1.0; 
	
	marker.color = color;

	// Point
	geometry_msgs::Point point_a; 

	// loop through all vertices

	marker.points.reserve(vect_point3d_in.size()); 
	
	for(int i=0; i<(int)vect_point3d_in.size(); ++i){
		point_a.x = vect_point3d_in[i].x;
		point_a.y = vect_point3d_in[i].y;     	
		point_a.z = vect_point3d_in[i].z;     	

		// add the point pair to the message
		marker.points.push_back(point_a);
	}
				
	// publish the marker
	pub_marker_point3d_.publish(marker); 
	ros::Duration(0.2).sleep();	
}


/**
 * This displayPoint3D function() displays an array of 3D points in RVIZ.
 * @param vect_point3d_in A reference to vector<Point3D>.
*/
template<class TConfig> void Planner<TConfig>::displayPoint3D(TConfig& config)
{
	visualization_msgs::Marker marker; 
	marker.header.frame_id = "terrain_elev_map"; 
	marker.header.stamp = ros::Time(); 
	
	// set the namespace, type, action and id for this marker. This serves to create a unique ID. 
	marker.ns = "sample_locations"; 
	marker.type = visualization_msgs::Marker::SPHERE_LIST; 
	marker.action = visualization_msgs::Marker::ADD; 
	marker.id = 0; 

	marker.pose.position.x = 0.0; 
	marker.pose.position.y = 0.0; 
	marker.pose.position.z = 0.0; 

	marker.pose.orientation.x = 0.0; 
	marker.pose.orientation.y = 0.0; 
	marker.pose.orientation.z = 0.0; 
	marker.pose.orientation.w = 1.0; 

	marker.scale.x = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.y = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.z = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 

	marker.color.r = 0.0; 
	marker.color.g = 0.0; 
	marker.color.b = 1.0; 
	marker.color.a = 1.0; 


	// specify the color of the marker
	std_msgs::ColorRGBA color; 
	color.r = 0.1; 
	color.g = 0.8; 
	color.b = 0.1; 
	color.a = 1.0; 
	
	marker.color = color;

	// Point
	geometry_msgs::Point point_a; 

	// loop through all vertices
	
	point_a.x = config.q_[0];
	point_a.y = config.q_[1];
	point_a.z = config.q_[2];

	// add the point pair to the message
	marker.points.push_back(point_a);
		
	// publish the marker
	pub_marker_point3d_.publish(marker); 
	ros::Duration(0.2).sleep();	
}

template<class TConfig> void Planner<TConfig>::displayLines(std::vector<Eigen::Matrix<Real,3,1> >& vect_point3d_in)
{
	visualization_msgs::Marker marker; 
	marker.header.frame_id = "terrain_elev_map"; 
	marker.header.stamp = ros::Time(); 
	
	// set the namespace, type, action and id for this marker. This serves to create a unique ID. 
	marker.ns = "lines"; 
	marker.type = visualization_msgs::Marker::LINE_LIST; 
	marker.action = visualization_msgs::Marker::ADD; 
	marker.id = 0; 

	marker.pose.position.x = 0.0; 
	marker.pose.position.y = 0.0; 
	marker.pose.position.z = 0.0; 

	marker.pose.orientation.x = 0.0; 
	marker.pose.orientation.y = 0.0; 
	marker.pose.orientation.z = 0.0; 
	marker.pose.orientation.w = 1.0; 

	marker.scale.x = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.y = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.z = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 

	marker.color.r = 0.0; 
	marker.color.g = 0.0; 
	marker.color.b = 1.0; 
	marker.color.a = 1.0; 


	// specify the color of the marker
	std_msgs::ColorRGBA color; 
	color.r = 0.1; 
	color.g = 0.8; 
	color.b = 0.1; 
	color.a = 1.0; 
	
	marker.color = color; 

	// Point
	geometry_msgs::Point point_a; 

	// loop through all vertices

	marker.points.reserve(2*vect_point3d_in.size()); 
	for(int i=0; i<(int)vect_point3d_in.size()-1; ++i){
		point_a.x = vect_point3d_in[i](0);
		point_a.y = vect_point3d_in[i](1);     	
		point_a.z = vect_point3d_in[i](2);      	

		// add the point pair to the message
		marker.points.push_back(point_a);
		
		point_a.x = vect_point3d_in[i+1](0);
		point_a.y = vect_point3d_in[i+1](1);     	
		point_a.z = vect_point3d_in[i+1](2);		
		
		marker.points.push_back(point_a);
	}
	point_a.x = vect_point3d_in[vect_point3d_in.size()-1](0);
	point_a.y = vect_point3d_in[vect_point3d_in.size()-1](1);     	
	point_a.z = vect_point3d_in[vect_point3d_in.size()-1](2);     	

	// add the point pair to the message
	marker.points.push_back(point_a);
	
	point_a.x = vect_point3d_in[0](0);
	point_a.y = vect_point3d_in[0](1);     	
	point_a.z = vect_point3d_in[0](2);		
	
	marker.points.push_back(point_a);
				
	// publish the marker
	pub_marker_line_.publish(marker); 
	ros::Duration(0.2).sleep();	
}

template<class TConfig> void Planner<TConfig>::displayObstacleContours()
{
	visualization_msgs::Marker marker; 
	marker.header.frame_id = "terrain_elev_map"; 
	marker.header.stamp = ros::Time(); 
	
	// set the namespace, type, action and id for this marker. This serves to create a unique ID. 
	marker.ns = "lines"; 
	marker.type = visualization_msgs::Marker::LINE_LIST; 
	marker.action = visualization_msgs::Marker::ADD; 
	marker.id = 0; 

	marker.pose.position.x = 0.0; 
	marker.pose.position.y = 0.0; 
	marker.pose.position.z = 0.0; 

	marker.pose.orientation.x = 0.0; 
	marker.pose.orientation.y = 0.0; 
	marker.pose.orientation.z = 0.0; 
	marker.pose.orientation.w = 1.0; 

	marker.scale.x = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.y = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.z = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 

	marker.color.r = 0.0; 
	marker.color.g = 0.0; 
	marker.color.b = 1.0; 
	marker.color.a = 1.0; 


	// specify the color of the marker
	std_msgs::ColorRGBA color; 
	color.r = 0.33; 
	color.g = 0.33; 
	color.b = 0.33; 
	color.a = 1.0; 
	
	marker.color = color; 

	// Point
	geometry_msgs::Point point_a; 

	// loop through all vertices
	
	marker.points.reserve(2*(num_total_obstacles_contouring_pts_+1)); 

	for(int i=0; i<(int)map_elev_curr_->obstacle_.size(); ++i){
		for (int j=0; j<(int)map_elev_curr_->obstacle_[i].size()-1; ++j){
			point_a.x = map_elev_curr_->obstacle_[i][j](0);
			point_a.y = map_elev_curr_->obstacle_[i][j](1);     	
			point_a.z = map_elev_curr_->obstacle_[i][j](2);      	

			// add the point pair to the message
			marker.points.push_back(point_a);
			
			point_a.x = map_elev_curr_->obstacle_[i][j+1](0);
			point_a.y = map_elev_curr_->obstacle_[i][j+1](1);     	
			point_a.z = map_elev_curr_->obstacle_[i][j+1](2);		
			
			marker.points.push_back(point_a);
		}
		point_a.x = map_elev_curr_->obstacle_[i][map_elev_curr_->obstacle_[i].size()-1](0);
		point_a.y = map_elev_curr_->obstacle_[i][map_elev_curr_->obstacle_[i].size()-1](1);
		point_a.z = map_elev_curr_->obstacle_[i][map_elev_curr_->obstacle_[i].size()-1](2);

		// add the point pair to the message
		marker.points.push_back(point_a);
		
		point_a.x = map_elev_curr_->obstacle_[i][0](0);
		point_a.y = map_elev_curr_->obstacle_[i][0](1);     	
		point_a.z = map_elev_curr_->obstacle_[i][0](2);		
		
		marker.points.push_back(point_a);
	}
			
	// publish the marker
	pub_marker_obstacle_contours_.publish(marker); 
	ros::Duration(0.2).sleep();	
}

template<class TConfig> void Planner<TConfig>::displayLineGradients(std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& vect_point3d_in, std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& vect_gradient_in)
// template<class TConfig> void Planner<TConfig>::displayLineGradients(std::vector<Eigen::Matrix<Real,3,1> >& vect_point3d_in, std::vector<Eigen::Matrix<Real,3,1> >& vect_gradient_in)
{
	visualization_msgs::Marker marker; 
	marker.header.frame_id = "terrain_elev_map"; 
	marker.header.stamp = ros::Time(); 
	
	// set the namespace, type, action and id for this marker. This serves to create a unique ID. 
	marker.ns = "lines_gradient"; 
	marker.type = visualization_msgs::Marker::LINE_LIST; 
	marker.action = visualization_msgs::Marker::ADD; 
	marker.id = 0; 

	marker.pose.position.x = 0.0; 
	marker.pose.position.y = 0.0; 
	marker.pose.position.z = 0.0; 

	marker.pose.orientation.x = 0.0; 
	marker.pose.orientation.y = 0.0; 
	marker.pose.orientation.z = 0.0; 
	marker.pose.orientation.w = 1.0; 

	marker.scale.x = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.y = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 
	marker.scale.z = 0.1; //g_size_node_display; //10*step_c_space_; // 0.2; 

	marker.color.r = 0.0; 
	marker.color.g = 0.0; 
	marker.color.b = 1.0; 
	marker.color.a = 1.0; 

	// specify the color of the marker
	std_msgs::ColorRGBA color; 
	color.r = 0.8; 
	color.g = 0.1; 
	color.b = 0.1; 
	color.a = 1.0; 
	
	marker.color = color; 

	// Point
	geometry_msgs::Point point_a; 

	// loop through all vertices
	
	marker.points.reserve(2*vect_point3d_in.size());

	for(int i=0; i<(int)vect_point3d_in.size(); ++i){	
		for(int j=0; j<(int)vect_point3d_in[i].size(); ++j){
	
			point_a.x = vect_point3d_in[i][j](0);
			point_a.y = vect_point3d_in[i][j](1);     	
			point_a.z = vect_point3d_in[i][j](2);      	

			// add the point pair to the message
			marker.points.push_back(point_a);
			
			Real norm_vect_gradient_in = vect_gradient_in[i][j].norm();
			point_a.x = vect_point3d_in[i][j](0) + vect_gradient_in[i][j](0) / norm_vect_gradient_in; 
			point_a.y = vect_point3d_in[i][j](1) + vect_gradient_in[i][j](1) / norm_vect_gradient_in;     	
			point_a.z = vect_point3d_in[i][j](2) + vect_gradient_in[i][j](2) / norm_vect_gradient_in;		
			
			marker.points.push_back(point_a);
		}
	}
	
	// publish the marker
	pub_marker_line_gradient_.publish(marker); 
	ros::Duration(0.2).sleep();	
}


/**
* This displayConfigSolution() function displays the nodes that form the optimal path.
*/
template<class TConfig> void Planner<TConfig>::displayConfigSolution()
{
	visualization_msgs::Marker marker;
	// set the frame ID and timestamp
	if(map_elev_curr_->getIsMapStaticGlobal()){
		marker.header.frame_id = "terrain_elev_map";
	}else{
		marker.header.frame_id = "d6dvo_map";
	}
	marker.header.stamp = ros::Time();

	// set the namespace, type, action and id for this marker. This serves to create a unique ID.
	marker.ns = "states_optimal_path";
	marker.type = visualization_msgs::Marker::LINE_LIST;
	marker.action = visualization_msgs::Marker::ADD;
	marker.id = 9;

	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;

	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;

	marker.scale.x = 0.5*g_size_node_display;//5*step_c_space_; // 0.02;
	marker.scale.y = 0.5*g_size_node_display;//5*step_c_space_; // 0.02;
	marker.scale.z = 0.5*g_size_node_display;//5*step_c_space_; // 0.02;

	marker.color.r = 0.0;
	marker.color.g = 0.0;
	marker.color.b = 1.0;
	marker.color.a = 1.0;

	// specify the color of the marker
	std_msgs::ColorRGBA color;
	color.r = 0.0;
	color.g = 1.0;
	color.b = 0.0;
	color.a = 1.0;
	
	marker.color = color; 

	// Point
	geometry_msgs::Point point_a, point_b;
	
	marker.points.reserve(2*(config_solution_.size()-1));

	for(int i=0; i<(int)(config_solution_.size()-1) ; ++i){

		point_a.x = config_solution_[i].q_[0];
		point_a.y = config_solution_[i].q_[1];
		point_a.z = this->map_elev_curr_->estimateTerrainHeight(config_solution_[i].q_[0], config_solution_[i].q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;		

		point_b.x = config_solution_[i+1].q_[0];
		point_b.y = config_solution_[i+1].q_[1];
		point_b.z = this->map_elev_curr_->estimateTerrainHeight(config_solution_[i+1].q_[0], config_solution_[i+1].q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;

		// add the point pair to the message
		marker.points.push_back(point_a);
		marker.points.push_back(point_b);
	}

	// publish the marker
	pub_marker_config_solution_.publish(marker);
	ros::Duration(0.2).sleep();	
}


/**
* This displayConfigSmoothedSolution() function displays the nodes that form the smoothed path.
*/
template<class TConfig> void Planner<TConfig>::displayConfigSmoothedSolution()
{
	visualization_msgs::Marker marker;
	// set the frame ID and timestamp
	if(map_elev_curr_->is_map_static_global_){
		marker.header.frame_id = "terrain_elev_map";		
	}else{
		marker.header.frame_id = "d6dvo_map";
	}
	marker.header.stamp = ros::Time();

	// set the namespace, type, action and id for this marker. This serves to create a unique ID.
	marker.ns = "states_smoothed_path";
	marker.type = visualization_msgs::Marker::LINE_LIST;
	marker.action = visualization_msgs::Marker::ADD;
	marker.id = 9;

	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;

	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;

	marker.scale.x = 1.0*g_size_node_display;//5*step_c_space_; // 0.02;
	marker.scale.y = 1.0*g_size_node_display;//5*step_c_space_; // 0.02;
	marker.scale.z = 1.0*g_size_node_display;//5*step_c_space_; // 0.02;

	marker.color.r = 0.0;
	marker.color.g = 0.0;
	marker.color.b = 1.0;
	marker.color.a = 1.0;

	// specify the color of the marker
	std_msgs::ColorRGBA color;
	color.r = 1.0;
	color.g = 0.0;
	color.b = 1.0;
	color.a = 1.0;
	
	marker.color = color; 

	// Point
	geometry_msgs::Point point_a, point_b;

	marker.points.reserve(2*(config_smoothed_solution_.size()-1)); 
	for(int i=0; i<(int)(config_smoothed_solution_.size()-1) ; ++i){

		point_a.x = config_smoothed_solution_[i].q_[0];
		point_a.y = config_smoothed_solution_[i].q_[1];		
		point_a.z = this->map_elev_curr_->estimateTerrainHeight(config_smoothed_solution_[i].q_[0], config_smoothed_solution_[i].q_[1]) + 0.5 * ROBOT_TRACK_HEIGHT;

		point_b.x = config_smoothed_solution_[i+1].q_[0];
		point_b.y = config_smoothed_solution_[i+1].q_[1];
		point_b.z = this->map_elev_curr_->estimateTerrainHeight(config_smoothed_solution_[i+1].q_[0], config_smoothed_solution_[i+1].q_[1]) + 0.5 * ROBOT_TRACK_HEIGHT;	

		// add the point pair to the message
		marker.points.push_back(point_a);
		marker.points.push_back(point_b);
	}

	// publish the marker
	pub_marker_config_smoothed_solution_.publish(marker);
	ros::Duration(0.2).sleep();	
}

/**
 * This publishTrajectoryMsg function() publishes a trajectory.
 * @param vect_point3d_in A reference to std::vector<Point3D>.
*/
template<class TConfig> void Planner<TConfig>::publishTrajectoryMsg(Trajectory<TConfig>& traj, int id_trajectory) // had to include id_trajectory as an argument because this counter is not shared b/w PlannerInitial and PlannerRRTStar
{
	fraudo_msgs::Trajectory traj_msg;
	traj_msg.is_map_static_global = is_map_static_global_; // to indicate the controller if the map is generated synthetically or is captured from the Xtion.
	traj_msg.id_trajectory = id_trajectory; 
	traj_msg.ind_switch_traj_prev = ind_switch_traj_prev_; // index of the previous trajectory from which the previous trajectory will be replaced by this new one.		
	
	traj_msg.num_samples = (int)traj.data_.size();
	traj_msg.x.reserve(traj_msg.num_samples); 
	traj_msg.y.reserve(traj_msg.num_samples); 
	traj_msg.z.reserve(traj_msg.num_samples); 
	traj_msg.roll.reserve(traj_msg.num_samples); 
	traj_msg.pitch.reserve(traj_msg.num_samples); 
	traj_msg.yaw.reserve(traj_msg.num_samples); 
	traj_msg.ang_flipper.reserve(traj_msg.num_samples); 
	traj_msg.vel_linear_body.reserve(traj_msg.num_samples); 
	traj_msg.vel_ang_body.reserve(traj_msg.num_samples); 
	traj_msg.time.reserve(traj_msg.num_samples);			
	
	for(int i=0; i<traj_msg.num_samples; ++i){
		traj_msg.x.push_back(traj.data_[i](0)); 
		traj_msg.y.push_back(traj.data_[i](1)); 
		traj_msg.z.push_back(traj.data_[i](2)); 
		traj_msg.roll.push_back(traj.data_[i](3)); 
		traj_msg.pitch.push_back(traj.data_[i](4)); 
		traj_msg.yaw.push_back(traj.data_[i](5)); 
		traj_msg.ang_flipper.push_back(traj.data_[i](6)); 
		traj_msg.vel_linear_body.push_back(traj.data_[i](7)); 
		traj_msg.vel_ang_body.push_back(traj.data_[i](8)); 
		traj_msg.time.push_back(traj.data_[i](9));			
	}	
	pub_traj_.publish(traj_msg);	
}

/**
 * This splineDeBoor function() splines a path using the path configurations as control points.
 * @param traj A reference to the trajectory.
*/
template<class TConfig> void Planner<TConfig>::splineDeBoor(Trajectory<TConfig>& traj)
{
  // First define some control points
  
  // Define the order and the number of control points
  int order = 4; 
  int num_control_pts = config_solution_.size(); 
  
  // I need to make sure that the order is equal or larger than the number of control pts (Nov/09/2013).
  if (order > num_control_pts){
	  order = num_control_pts;
  }

  int num_interp = num_traj_interpolation_pts_; // number of interpolation points should be variable depending on the length of the searched path assuming that the time step is fixed. See my notes of Nov/06/2013. This value is updated right before the call of the present function. 
  double* y = new double[num_interp]; 
  for(int i=0; i<num_interp; ++i){
    y[i] = (double)(i)/(double)(num_interp-1); 
  }

  int size_T = num_control_pts-order+2; 
  double* T = new double[size_T]; 
  for(int i=0; i<size_T; ++i){
      T[i] = (double)(i)/(double)(size_T-1); 
  }

  double **X, **Y, **Z, **roll, **pitch, **ang_flipper; 
  X = new double*[order]; 
  Y = new double*[order]; 
  Z = new double*[order]; 
  roll = new double*[order]; 
  pitch = new double*[order]; 
  ang_flipper = new double*[order]; 
  for(int i=0; i<order; ++i){
    X[i] = new double[order]; 
    Y[i] = new double[order]; 
    Z[i] = new double[order]; 
    roll[i] = new double[order]; 
    pitch[i] = new double[order]; 
    ang_flipper[i] = new double[order]; 
  }
  
  int size_T_new = 2*(order-1)+size_T; 
  double *T_new;
  T_new = new double[size_T_new]; 
  
  int ind_T = 0; 
  for(int i=0; i<size_T_new; ++i){
	if(i < order-1){
	  T_new[i] = T[0]; 
	}else{
	  if(i < ((order-1)+size_T)){		
		T_new[i] = T[ind_T]; 
		ind_T++; 
	  }else{
		T_new[i] = T[size_T-1]; 
	  }
	}
  }
   
  for(int l=0; l<num_interp; ++l){		 
    double t0 = y[l]; 
    int k; 
    for(int id=0; id<size_T_new; ++id){
		
      
      if((t0-T_new[id]) >= -TOL_NUM_ERR_FLOOR){ // dealing with numerical errors
		k = id; // hence k saves the index of the last element of T_new[ind] that is smaller or equal to t0
      } 
    }
    
    if( k > num_control_pts){
	  break;
    }
    
    for(int j=k-order+1, i=0; j<k+1; ++j,++i){
      X[i][0] = config_solution_[j].q_[0]; 
      Y[i][0] = config_solution_[j].q_[1]; 
      Z[i][0] = config_solution_[j].q_[2]; 
      roll[i][0] = config_solution_[j].q_[3]; 
      pitch[i][0] = config_solution_[j].q_[4]; 
      ang_flipper[i][0] = config_solution_[j].q_[6]; 
    }
    
    for(int i=1; i<order; ++i){
      for(int j=i; j<order; ++j){
		double num = t0 - T_new[k-order+j+1]; 
				
		double weight; 
		if (fabsf(num) < TOL_NUM_ERR_FLOOR){
			weight = 0.0; 
		}else{
			double s = T_new[k+j-i+1] - T_new[k-order+j+1]; 
			weight = num/s; 
		}

		X[j][i] = (1-weight)*X[j-1][i-1] + weight*X[j][i-1];
		Y[j][i] = (1-weight)*Y[j-1][i-1] + weight*Y[j][i-1];
		Z[j][i] = (1-weight)*Z[j-1][i-1] + weight*Z[j][i-1];
		roll[j][i] = (1-weight)*roll[j-1][i-1] + weight*roll[j][i-1];
		pitch[j][i] = (1-weight)*pitch[j-1][i-1] + weight*pitch[j][i-1];
		ang_flipper[j][i] = (1-weight)*ang_flipper[j-1][i-1] + weight*ang_flipper[j][i-1];
		
      }
    }    

	Eigen::Matrix<Real,10,1> traj_tmp; // x_,y_,z_,roll_,pitch_,yaw_,ang_flipper_,vel_linear_body_,vel_ang_body_,time_
	traj_tmp(0) = X[order-1][order-1]; // [m], x
	traj_tmp(1) = Y[order-1][order-1]; // [m], y
	traj_tmp(2) = Z[order-1][order-1]; // [m], z
	traj_tmp(3) = roll[order-1][order-1]; // [rad], roll
	traj_tmp(4) = pitch[order-1][order-1]; // [rad], pitch
	traj_tmp(6) = ang_flipper[order-1][order-1]; // [rad], ang_flipper
	// For the rest of elements of traj_tmp will be assigned posteriorly.
	traj.data_.push_back(traj_tmp); 
  }
  
  // from the previous loop the last element of the array does not have any value assigned.Hence, let's assign the goal pose here.   
  Eigen::Matrix<Real,10,1> traj_tmp; // x_,y_,z_,roll_,pitch_,yaw_,ang_flipper_,vel_linear_body_,vel_ang_body_,time_
  traj_tmp(0) = config_goal_.q_(0); // [m], x
  traj_tmp(1) = config_goal_.q_(1); // [m], y
  traj_tmp(2) = config_goal_.q_(2); // [m], z
  traj_tmp(3) = config_goal_.q_(3); // [rad], roll
  traj_tmp(4) = config_goal_.q_(4); // [rad], pitch
  traj.data_.push_back(traj_tmp); 
  
  traj.num_data_ = num_interp; 
  
  delete [] T_new; 

  for(int i=0; i<order; ++i){
    delete [] X[i]; 
    delete [] Y[i]; 
    delete [] Z[i]; 
    delete [] roll[i]; 
    delete [] pitch[i]; 
    delete [] ang_flipper[i]; 
  }
  delete [] X;
  delete [] Y;
  delete [] Z; 
  delete [] roll; 
  delete [] pitch; 
  delete [] ang_flipper; 

  delete[] T; 
  delete[] y; 
}


/**
 * This generateSmoothTrajectoryFromPath function() generates a smooth trajectory from a non-smooth trajectory.
 * @param traj A reference to the trajectory.
*/ 
template<class TConfig> void Planner<TConfig>::generateSmoothTrajectoryFromPath(Trajectory<TConfig>& traj)
{		
	double **vect_tangent; 
	
	traj.num_data_ = (int)traj.data_.size();
	
	vect_tangent = new double*[traj.num_data_]; 
	for(int i=0; i<traj.num_data_; ++i){
		vect_tangent[i] = new double[2]; 
	}
	double delta_s; 
	
	vect_tangent[0][0] = traj.data_[1](0) - traj.data_[0](0); 
	vect_tangent[1][0] = traj.data_[1](1) - traj.data_[0](1); 
	

	traj.data_[0](5) = config_start_.q_(5); // yaw in rad
	traj.data_[0](6) = ANG_FLIPPER_INIT; // flipper angle in radians
	traj.data_[0](7) = 0.0; // vel_linear_body in meters per second	
	traj.data_[0](8) = 0.0; // vel_ang_body in radians per second	
	traj.data_[0](9) = 0.0; // time in second.	

	int num_ind_end_initial_transition = 100; //100;//25; //50; //100;//50; 
	int num_ind_start_final_transition;  
	if(traj.num_data_ < 3*num_ind_end_initial_transition){ // if the traj.num_data_ is smaller than three times of num_ind_end_initial_transition, then consider 10% of the traj.num_data_ for the end of the initial transition and 90% of the traj.num_data_ for the start of the final transition.
		num_ind_end_initial_transition = floorf(0.1 * traj.num_data_); 
		num_ind_start_final_transition = floorf(0.9 * traj.num_data_); 
	}else{
		num_ind_start_final_transition = traj.num_data_ - num_ind_end_initial_transition; 
	}
	
	// For the initial transition to raise the speeds linearly. 		
	Real vel_linear_end_initial_transition = sqrt(pow(traj.data_[num_ind_end_initial_transition](0) - traj.data_[num_ind_end_initial_transition-1](0),2)+pow(traj.data_[num_ind_end_initial_transition](1) - traj.data_[num_ind_end_initial_transition-1](1),2)) / TIME_INCR_TRAJ; 
	Real yaw_end_initial_transition = 	modNPiPi(atan2f(traj.data_[num_ind_end_initial_transition+1](1) - traj.data_[num_ind_end_initial_transition-1](1),traj.data_[num_ind_end_initial_transition+1](0) - traj.data_[num_ind_end_initial_transition-1](0))); 
	Real yaw_before_end_initial_transition = modNPiPi(atan2f(traj.data_[num_ind_end_initial_transition](1) - traj.data_[num_ind_end_initial_transition-2](1),traj.data_[num_ind_end_initial_transition](0) - traj.data_[num_ind_end_initial_transition-2](0))); 
	Real vel_angular_end_initial_transition = modNPiPi(yaw_end_initial_transition - yaw_before_end_initial_transition) / TIME_INCR_TRAJ; // [rad/s], angular vel	

	Real yaw_before_before_end_initial_transition = modNPiPi(atan2f(traj.data_[num_ind_end_initial_transition-1](1) - traj.data_[num_ind_end_initial_transition-3](1),traj.data_[num_ind_end_initial_transition-1](0) - traj.data_[num_ind_end_initial_transition-3](0))); 
	Real vel_angular_before_end_initial_transition = modNPiPi(yaw_before_end_initial_transition - yaw_before_before_end_initial_transition) / TIME_INCR_TRAJ; // [rad/s], angular vel	
	Real ang_angular_end_initial_transition = (vel_angular_end_initial_transition - vel_angular_before_end_initial_transition)/ TIME_INCR_TRAJ;
		
	Real time_end_initial_transition = num_ind_end_initial_transition*TIME_INCR_TRAJ; 
	
	// Including acceleration as well. 
	Eigen::Matrix<Real,6,6> mat_bc; 
	mat_bc << 1,0,0,0,0,0,
			  1,time_end_initial_transition,pow(time_end_initial_transition,2),pow(time_end_initial_transition,3),pow(time_end_initial_transition,4),pow(time_end_initial_transition,5),
			  0,1,0,0,0,0,
			  0,1,2*time_end_initial_transition,3*pow(time_end_initial_transition,2),4*pow(time_end_initial_transition,3),5*pow(time_end_initial_transition,4), 
			  0,0,2,0,0,0,
			  0,0,2,6*time_end_initial_transition,12*pow(time_end_initial_transition,2),20*pow(time_end_initial_transition,3); 	
	Eigen::Matrix<Real,6,6> mat_bc_inv = mat_bc.inverse(); 
	Eigen::Matrix<Real,6,3> coeff_bc, boundary_conds; 
	boundary_conds.col(0) << traj.data_[0](0), 
							 traj.data_[num_ind_end_initial_transition](0), 
							 0, 
							 (traj.data_[num_ind_end_initial_transition](0) - traj.data_[num_ind_end_initial_transition-1](0))/TIME_INCR_TRAJ, 
							 0,
							 (traj.data_[num_ind_end_initial_transition](0) - 2*traj.data_[num_ind_end_initial_transition-1](0) + traj.data_[num_ind_end_initial_transition-2](0))/pow(TIME_INCR_TRAJ,2); 
	
	boundary_conds.col(1) << traj.data_[0](1), 
							 traj.data_[num_ind_end_initial_transition](1), 
							 0, 
							 (traj.data_[num_ind_end_initial_transition](1) - traj.data_[num_ind_end_initial_transition-1](1))/TIME_INCR_TRAJ, 
							 0, 
							 (traj.data_[num_ind_end_initial_transition](1) - 2*traj.data_[num_ind_end_initial_transition-1](1) + traj.data_[num_ind_end_initial_transition-2](1))/pow(TIME_INCR_TRAJ,2); 
							 
	
	boundary_conds.col(2) << traj.data_[0](5), 
							 yaw_end_initial_transition, 
							 0, 
							 vel_angular_end_initial_transition, 
							 0, 
							 ang_angular_end_initial_transition; 

	coeff_bc = mat_bc_inv*boundary_conds; 
		
	for(int i=1; i<num_ind_end_initial_transition; ++i){
		traj.data_[i](9) = i*TIME_INCR_TRAJ; // [s], time
			
		Eigen::Matrix<Real,1,6> time_vect; 
		time_vect << 1, traj.data_[i](9), pow(traj.data_[i](9),2), pow(traj.data_[i](9),3), pow(traj.data_[i](9),4), pow(traj.data_[i](9),5); 
		
		traj.data_[i](0) = time_vect*coeff_bc.col(0);
		traj.data_[i](1) = time_vect*coeff_bc.col(1);
		traj.data_[i](5) = time_vect*coeff_bc.col(2);
		
		Real xdot = coeff_bc(1,0) + 2*coeff_bc(2,0)*traj.data_[i](9) + 3*coeff_bc(3,0)*pow(traj.data_[i](9),2) + 4*coeff_bc(4,0)*pow(traj.data_[i](9),3) + 5*coeff_bc(5,0)*pow(traj.data_[i](9),4); 
		Real ydot = coeff_bc(1,1) + 2*coeff_bc(2,1)*traj.data_[i](9) + 3*coeff_bc(3,1)*pow(traj.data_[i](9),2) + 4*coeff_bc(4,1)*pow(traj.data_[i](9),3) + 5*coeff_bc(5,1)*pow(traj.data_[i](9),4); 
		traj.data_[i](7) = sqrt(xdot*xdot+ydot*ydot); 
		traj.data_[i](8) = coeff_bc(1,2) + 2*coeff_bc(2,2)*traj.data_[i](9) + 3*coeff_bc(3,2)*pow(traj.data_[i](9),2) + 4*coeff_bc(4,2)*pow(traj.data_[i](9),3) + 5*coeff_bc(5,2)*pow(traj.data_[i](9),4); 
		
		vect_tangent[i][0] = traj.data_[i+1](0) - traj.data_[i-1](0); 
		vect_tangent[i][1] = traj.data_[i+1](1) - traj.data_[i-1](1); 		
	}
		
	traj.data_[num_ind_end_initial_transition](7) =  vel_linear_end_initial_transition; 
	traj.data_[num_ind_end_initial_transition](8) =  vel_angular_end_initial_transition; 
	traj.data_[num_ind_end_initial_transition](5) =  yaw_end_initial_transition; 
		
	traj.data_[num_ind_end_initial_transition](9) = num_ind_end_initial_transition*TIME_INCR_TRAJ; // [s], time
	
	vect_tangent[num_ind_end_initial_transition][0] = traj.data_[num_ind_end_initial_transition+1](0) - traj.data_[num_ind_end_initial_transition-1](0); 
	vect_tangent[num_ind_end_initial_transition][1] = traj.data_[num_ind_end_initial_transition+1](1) - traj.data_[num_ind_end_initial_transition-1](1); 
		
	// The body of the trajectory	
	for(int i=num_ind_end_initial_transition+1; i<num_ind_start_final_transition; ++i){	
		vect_tangent[i][0] = traj.data_[i+1](0) - traj.data_[i-1](0); 
		vect_tangent[i][1] = traj.data_[i+1](1) - traj.data_[i-1](1); 
		delta_s = sqrt(pow(traj.data_[i](0) - traj.data_[i-1](0),2)+pow(traj.data_[i](1) - traj.data_[i-1](1),2)); 

		traj.data_[i](5) = modNPiPi(atan2f(vect_tangent[i][1],vect_tangent[i][0])); // [rad], yaw
		traj.data_[i](7) = delta_s / TIME_INCR_TRAJ; // [m/s], linear vel
		traj.data_[i](8) = modNPiPi(traj.data_[i][5] - traj.data_[i-1][5]) / TIME_INCR_TRAJ; // [rad/s], angular vel
		traj.data_[i](9) = i*TIME_INCR_TRAJ; // [s], time
	}
	
	Real time_start_final_transition = (num_ind_start_final_transition-1)*TIME_INCR_TRAJ; 	
	Real time_end = (traj.num_data_-1)*TIME_INCR_TRAJ; 	

	// Considering only position and velocity
	Eigen::Matrix4d mat_bc_1, mat_bc_inv_1; 
	mat_bc_1 << 1, time_start_final_transition, pow(time_start_final_transition,2), pow(time_start_final_transition,3), 
			    1, time_end,                    pow(time_end,2),                    pow(time_end,3), 
			    0, 1,                           2*time_start_final_transition,      3*pow(time_start_final_transition,2),
			    0, 1,                           2*time_end,                         3*pow(time_end,2); 				
	Eigen::Matrix<double,4,3> coeff_bc_1, boundary_conds_1; 
	boundary_conds_1.col(0) << traj.data_[num_ind_start_final_transition-1](0), 
							 traj.data_[traj.num_data_-1](0), 
							 (traj.data_[num_ind_start_final_transition-1](0) - traj.data_[num_ind_start_final_transition-2](0))/TIME_INCR_TRAJ, 							
							 0; 
	boundary_conds_1.col(1) << traj.data_[num_ind_start_final_transition-1](1), 
							 traj.data_[traj.num_data_-1](1), 
							 (traj.data_[num_ind_start_final_transition-1](1) - traj.data_[num_ind_start_final_transition-2](1))/TIME_INCR_TRAJ, 
							 0; 
	boundary_conds_1.col(2) << traj.data_[num_ind_start_final_transition-1](5), 
							 modNPiPi(atan2f(traj.data_[traj.num_data_-1](1) - traj.data_[traj.num_data_-2](1),traj.data_[traj.num_data_-1](0) - traj.data_[traj.num_data_-2](0))),
							 traj.data_[num_ind_start_final_transition-1](8), 
							 0; 

	mat_bc_inv_1 = mat_bc_1.inverse(); 
	
	coeff_bc_1 = mat_bc_inv_1*boundary_conds_1; 

	for(int i=num_ind_start_final_transition; i<traj.num_data_-1; ++i){		
		traj.data_[i](9) = i*TIME_INCR_TRAJ; // [s], time
				
		Eigen::Matrix<double,1,4> time_vect(1, traj.data_[i](9), pow(traj.data_[i](9),2), pow(traj.data_[i](9),3)); 
		
		traj.data_[i](0) = time_vect*coeff_bc_1.col(0);
		traj.data_[i](1) = time_vect*coeff_bc_1.col(1);
		traj.data_[i](5) = time_vect*coeff_bc_1.col(2);
		
		Real xdot = coeff_bc_1(1,0) + 2*coeff_bc_1(2,0)*traj.data_[i](9) + 3*coeff_bc_1(3,0)*pow(traj.data_[i](9),2); 
		Real ydot = coeff_bc_1(1,1) + 2*coeff_bc_1(2,1)*traj.data_[i](9) + 3*coeff_bc_1(3,1)*pow(traj.data_[i](9),2); 
		traj.data_[i](7) = sqrt(xdot*xdot+ydot*ydot); 
		traj.data_[i](8) = coeff_bc_1(1,2) + 2*coeff_bc_1(2,2)*traj.data_[i](9) + 3*coeff_bc_1(3,2)*pow(traj.data_[i](9),2); 
				
		vect_tangent[i][0] = traj.data_[i+1](0) - traj.data_[i-1](0); 
		vect_tangent[i][1] = traj.data_[i+1](1) - traj.data_[i-1](1); 
		
	}
		
	// For the final sample
	vect_tangent[traj.num_data_-1][0] = traj.data_[traj.num_data_-1](0) - traj.data_[traj.num_data_-2](0); 
	vect_tangent[traj.num_data_-1][1] = traj.data_[traj.num_data_-1](1) - traj.data_[traj.num_data_-2](1); 
	
	traj.data_[traj.num_data_-1](5) = traj.data_[traj.num_data_-2](5); // yaw in rad
	traj.data_[traj.num_data_-1](6) = traj.data_[traj.num_data_-2](6); // flipper angle in radians
	traj.data_[traj.num_data_-1](7) = 0.0; // vel_linear_body in meters per second
	traj.data_[traj.num_data_-1](8) = 0.0; // vel_ang_body in radians per second	
	traj.data_[traj.num_data_-1](9) = (traj.num_data_-1)*TIME_INCR_TRAJ;  // time in second.
				
	for (int i=0; i<traj.num_data_; ++i){
		delete [] vect_tangent[i];
	}
    delete [] vect_tangent;
    
}

/**
 * This displayStartAndGoalNodes function() displays start and goal nodes in RVIZ.
*/
template<class TConfig> void Planner<TConfig>::displayStartAndGoalConfigs()
{
	visualization_msgs::Marker marker; 
	// set the frame ID and timestamp
	if(map_elev_curr_->is_map_static_global_){
		marker.header.frame_id = "terrain_elev_map"; 		
	}else{
		marker.header.frame_id = "d6dvo_map"; 
	}
	marker.header.stamp = ros::Time(); 

	// set the namespace, type, action and id for this marker. This serves to create a unique ID. 
	marker.ns = "start_and_goal_nodes"; 
	marker.type = visualization_msgs::Marker::SPHERE_LIST; 
	marker.action = visualization_msgs::Marker::ADD; 
	marker.id = 4; 

	marker.pose.position.x = 0.0; 
	marker.pose.position.y = 0.0; 
	marker.pose.position.z = 0.0; 

	marker.pose.orientation.x = 0.0; 
	marker.pose.orientation.y = 0.0; 
	marker.pose.orientation.z = 0.0; 
	marker.pose.orientation.w = 1.0; 

	Real g_size_node_display = 0.05;
	marker.scale.x = 2*g_size_node_display;//8*step_c_space_; // 0.05; 
	marker.scale.y = 2*g_size_node_display;//8*step_c_space_; // 0.05; 
	marker.scale.z = 2*g_size_node_display;//8*step_c_space_; // 0.05; 

	marker.color.r = 1.0; 
	marker.color.g = 0.0; 
	marker.color.b = 1.0; 
	marker.color.a = 1.0; 

	// specify the color of the marker
	std_msgs::ColorRGBA color; 
	color.r = 1.0; 
	color.g = 0.0; 
	color.b = 1.0; 
	color.a = 1.0; 

	marker.colors = color; 

	// Point
	geometry_msgs::Point point_a; 

	point_a.x = config_start_.q_[0];
	point_a.y = config_start_.q_[1];
	point_a.z = config_start_.q_[2];
	  
	// add the point pair to the message
	marker.points.push_back(point_a);
	// marker.colors.push_back(color);

	point_a.x = config_goal_.q_[0];
	point_a.y = config_goal_.q_[1];
	point_a.z = config_goal_.q_[2];
	  
	// add the point pair to the message
	marker.points.push_back(point_a);

	// publish the marker
	pub_marker_startAndGoal_.publish(marker); 
	ros::Duration(0.2).sleep();	
}


template<class TConfig> void Planner<TConfig>::setIndSampleDimsInConfigDims(std::vector<int> ind_sample_dims_in_config_dims)
{
	ind_sample_dims_in_config_dims_.resize(ind_sample_dims_in_config_dims.size()); 
	std::copy(ind_sample_dims_in_config_dims.begin(), ind_sample_dims_in_config_dims.end(), ind_sample_dims_in_config_dims_.begin()); 	
}

template<class TConfig> void Planner<TConfig>::setBoundsSampleDims(Eigen::Matrix<Real,Eigen::Dynamic,2> bounds_sample_dims) // first column: lower bound; second column: upper bound
{	
	bounds_sample_dims_.resize(bounds_sample_dims.rows(),2); 
	bounds_sample_dims_ = bounds_sample_dims; 	
}

template<class TConfig> int Planner<TConfig>::evaluateSampleAgainstSampleBounds(TConfig& config) // first column: lower bound; second column: upper bound
{
	for(int i=0; i<num_sample_dimensions_; ++i){
		if (config.q_[ind_sample_dims_in_config_dims_[i]] < bounds_sample_dims_(i,0) || 
			config.q_[ind_sample_dims_in_config_dims_[i]] > bounds_sample_dims_(i,1)){
	
			return ERROR; 
		}
	}
		
	return OK; 	
}


template<class TConfig> void Planner<TConfig>::defineElevMapBounds(Terrain* map_elev_curr)
{
	boundary_terrain_domain_.row(0).setConstant(map_elev_curr_->xmin_ + MARGIN_SAFETY_BOUNDARY_TERRAIN); // (+MARGIN_SAFETY_BOUNDARY_TERRAIN) for a tighter constraint; OW, I got robot configurations barely satisfying the boundary conditions, but causing pose estimation error.
	boundary_terrain_domain_.row(1).setConstant(map_elev_curr_->xmax_ - MARGIN_SAFETY_BOUNDARY_TERRAIN);// (-MARGIN_SAFETY_BOUNDARY_TERRAIN) for a tighter constraint; OW, I got robot configurations barely satisfying the boundary conditions, but causing pose estimation error.
	boundary_terrain_domain_.row(2).setConstant(map_elev_curr_->ymin_ + MARGIN_SAFETY_BOUNDARY_TERRAIN); // (+MARGIN_SAFETY_BOUNDARY_TERRAIN) for a tighter constraint; OW, I got robot configurations barely satisfying the boundary conditions, but causing pose estimation error.
	boundary_terrain_domain_.row(3).setConstant(map_elev_curr_->ymax_ - MARGIN_SAFETY_BOUNDARY_TERRAIN); // (-MARGIN_SAFETY_BOUNDARY_TERRAIN) for a tighter constraint; OW, I got robot configurations barely satisfying the boundary conditions, but causing pose estimation error.	
}


template<class TConfig> int Planner<TConfig>::interpolateConfigLinearly( const TConfig& config_start, const TConfig& config_end, const Real alpha, TConfig& config_interpolated)
{	
	for(int i=0; i<num_config_dimensions_; ++i){
		config_interpolated.q_[i] = (1-alpha)*config_start.q_[i] + alpha*config_end.q_[i]; 		
	}
		
	return OK; 
}

template<class TConfig> int Planner<TConfig>::interpolateSampleLinearly( const TConfig& config_start, const TConfig& config_end, const Real alpha, TConfig& config_interpolated)
{	
	
	// just for the coordinates of the sample space (and not of the configuration space.) 
	for(int i=0; i<num_sample_dimensions_; ++i){	
		config_interpolated.q_[ind_sample_dims_in_config_dims_[i]] = (1-alpha)*config_start.q_[ind_sample_dims_in_config_dims_[i]] + alpha*config_end.q_[ind_sample_dims_in_config_dims_[i]]; 
		
		// evaluate an interpolated configuration against the sample space bounds. 
		if (config_interpolated.q_[ind_sample_dims_in_config_dims_[i]] < bounds_sample_dims_(i,0) || 
			config_interpolated.q_[ind_sample_dims_in_config_dims_[i]] > bounds_sample_dims_(i,1)){	
			return ERROR; 
		}		
	}
		
	return OK; 
}


template<class TConfig> int Planner<TConfig>::updateHorizontalRobotConfig(const TConfig& config, Robot* robot) // updated the member variable robot_ with its contact points updated. 
{
	robot->pose_ << config.q_[0], config.q_[1], POSE_ROBOT_INIT_Z, POSE_ROBOT_INIT_ROLL, POSE_ROBOT_INIT_PITCH, config.q_[5], ANG_FLIPPER_INIT; 	
	robot->updatePoseRobot(); 
	
	if( (robot->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(0).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(1).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(2).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(3).array()).any() ){
		
		return ERROR; 
	}
	return OK; 		
}

template<class TConfig> int Planner<TConfig>::updateFullRobotConfig(const TConfig& config, Robot* robot) // updated the member variable robot_ with its contact points updated. 
{
	robot->pose_ = config.q_; 
	robot->updatePoseRobot(); 
	
	if( (robot->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(0).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(1).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(2).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(3).array()).any() ){
		
		return ERROR; 
	}
	
	return OK; 		
}


template<class TConfig> int Planner<TConfig>::updateFullRobotConfigFromItsHorionzontalConfig(const TConfig& config, Robot* robot) // fully update the robot configuration. 
{	
	robot->pose_ << config.q_[0], config.q_[1], POSE_ROBOT_INIT_Z, POSE_ROBOT_INIT_ROLL, POSE_ROBOT_INIT_PITCH, config.q_[5], ANG_FLIPPER_INIT; 

	robot->updatePoseRobot(); 
	if (robot->findPtsTerrainContact() == ERROR){
		return ERROR; 
	}
	
	Eigen::Matrix<Real,1,NUM_TOTAL_CONTACT> height_potential_contact_pts = robot->pt_robot_potential_contact_global_frame_.bottomRows(1) - robot->pt_terrain_contact_.bottomRows(1); 
	Real height_lowest_potential_contact_pt = height_potential_contact_pts.minCoeff(); 
	robot->pose_(2) -= height_lowest_potential_contact_pt; 
	robot->updatePoseRobot(); 
	
	if( (robot->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(0).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(1).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(2).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(3).array()).any() ){
		
		return ERROR; 
	}
		
	if (robot->estimatePose() == ERROR){
		return ERROR; 
	}
	
	if( (robot->pt_robot_potential_contact_global_frame_.row(0).array() < boundary_terrain_domain_.row(0).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(0).array() > boundary_terrain_domain_.row(1).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() < boundary_terrain_domain_.row(2).array()).any() ||
		(robot->pt_robot_potential_contact_global_frame_.row(1).array() > boundary_terrain_domain_.row(3).array()).any() ){
		
		return ERROR; 
	}
	
	// check if the robot body has penetrated a given terrain. If so, then return infinity cost. 
	Real height_terrain_specific_body_contact_pt; 
	for(int i=0; i<NUM_CONTACT_BODY; ++i){
		height_terrain_specific_body_contact_pt = map_elev_curr_->estimateTerrainHeight(robot_->getBodyPtContactGlobalFrame(0,i),robot_->getBodyPtContactGlobalFrame(1,i)); 
		
		if (height_terrain_specific_body_contact_pt > robot_->getBodyPtContactGlobalFrame(2,i) + TOL_CONTACT_CRITERION){
			return ERROR; 
		}
	}
	
	return OK; 		
}


template<class TConfig> bool Planner<TConfig>::collisionOccurredBWTwoConfigsWithRigidBodyRobot(const TConfig& config0, const TConfig& config1, YAW_MOTION_START_TYPE yaw_motion_config0, YAW_MOTION_END_TYPE yaw_motion_config1)
{
	// Consider the interpolating points between config0 and config1 which follow the template of the first yaw-angular motion and then the translational motion. 
	TConfig config_cog_interp(config0), config_contact_interp(config0);
	Real ang_yaw_end_config0=0.0, ang_yaw_diff_config0=0.0, ang_yaw_diff_config1=0.0, ang_yaw_diff_incr_config0=0.0, ang_yaw_diff_incr_config1=0.0; 
	int num_interp_steps_yaw_config0=0, num_interp_steps_yaw_config1=0; 
	
	switch(yaw_motion_config0){
		case NO_YAW_MOTION_START:
			ang_yaw_end_config0 = config0.q_[5]; 
			ang_yaw_diff_config0 = 0.0;
			num_interp_steps_yaw_config0 = 0;
			ang_yaw_diff_incr_config0 = 0.0; 
			break;
		
		case FWD_SHORT_START:			
			ang_yaw_end_config0 = modNPiPi(atan2f(config1.q_[1] - config0.q_[1], config1.q_[0] - config0.q_[0])); // yaw end angle for the config0. 
			ang_yaw_diff_config0 = modNPiPi(ang_yaw_end_config0 - config0.q_[5]); // the difference b/w the yaw end angle for the config0 after the initial rotational motion and the actual yaw angle of the config0
			num_interp_steps_yaw_config0 = 1 + floorf((fabsf(ang_yaw_diff_config0)/STEP_SIZE_INTERP_ANG_YAW) + TOL_NUM_ERR_FLOOR);
			ang_yaw_diff_incr_config0 = ang_yaw_diff_config0 / num_interp_steps_yaw_config0; 
			break;
			
		case FWD_LONG_START: 		
			ang_yaw_end_config0 = modNPiPi(atan2f(config1.q_[1] - config0.q_[1], config1.q_[0] - config0.q_[0]));		
			ang_yaw_diff_config0 = modNPiPi(ang_yaw_end_config0 - config0.q_[5]); 
			ang_yaw_diff_config0 = (sgn(ang_yaw_diff_config0)>0) ? -2*M_PI+ang_yaw_diff_config0 : 2*M_PI+ang_yaw_diff_config0; // update ang_yaw_diff_config0 with its complementary angle
			ang_yaw_diff_config0 = (ang_yaw_diff_config0>0)?fmod(ang_yaw_diff_config0+TOL_NUM_ERR_FLOOR,2*M_PI):fmod(ang_yaw_diff_config0-TOL_NUM_ERR_FLOOR,2*M_PI); // this maintains the sign and deals with numerical errors
			num_interp_steps_yaw_config0 = 1 + floorf((fabsf(ang_yaw_diff_config0)/STEP_SIZE_INTERP_ANG_YAW) + TOL_NUM_ERR_FLOOR);
			ang_yaw_diff_incr_config0 = ang_yaw_diff_config0 / num_interp_steps_yaw_config0; 
			break;
			
		case BWD_SHORT_START: 		
			ang_yaw_end_config0 = modNPiPi(atan2f(config0.q_[1] - config1.q_[1], config0.q_[0] - config1.q_[0])); // instead of computing the yaw angle from config0 to config1, compute the yaw angle from config1 to config0			
			ang_yaw_diff_config0 = modNPiPi(ang_yaw_end_config0 - config0.q_[5]); 
			num_interp_steps_yaw_config0 = 1 + floorf((fabsf(ang_yaw_diff_config0)/STEP_SIZE_INTERP_ANG_YAW) + TOL_NUM_ERR_FLOOR);
			ang_yaw_diff_incr_config0 = ang_yaw_diff_config0 / num_interp_steps_yaw_config0; 
			break;
			
		case BWD_LONG_START:		
			ang_yaw_end_config0 = modNPiPi(atan2f(config0.q_[1] - config1.q_[1], config0.q_[0] - config1.q_[0])); // instead of computing the yaw angle from config0 to config1, compute the yaw angle from config1 to config0		
			ang_yaw_diff_config0 = modNPiPi(ang_yaw_end_config0 - config0.q_[5]); 
			ang_yaw_diff_config0 = (sgn(ang_yaw_diff_config0)>0) ? -2*M_PI+ang_yaw_diff_config0 : 2*M_PI+ang_yaw_diff_config0; // update ang_yaw_diff_config0 with its complementary angle
			ang_yaw_diff_config0 = (ang_yaw_diff_config0>0)?fmod(ang_yaw_diff_config0+TOL_NUM_ERR_FLOOR,2*M_PI):fmod(ang_yaw_diff_config0-TOL_NUM_ERR_FLOOR,2*M_PI); // this maintains the sign and deals with numerical errors
			num_interp_steps_yaw_config0 = 1 + floorf((fabsf(ang_yaw_diff_config0)/STEP_SIZE_INTERP_ANG_YAW) + TOL_NUM_ERR_FLOOR);
			ang_yaw_diff_incr_config0 = ang_yaw_diff_config0 / num_interp_steps_yaw_config0; 
			break;
			
		default:
			ROS_ERROR("Wrong yaw motion type for config0"); 
			exit(-1); 
		
	}
	
	switch(yaw_motion_config1){
		case NO_YAW_MOTION_END:
			ang_yaw_diff_config1 = 0.0;
			num_interp_steps_yaw_config1 = 0; 
			break;
					
		case YAW_SHORT_MOTION_END:								
			ang_yaw_diff_config1 = modNPiPi(config1.q_[5] - ang_yaw_end_config0); // ang_yaw_end_config0 can correspond to any of the four yaw angle motion from config0:  FWD_SHORT_START, FWD_LONG_START, BWD_SHORT_START, or BWD_LONG_START. 
			num_interp_steps_yaw_config1 = 1 + floorf((fabsf(ang_yaw_diff_config1)/STEP_SIZE_INTERP_ANG_YAW) + TOL_NUM_ERR_FLOOR);
			ang_yaw_diff_incr_config1 = ang_yaw_diff_config1 / num_interp_steps_yaw_config1; 					
			break;
								
		case YAW_LONG_MOTION_END: 
			ang_yaw_diff_config1 = modNPiPi(config1.q_[5] - ang_yaw_end_config0); // ang_yaw_end_config0 can correspond to any of the four yaw angle motion from config0:  FWD_SHORT_START, FWD_LONG_START, BWD_SHORT_START, or BWD_LONG_START. 
			ang_yaw_diff_config1 = (sgn(ang_yaw_diff_config1)>0) ? -2*M_PI+ang_yaw_diff_config1 : 2*M_PI+ang_yaw_diff_config1; // update ang_yaw_diff_config1 with its complementary angle with either (2*pi-th) or (th-2*pi), depending on the sign of th.
			ang_yaw_diff_config1 = (ang_yaw_diff_config1>0)?fmod(ang_yaw_diff_config1+TOL_NUM_ERR_FLOOR,2*M_PI):fmod(ang_yaw_diff_config1-TOL_NUM_ERR_FLOOR,2*M_PI); // this maintains the sign and deals with numerical errors
			num_interp_steps_yaw_config1 = 1 + floorf((fabsf(ang_yaw_diff_config1)/STEP_SIZE_INTERP_ANG_YAW) + TOL_NUM_ERR_FLOOR);
			ang_yaw_diff_incr_config1 = ang_yaw_diff_config1 / num_interp_steps_yaw_config1; 					
			break;
		
		default:
			ROS_ERROR("Wrong yaw motion type for config1"); 
			exit(-1); 
	}
	
	// (1) First, the yaw-angular motion from the config0
	// Assumed that the configuration of the nearest node has already satisfied the collision test previously (therefore there is no need to check the collision for this configuration).
	for (int i=1; i<num_interp_steps_yaw_config0; ++i){ // i starts with value 1 b/c i=0 corresponds to config0, and this case has already been considered in the previous iterations of collision checking		
		// just looking at the robot's horizontal configuration
		config_cog_interp.q_[5] += ang_yaw_diff_incr_config0; // update the yaw angle		
		if (updateHorizontalRobotConfig(config_cog_interp, robot_) == ERROR){ // updated the member variable robot_ with its contact points updated. 		
			return true; // some of the potential contact points are outside of the map dimension, and this effect is considered as collision (with the map border, if you will). 
		}
				
		Eigen::Matrix<Real,2,4> pt_robot_body_extreme_global_frame; 				
		pt_robot_body_extreme_global_frame.col(0) = robot_->pt_robot_potential_contact_global_frame_.col(0).segment(0,2);   
		pt_robot_body_extreme_global_frame.col(1) = robot_->pt_robot_potential_contact_global_frame_.col(NUM_CONTACT_TRACK-1).segment(0,2); 
		pt_robot_body_extreme_global_frame.col(2) = robot_->pt_robot_potential_contact_global_frame_.col(NUM_CONTACT_TRACK+2*NUM_CONTACT_FLIPPER).segment(0,2); 
		pt_robot_body_extreme_global_frame.col(3) = robot_->pt_robot_potential_contact_global_frame_.col(2*(NUM_CONTACT_TRACK+NUM_CONTACT_FLIPPER)-1).segment(0,2);
		
		for(int j=0; j<pt_robot_body_extreme_global_frame.cols(); ++j){ // considering only the four extreme points that define the support polygon of the robot's rigid body model. 	
			config_contact_interp.q_.segment(0,2) = pt_robot_body_extreme_global_frame.col(j); // I could have used config_contact_interp.q_.segment(0,2), but the expression that I used is supposed to be executed faster since it is for a fixed-size block expression. 
			if (collisionOccurredBasedOnTerrainLabeling(config_contact_interp) == true){
				return true;
			}
		}
	}
	
	Real distance = (config1.q_.segment(0,2) - config0.q_.segment(0,2)).norm(); // distance in 2D
	int num_interp_steps = distance / size_step_cspace_discretization_; 
	
	// The following instruction is wrong for when the config1's yaw angle is different from the yaw angle required to translate from config0 to config1. 
	// Hence let us assign ang_yaw_end_config0 to config_cog_interp.q_[5] since this should always work regardless of the sense of rotating motions. 	
	config_cog_interp.q_[5] = ang_yaw_end_config0; 
	
	for(int i=1; i<num_interp_steps; ++i){ // i starts with value 1 b/c i=0 corresponds to the case of the last collision checking
		Real alpha = ((Real) i) / ((Real) num_interp_steps); 
				
		// just looking at the robot's horizontal configuration
		interpolateSampleLinearly(config0, config1, alpha, config_cog_interp); 		
		if (updateHorizontalRobotConfig(config_cog_interp, robot_) == ERROR){ // updated the member variable robot_ with its contact points updated. 		
			return true; // some of the potential contact points are outside of the map dimension, and this effect is considered as collision (with the map border, if you will). 
		}
				
		Eigen::Matrix<Real,2,4> pt_robot_body_extreme_global_frame; 			
		pt_robot_body_extreme_global_frame.col(0) = robot_->pt_robot_potential_contact_global_frame_.col(0).segment(0,2);   
		pt_robot_body_extreme_global_frame.col(1) = robot_->pt_robot_potential_contact_global_frame_.col(NUM_CONTACT_TRACK-1).segment(0,2); 
		pt_robot_body_extreme_global_frame.col(2) = robot_->pt_robot_potential_contact_global_frame_.col(NUM_CONTACT_TRACK+2*NUM_CONTACT_FLIPPER).segment(0,2); 
		pt_robot_body_extreme_global_frame.col(3) = robot_->pt_robot_potential_contact_global_frame_.col(2*(NUM_CONTACT_TRACK+NUM_CONTACT_FLIPPER)-1).segment(0,2); 
		
		for(int j=0; j<pt_robot_body_extreme_global_frame.cols(); ++j){ // considering only the four extreme points that define the support polygon of the robot's rigid body model. 

			config_contact_interp.q_.segment(0,2) = pt_robot_body_extreme_global_frame.col(j); // I could have used config_contact_interp.q_.segment(0,2), but the expression that I used is supposed to be executed faster since it is for a fixed-size block expression. 			
			
			if (collisionOccurredBasedOnTerrainLabeling(config_contact_interp) == true){			
				return true; 
			}
		}
	}
		
	config_cog_interp = config1; // for updating the x and y coordinates
	config_cog_interp.q_[5] = ang_yaw_end_config0; 
	
	// (3) Afterwards, the yaw-angular motion till the config1
	// Assumed that the configuration of the nearest node has already satisfied the collision test previously (therefore there is no need to check the collision for this configuration).
	for (int i=1; i<num_interp_steps_yaw_config1; ++i){
		
		// just looking at the robot's horizontal configuration
		config_cog_interp.q_[5] += ang_yaw_diff_incr_config1; // update the yaw angle		
		if (updateHorizontalRobotConfig(config_cog_interp, robot_) == ERROR){ // updated the member variable robot_ with its contact points updated. 		
			return true; // some of the potential contact points are outside of the map dimension, and this effect is considered as collision (with the map border, if you will). 
		}
				
		Eigen::Matrix<Real,2,4> pt_robot_body_extreme_global_frame; 		
		pt_robot_body_extreme_global_frame.col(0) = robot_->pt_robot_potential_contact_global_frame_.col(0).segment(0,2);   
		pt_robot_body_extreme_global_frame.col(1) = robot_->pt_robot_potential_contact_global_frame_.col(NUM_CONTACT_TRACK-1).segment(0,2); 
		pt_robot_body_extreme_global_frame.col(2) = robot_->pt_robot_potential_contact_global_frame_.col(NUM_CONTACT_TRACK+2*NUM_CONTACT_FLIPPER).segment(0,2); 
		pt_robot_body_extreme_global_frame.col(3) = robot_->pt_robot_potential_contact_global_frame_.col(2*(NUM_CONTACT_TRACK+NUM_CONTACT_FLIPPER)-1).segment(0,2);
		
		for(int j=0; j<pt_robot_body_extreme_global_frame.cols(); ++j){ // considering only the four extreme points that define the support polygon of the robot's rigid body model. 
			
			config_contact_interp.q_.segment(0,2) = pt_robot_body_extreme_global_frame.col(j); // I could have used config_contact_interp.q_.segment(0,2), but the expression that I used is supposed to be executed faster since it is for a fixed-size block expression. 			
		
			if (collisionOccurredBasedOnTerrainLabeling(config_contact_interp) == true){		
				return true;
			}
		}
	}
	
	// (4) Finally, check the collision for the config1 as well. This is done here b/c the distance b/w the two configs (config0 and config1) might not be a multiple of size_step_cspace_discretization_. 	
	if (collisionOccurredBasedOnTerrainLabeling(config1) == true){
	// if (collisionOccurredBW2DGenericPolygonsAndPoint(map_elev_curr_->obstacle_, config1) == true){
		return true; 
	}
	
	return false; 		
}


template<class TConfig> bool Planner<TConfig>::collisionOccurredAlongTwoConfigs(const TConfig& config0, const TConfig& config1, TConfig& config_lastly_valid)
{	
	Real distance, alpha; 
	int num_intermediate_steps; 
	TConfig config_interp(config0);
	
	distance = (config1.q_.segment(0,2) - config0.q_.segment(0,2)).norm(); // distance in 2D
	num_intermediate_steps = distance / size_step_cspace_discretization_; 
	
	for(int i=0; i<num_intermediate_steps; ++i){
		alpha = ((Real) i) / ((Real) num_intermediate_steps); 
		interpolateSampleLinearly(config0, config1, alpha, config_interp); 

		if (collisionOccurredBW2DGenericPolygonsAndPoint(map_elev_curr_->obstacle_,config_interp) == true){
			return true; 
		}
		config_lastly_valid = config_interp;
	}
	
	if (collisionOccurredBW2DGenericPolygonsAndPoint(map_elev_curr_->obstacle_,config1) == true){
		return true;
	}

	return false; 
}

// reserve the memory for the vectors involved in collision checking ahead in order to reduce the execution time (based on the results obtained from various profilers).
template<class TConfig> void Planner<TConfig>::reserveMemoryForVectorsInvolvedInCollisionChecking(const std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& obstacle)
{	
	max_size_obstacle_countering_pts_ = 0; 	
	num_total_obstacles_contouring_pts_ = 0; 
	for(int i=0; i<(int)obstacle.size(); ++i){
		num_total_obstacles_contouring_pts_ += (int) obstacle[i].size(); 
		if (max_size_obstacle_countering_pts_ < (int)obstacle[i].size()){
			max_size_obstacle_countering_pts_ = (int) obstacle[i].size(); 
		}
	}

	gradient_edge_obstacle_.resize(obstacle.size()); 
	midPoint_edge_obstacle_.resize(obstacle.size()); 
	for(int i=0; i<(int)obstacle.size(); ++i){
		gradient_edge_obstacle_[i].resize(obstacle[i].size()); 
		midPoint_edge_obstacle_[i].resize(obstacle[i].size()); 		
	}
	// The below one is just an intermediate vector, and, therefore, in order to reduce the employed memory space, I'll reserve memory for the largest case. 	
	diff_bw_config_and_midPointEdgeObstacle_.resize(max_size_obstacle_countering_pts_);	
	discriminant_edge_obstacle_.resize(max_size_obstacle_countering_pts_); 

	for(int i=0; i<(int)obstacle.size(); ++i){		
		for (int j=0; j<(int)obstacle[i].size()-1; ++j){
			// 1. compute its gradient vector
			gradient_edge_obstacle_[i][j] = Eigen::Matrix<Real,3,1>(-(obstacle[i][j+1](1)-obstacle[i][j](1)), obstacle[i][j+1](0) - obstacle[i][j](0), 0.0);
						
			// 2. compute its midpoint	
			midPoint_edge_obstacle_[i][j] = 0.5 * (obstacle[i][j+1] + obstacle[i][j]);
		}
		
		gradient_edge_obstacle_[i][obstacle[i].size()-1] = Eigen::Matrix<Real,3,1>(-(obstacle[i][0](1)-obstacle[i][obstacle[i].size()-1](1)), (obstacle[i][0](0) - obstacle[i][obstacle[i].size()-1](0)), 0.0); 
		midPoint_edge_obstacle_[i][obstacle[i].size()-1] = (0.5 * (obstacle[i][0] + obstacle[i][obstacle[i].size()-1])); 
	}	
}

template<class TConfig> int Planner<TConfig>::identifyRegionContainingStartConfig(const std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& obstacle, const TConfig& config)
{
	int ind_region_startConfig_belongsTo = -1; 
	Eigen::Matrix<Real,3,1> config_eigen(config.q_[0],config.q_[1],0.0); 
		
	for(int i=0; i<(int)obstacle.size(); ++i){
		
		Eigen::Matrix<Real,Eigen::Dynamic,1> dist_bw_config_and_edgeObstacle(obstacle[i].size()); 
		for(int j=0; j<(int)obstacle[i].size()-1; ++j){
			diff_bw_config_and_midPointEdgeObstacle_[j] = config_eigen - midPoint_edge_obstacle_[i][j]; 
			discriminant_edge_obstacle_[j] = (diff_bw_config_and_midPointEdgeObstacle_[j]).dot(gradient_edge_obstacle_[i][j]); 
			dist_bw_config_and_edgeObstacle[j] = distanceBWLineSegmentAndPoint(obstacle[i][j], obstacle[i][j+1], config_eigen); 
		}
		diff_bw_config_and_midPointEdgeObstacle_[obstacle[i].size()-1] = config_eigen - midPoint_edge_obstacle_[i][obstacle[i].size()-1]; 
		discriminant_edge_obstacle_[obstacle[i].size()-1] = (diff_bw_config_and_midPointEdgeObstacle_[obstacle[i].size()-1]).dot(gradient_edge_obstacle_[i][obstacle[i].size()-1]); 
		dist_bw_config_and_edgeObstacle[obstacle[i].size()-1] = distanceBWLineSegmentAndPoint(obstacle[i][obstacle[i].size()-1], obstacle[i][0], config_eigen); 
		
		int ind_closest_edge; 
		dist_bw_config_and_edgeObstacle.minCoeff(&ind_closest_edge); 
		if (discriminant_edge_obstacle_[ind_closest_edge] > 0){ // from July 15, 2014 b/c of the redefinition of the obstacle region (in particular, due to the new conversion from the map_traversability Eigen matrix to the map_traversability of Opencv matrix. 		
			ind_region_startConfig_belongsTo = i; 
			break; 
		}			
	}
	
	return ind_region_startConfig_belongsTo; 
	
}


template<class TConfig> bool Planner<TConfig>::collisionOccurredBasedOnTerrainLabeling(const TConfig& config)
{
	int ind_x = floor(((config.q_[0] - map_elev_curr_->xmin_)/map_elev_curr_->res_cell_x_) + TOL_NUM_ERR_FLOOR); 
	int ind_y = floor(((config.q_[1] - map_elev_curr_->ymin_)/map_elev_curr_->res_cell_y_) + TOL_NUM_ERR_FLOOR); 
	
	if (map_elev_curr_->map_label_terrain_(ind_x,ind_y) != map_elev_curr_->ind_label_region_startConfig_belongsTo_){
		return true;
	}
	
	return false; 	
}


template<class TConfig> bool Planner<TConfig>::collisionOccurredBW2DGenericPolygonsAndPoint(const std::vector<std::vector<Eigen::Matrix<Real,3,1> > >& obstacle, const TConfig& config)
{
	Eigen::Matrix<Real,3,1> config_eigen(config.q_[0],config.q_[1],0.0); 
	
	if (ind_region_startConfig_belongsTo_ == -1){ // the start configuration belongs to a classified region. 
		int i = ind_region_startConfig_belongsTo_; // a handy variable
		
		Eigen::Matrix<Real,Eigen::Dynamic,1> dist_bw_config_and_edgeObstacle(obstacle[i].size()); 
		
		for (int j=0; j<(int)obstacle[i].size()-1; ++j){
			// 3. compute its discriminant			
			diff_bw_config_and_midPointEdgeObstacle_[j] = config_eigen - midPoint_edge_obstacle_[i][j];						
			discriminant_edge_obstacle_[j] =  (diff_bw_config_and_midPointEdgeObstacle_[j]).dot(gradient_edge_obstacle_[i][j]); 
			
			// 4. compute the distance between the given configuration and each mid-point of the edges of a given obstacle			
			// I am returning the squared distance and not the actual distance to optimize the execution time (since the sqrt() operation takes excessive time).			
			dist_bw_config_and_edgeObstacle[j] = distanceBWLineSegmentAndPoint(obstacle[i][j], obstacle[i][j+1], config_eigen); 			
		}
		diff_bw_config_and_midPointEdgeObstacle_[obstacle[i].size()-1] = config_eigen - midPoint_edge_obstacle_[i][obstacle[i].size()-1];		
		discriminant_edge_obstacle_[obstacle[i].size()-1] = (diff_bw_config_and_midPointEdgeObstacle_[obstacle[i].size()-1]).dot(gradient_edge_obstacle_[i][obstacle[i].size()-1]); 
				
		// I am returning the squared distance and not the actual distance to optimize the execution time (since the sqrt() operation takes excessive time).		
		dist_bw_config_and_edgeObstacle[obstacle[i].size()-1] = distanceBWLineSegmentAndPoint(obstacle[i][obstacle[i].size()-1], obstacle[i][0], config_eigen); 									
				
		// 4. find the closest edge for the configuration in question
		int ind_closest_edge; 		
		dist_bw_config_and_edgeObstacle.minCoeff(&ind_closest_edge); // I am not interested in the minimum value but rather the index of the closest edge for the given configuration.			
		
		// 5. Assuming that the obstacle polygons are oriented in the CW, if the discriminant is positive, then the configuration in question is inside the obstacle in question. 		
		if (discriminant_edge_obstacle_[ind_closest_edge] < 0){ // this collision condition is precisely the opposite to the one used if the start configuration belongs to the "rest region"! 
			return true; 
		}		
	}else{ // the start configuration does not belong to any of the classified region (i.e., it belongs to "the rest region". 
		// For all the obstacles, 
		for(int i=0; i<(int)obstacle.size(); ++i){	
			
			Eigen::Matrix<Real,Eigen::Dynamic,1> dist_bw_config_and_edgeObstacle(obstacle[i].size()); 
			
			for (int j=0; j<(int)obstacle[i].size()-1; ++j){
				// 3. compute its discriminant			
				diff_bw_config_and_midPointEdgeObstacle_[j] = config_eigen - midPoint_edge_obstacle_[i][j];						
				discriminant_edge_obstacle_[j] =  (diff_bw_config_and_midPointEdgeObstacle_[j]).dot(gradient_edge_obstacle_[i][j]); 
				
				// 4. compute the distance between the given configuration and each mid-point of the edges of a given obstacle			
				// I am returning the squared distance and not the actual distance to optimize the execution time (since the sqrt() operation takes excessive time).			
				dist_bw_config_and_edgeObstacle[j] = distanceBWLineSegmentAndPoint(obstacle[i][j], obstacle[i][j+1], config_eigen); 			
			}
			diff_bw_config_and_midPointEdgeObstacle_[obstacle[i].size()-1] = config_eigen - midPoint_edge_obstacle_[i][obstacle[i].size()-1];		
			discriminant_edge_obstacle_[obstacle[i].size()-1] = (diff_bw_config_and_midPointEdgeObstacle_[obstacle[i].size()-1]).dot(gradient_edge_obstacle_[i][obstacle[i].size()-1]); 
						
			// I am returning the squared distance and not the actual distance to optimize the execution time (since the sqrt() operation takes excessive time).		
			dist_bw_config_and_edgeObstacle[obstacle[i].size()-1] = distanceBWLineSegmentAndPoint(obstacle[i][obstacle[i].size()-1], obstacle[i][0], config_eigen); 									
					
			// 4. find the closest edge for the configuration in question
			int ind_closest_edge; 		
			dist_bw_config_and_edgeObstacle.minCoeff(&ind_closest_edge); // I am not interested in the minimum value but rather the index of the closest edge for the given configuration.			
			
			// 5. Assuming that the obstacle polygons are oriented in the CW, if the discriminant is positive, then the configuration in question is inside the obstacle in question. 		
			if (discriminant_edge_obstacle_[ind_closest_edge] > 0){ // from July 15, 2014 b/c of the redefinition of the obstacle region (in particular, due to the new conversion from the map_traversability Eigen matrix to the map_traversability of Opencv matrix. 			
				return true; 
			}	
		} // for
	}// else
		
	return false;
}




// I am returning the squared distance and not the actual distance to optimize the execution time (since the sqrt() operation takes excessive time).
template<class TConfig> Real Planner<TConfig>::distanceBWLineSegmentAndPoint(Eigen::Matrix<Real,3,1> pt0_segment, Eigen::Matrix<Real,3,1> pt1_segment, Eigen::Matrix<Real,3,1> pt)
{
	Eigen::Matrix<Real,3,1> v,w;
	v = pt1_segment - pt0_segment; 
	w = pt - pt0_segment; 
	
	Real c1 = w.dot(v); 
	if (c1 <= 0){
		return (pt-pt0_segment).squaredNorm(); // to optimize the execution time		
	}
	
	Real c2 = v.dot(v); 
	if (c2 <= c1){
		return (pt-pt1_segment).squaredNorm(); // to optimize the execution time		
	}
	
	Real b = c1 / c2; 
	Eigen::Matrix<Real,3,1> pb = pt0_segment + b * v;
	return (pt-pb).squaredNorm(); // to optimize the execution time	

}


/**
 * This smoothPath() function smoothes the searched path.
 * @param num_iterations_for_smoothing_path The maximum number of iterations for smoothing a given path.
 * @param states_path_optimal_out This is the optimal path that this function returns as of result. 
 * @return The error status of this function is returned as of an integer value. 
*/
template<class TConfig> int Planner<TConfig>::smoothPath(int num_iterations_for_smooting_path, std::vector<TConfig>& configs_path_optimal, std::vector<TConfig>& config_smoothed_solution)
{	
	std::vector<TConfig> configs_path_curr, configs_path_smooth;

	if (configs_path_optimal.size() < 2){
		ROS_ERROR("No solution has been computed previously.");
		return ERROR;
	}

	if (configs_path_optimal.size() < 3){
		ROS_ERROR("The solution can not be smoothed because it has only three configurations.");
		return ERROR;
	}

	configs_path_curr.resize(configs_path_optimal.size());
	copy(configs_path_optimal.begin(), configs_path_optimal.end(), configs_path_curr.begin());
		
	int i1, i2, i_tmp;
			
	for(int i=0; i<num_iterations_for_smooting_path; ++i){		
		if (configs_path_curr.size() < 3){
			break; // no need to continue, // break the i-th for loop
		}

		if (configs_path_curr.size() > 3){
			i1 = (int) generateRandNumUniform(0.0, (Real) (configs_path_curr.size()-1));
			i2 = (int) generateRandNumUniform(0.0, (Real) (configs_path_curr.size()-1));
		}else{
			i1 = 0; 
			i2 = 2; 
		}

		// Make sure that i1<i2
		if(i2 < i1){
			i_tmp = i1;
			i1 = i2;
			i2 = i_tmp;
		}else{
			if(i1 == i2){
				if(i1 == (int)(configs_path_curr.size())-2){
					i1--;
				}else{
					i2++;
				}
			}
		}
		
		configs_path_smooth.clear();
				
		if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[0], robot_) == OK){

			configs_path_curr[0].q_ = robot_->pose_; 
			configs_path_smooth.push_back(configs_path_curr[0]);
		}else{
			continue;
		}
		
		for(int j=1; j<=i1; ++j){
			configs_path_curr[j].q_[5] = modNPiPi(atan2f(configs_path_curr[j].q_[1] - configs_path_curr[j-1].q_[1], configs_path_curr[j].q_[0] - configs_path_curr[j-1].q_[0]));

			if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[j], robot_) == OK){
				configs_path_curr[j].q_ = robot_->pose_; 
				configs_path_smooth.push_back(configs_path_curr[j]);
			}else{
				continue;
			}
		}
					
		configs_path_curr[i2].q_[5] = modNPiPi(atan2f(configs_path_curr[i2].q_[1] - configs_path_curr[i1].q_[1], configs_path_curr[i2].q_[0] - configs_path_curr[i1].q_[0]));
		if(this->isTraversableBWTwoConfigs(configs_path_curr[i1], configs_path_curr[i2], TREE_EXTENSION, START_TREE) == ERROR){ // since the smoothed path should start from the start node, the tree is considered to be of START_TREE
			continue; // abandon this iteration since there can not be a connection b/w the new node and the goal node. 
		}		

		// I need to change the yaw angle for the i2-th configuration since its previous configuration has been changed to the i1-th configuration.
		// In addition, because the robot pose is estimated after having changed the yaw angle I need to evaluate the pose
		if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[i2], robot_) == OK){
			configs_path_curr[i2].q_ = robot_->pose_; 
			
			configs_path_smooth.push_back(configs_path_curr[i2]);
			
			int err = OK; 
			for(int j=i2+1; j<(int)configs_path_curr.size(); ++j){
				configs_path_curr[j].q_[5] = modNPiPi(atan2f(configs_path_curr[j].q_[1] - configs_path_curr[j-1].q_[1], configs_path_curr[j].q_[0] - configs_path_curr[j-1].q_[0]));
	
				if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[j], robot_) == OK){
					configs_path_curr[j].q_ = robot_->pose_; 
					configs_path_smooth.push_back(configs_path_curr[j]);
				}else{
					err = ERROR; 
					break;
				}
			}
									
			if (err == ERROR){
				continue; 
			}
			
			// states_path_curr is modified only if all the above conditions are satisfied.
			configs_path_curr.resize(configs_path_smooth.size());
			copy(configs_path_smooth.begin(), configs_path_smooth.end(), configs_path_curr.begin());
		}else{
			continue; 
		}
	}

	// See if I can smooth the last three steps. This case is special b/c the connection between the goal and the previous one is somewhat forced.
	// Smooth the path if only the changes of height, pitch and roll are smaller than certain values. That is smooth only if the terrain is flat. If not do not smooth the path b/c this will omit many details on the estimated pose of the robot.
	int num_min_to_smooth_last_nodes = 4;
	if ((int)configs_path_curr.size() > num_min_to_smooth_last_nodes){ // if this condition is not satisfied, then the number of nodes is so small that there is no reason why I want to smooth the last steps of the path.
		i1 = configs_path_curr.size()-(num_min_to_smooth_last_nodes+1);
		i2 = configs_path_curr.size()-1;

		configs_path_smooth.clear();

		if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[0], robot_) == OK){

			configs_path_curr[0].q_ = robot_->pose_; 
			configs_path_smooth.push_back(configs_path_curr[0]);
		}else{
			goto END_OF_SMOOTHING; 
		}				

		for(int j=1; j<=i1; ++j){
			configs_path_curr[j].q_[5] = modNPiPi(atan2f(configs_path_curr[j].q_[1] - configs_path_curr[j-1].q_[1], configs_path_curr[j].q_[0] - configs_path_curr[j-1].q_[0]));

			if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[j], robot_) == OK){
				configs_path_curr[j].q_ = robot_->pose_; 
				configs_path_smooth.push_back(configs_path_curr[j]);
			}else{
				goto END_OF_SMOOTHING; 
			}					
		}
						
		configs_path_curr[i2].q_[5] = modNPiPi(atan2f(configs_path_curr[i2].q_[1] - configs_path_curr[i1].q_[1], configs_path_curr[i2].q_[0] - configs_path_curr[i1].q_[0]));
		if(this->isTraversableBWTwoConfigs(configs_path_curr[i1], configs_path_curr[i2], TREE_EXTENSION, START_TREE) == ERROR){ // since the smoothed path should start from the start node, the tree is considered to be of START_TREE				
			goto END_OF_SMOOTHING; 
		}
	
		// I need to change the yaw angle for the i2-th configuration since its previous configuration has been changed to the i1-th configuration.
		// In addition, because the robot pose is estimated after having chnaged the yaw angle I need to evaluate the pose
		if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[i2], robot_) == OK){
			configs_path_curr[i2].q_ = robot_->pose_; 
			configs_path_smooth.push_back(configs_path_curr[i2]);
			
			for(int j=i2+1; j<(int)configs_path_curr.size(); ++j){
				configs_path_curr[j].q_[5] = modNPiPi(atan2f(configs_path_curr[j].q_[1] - configs_path_curr[j-1].q_[1], configs_path_curr[j].q_[0] - configs_path_curr[j-1].q_[0]));
	
				if (updateFullRobotConfigFromItsHorionzontalConfig(configs_path_curr[j], robot_) == OK){
					configs_path_curr[j].q_ = robot_->pose_; 
					configs_path_smooth.push_back(configs_path_curr[j]);
				}else{
					goto END_OF_SMOOTHING; 
				}
			}
			// configs_path_curr is modified only if all the above conditions are satisfied.
			configs_path_curr.resize(configs_path_smooth.size());
			copy(configs_path_smooth.begin(), configs_path_smooth.end(), configs_path_curr.begin());
			
		}else{
			goto END_OF_SMOOTHING; 
	
		}
	}
	
	END_OF_SMOOTHING: 	
	config_smoothed_solution.resize(configs_path_curr.size());
	copy(configs_path_curr.begin(), configs_path_curr.end(), config_smoothed_solution.begin());
	
	return OK;	
}	


template<class TConfig> int Planner<TConfig>::isTraversableBWTwoConfigs(TConfig& config0, TConfig& config1, CONFIG_CONNECTION_TYPE config_connection_type, GRAPH_TYPE type_tree=START_TREE) // config1 is not made constant b/c its yaw angle might change if robot backward motion is chosen.
{
	is_robot_motion_forward_ = true; // by default
	// bool is_robot_motion_forward = true; 		
	if(do_collision_check_ == true){
		switch(config_connection_type){
			case TREE_EXTENSION: 
				switch(type_tree){
					case START_TREE: 
																									
						if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,FWD_SHORT_START,NO_YAW_MOTION_END) == true){ // forward with short yaw angular motion for config0 and short yaw motion for config1
																							
							if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,FWD_LONG_START,NO_YAW_MOTION_END) == true){ // forward with long yaw angular motion for config0 and short yaw motion for config1
								is_robot_motion_forward_ = false; 									
																										
								if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_SHORT_START,YAW_SHORT_MOTION_END) == true){ // backward with short yaw angular motion for config0 and short yaw motion for config1
									
									if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_LONG_START,YAW_LONG_MOTION_END) == true){ // backward with long yaw angular motion for config0 and short yaw motion for config1
										config1.q_[5] = modNPiPi(atan2f(config0.q_[1] - config1.q_[1], config0.q_[0] - config1.q_[0])); 
										
										// Below code is commented out because it is computationally expensive
										//////////////////////////////////////////////////////////////////////
										// ESTIMATE THE ROBOT POSE TO KNOW WHETHER THE ROBOT FACES BACKWARD TO THE STAIR; IF SO SUCH PATH IS NOT FEASIBLE!		
										// BUT, THIS CAN BE VERY TIME CONSUMING
										//	if (this->updateFullRobotConfigFromItsHorionzontalConfig(config1, this->robot_) == ERROR){ // update the robot configuration	
										//	// if (this->updateRobotConfig(config_new, this->robot_) == ERROR){ // update the robot configuration	
										//		return ERROR; 		
										//	}
										//	config1.q_ = this->robot_->getPose();												
										//////////////////////////////////////////////////////////////////////
																														
										if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_SHORT_START,NO_YAW_MOTION_END) == true){ // backward with short yaw angular motion for config0 and short yaw motion for config1

											if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_LONG_START,NO_YAW_MOTION_END) == true){ // backward with long yaw angular motion for config0 and short yaw motion for config1
												return ERROR; 
											}
										}
									}
								}
							}
						}
						break; 
						
					case GOAL_TREE: 

						if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,NO_YAW_MOTION_START,YAW_SHORT_MOTION_END) == true){ // forward with short yaw angular motion for config0 and short yaw motion for config1
							if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,NO_YAW_MOTION_START,YAW_LONG_MOTION_END) == true){ // forward with short yaw angular motion for config0 and long yaw motion for config1
								is_robot_motion_forward_ = false; 
								// is_robot_motion_forward = false; 
								config0.q_[5] = modNPiPi(atan2f(config0.q_[1] - config1.q_[1], config0.q_[0] - config1.q_[0])); 
								
								// Below code is commented out because it is computationally expensive
								//////////////////////////////////////////////////////////////////////
								// ESTIMATE THE ROBOT POSE TO KNOW WHETHER THE ROBOT FACES BACKWARD TO THE STAIR; IF SO SUCH PATH IS NOT FEASIBLE!		
								// BUT, THIS CAN BE VERY TIME CONSUMING									
								//	if (this->updateFullRobotConfigFromItsHorionzontalConfig(config0, this->robot_) == ERROR){ // update the robot configuration	
								//	// if (this->updateRobotConfig(config_new, this->robot_) == ERROR){ // update the robot configuration	
								//		return ERROR; 		
								//	}
								//	config0.q_ = this->robot_->getPose();	
								//////////////////////////////////////////////////////////////////////
								
								if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,NO_YAW_MOTION_START,YAW_SHORT_MOTION_END) == true){ // backward with short yaw angular motion for config0 and short yaw motion for config1
									if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,NO_YAW_MOTION_START,YAW_LONG_MOTION_END) == true){ // forward with short yaw angular motion for config0 and long yaw motion for config1
										return ERROR; 
									}
								}
							}
						}
						break; 
						
					default: 
						ROS_ERROR("isTraversableBWTwoConfigs :: Wrong tree type!"); 
						return ERROR;
				}
				break;
				
			case TWO_TREES_CONNECTION: 

				// check if the forward/backward yaw-angular motion with CW/CCW are possible from both config0 and config1. Here I assume that config1 has already the right yaw angle. 
				if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,FWD_SHORT_START,YAW_SHORT_MOTION_END) == true){ // forward with short yaw angular motion for config0 and short yaw motion for config1
					if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,FWD_SHORT_START,YAW_LONG_MOTION_END) == true){ // forward with short yaw angular motion for config0 and long yaw motion for config1
						if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,FWD_LONG_START,YAW_SHORT_MOTION_END) == true){ // forward with long yaw angular motion for config0 and short yaw motion for config1
							if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,FWD_LONG_START,YAW_LONG_MOTION_END) == true){ // forward with long yaw angular motion for config0 and long yaw motion for config1
								is_robot_motion_forward_ = false; 
								// is_robot_motion_forward = false; 
								if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_SHORT_START,YAW_SHORT_MOTION_END) == true){ // backward with short yaw angular motion for config0 and short yaw motion for config1
									if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_SHORT_START,YAW_LONG_MOTION_END) == true){ // backward with short yaw angular motion for config0 and long yaw motion for config1
										if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_LONG_START,YAW_SHORT_MOTION_END) == true){ // backward with long yaw angular motion for config0 and short yaw motion for config1
											if(collisionOccurredBWTwoConfigsWithRigidBodyRobot(config0,config1,BWD_LONG_START,YAW_LONG_MOTION_END) == true){ // backward with long yaw angular motion for config0 and long yaw motion for config1
												return ERROR; 
											}
										}
									}
								}
							}
						}
					}
				}
				break; 
				
			default: 
				ROS_ERROR("isTraversableBWTwoConfigs :: Wrong configuration connection type!"); 
				return ERROR;
		}
	}
	
	return OK; 	
}

/**
* This planFlipperMotionFromPitchPath() function plans the flipper motion based on the changes of the pitch angle obtained from the searched path.
*/
template<class TConfig> void Planner<TConfig>::planFlipperMotionFromPitchPath(std::vector<TConfig>& config_solution)
{	
	std::vector<Real> pitch_filt;
	
    const Real ANG_NOISE_THRESHOLD = 1e-2;

    const Real ANG_PITCH_THRESHOLD = deg2rad(8); // 0.2094rad == 12deg
	
	const int NZEROS = 1;
	const int NPOLES = 1;

	// 0.1 [-]
    const Real GAIN = 4.077683537e+00;
    const Real FREQ_CUTOFF = 0.5095254495;

	Real xv[NZEROS+1], yv[NPOLES+1];

	xv[0] = yv[0] = 0.0;
	xv[1] = config_solution[0].q_[4] / GAIN;
	yv[1] = (xv[0] + xv[1]) + (FREQ_CUTOFF * yv[0]);
	pitch_filt.push_back(yv[1]);

	for(int i=1; i<(int)config_solution.size(); ++i){		
		xv[0] = xv[1];
		xv[1] = config_solution[i].q_[4] / GAIN;
		yv[0] = yv[1];
		yv[1] = (xv[0] + xv[1]) + (FREQ_CUTOFF * yv[0]);
		pitch_filt.push_back(yv[1]);
	}
	
		
	int slope_curve = 0;
	int ind_first_interior_extreme_pt = 0;
	for(int i=1; i<(int)config_solution.size(); ++i){
		if (slope_curve == 0){
			if (pitch_filt[i] - pitch_filt[i-1] > ANG_NOISE_THRESHOLD){
				slope_curve = 1; // positive slope
				ind_first_interior_extreme_pt = i-1;
				break;
			}else{
				if (pitch_filt[i] - pitch_filt[i-1] < -ANG_NOISE_THRESHOLD){
					slope_curve = -1; // negative slope
					ind_first_interior_extreme_pt = i-1;
					break;
				}
			}
		}else{
			if (slope_curve == -1){
				if (pitch_filt[i] - pitch_filt[i-1] > ANG_NOISE_THRESHOLD){
					slope_curve = 1; // positive slope
					ind_first_interior_extreme_pt = i-1;
					break;
				}else{
					if (abs(pitch_filt[i] - pitch_filt[i-1]) < ANG_NOISE_THRESHOLD){
						slope_curve = 0; // flat
						ind_first_interior_extreme_pt = i-1;
						break;
					}
				}
			}else{
				if (slope_curve == 1){
					if (pitch_filt[i] - pitch_filt[i-1] < -ANG_NOISE_THRESHOLD){
						slope_curve = -1; // negative slope
						ind_first_interior_extreme_pt = i-1;
						break;
					}else{
						if (abs(pitch_filt[i] - pitch_filt[i-1]) < ANG_NOISE_THRESHOLD){
							slope_curve = 0; // flat
							ind_first_interior_extreme_pt = i-1;
							break;
						}
					}
				}
			}
		}
	} // for

	std::vector<int> ind_extreme_pts;
	ind_extreme_pts.push_back(0); // first index
	ind_extreme_pts.push_back(ind_first_interior_extreme_pt);

	for(int i=ind_first_interior_extreme_pt+1; i<(int)config_solution.size(); ++i){
		if (slope_curve == 0){
			if(pitch_filt[i] - pitch_filt[i-1] > ANG_NOISE_THRESHOLD){
				slope_curve = 1;
				ind_extreme_pts.push_back(i-1);
			}else{
				if(pitch_filt[i] - pitch_filt[i-1] < -ANG_NOISE_THRESHOLD){
					slope_curve = -1;
					ind_extreme_pts.push_back(i-1);
				}
			}
		}else{
			if (slope_curve == -1){
				if(pitch_filt[i] - pitch_filt[i-1] > ANG_NOISE_THRESHOLD){
					slope_curve = 1;
					ind_extreme_pts.push_back(i-1);
				}else{
					if(abs(pitch_filt[i] - pitch_filt[i-1]) < ANG_NOISE_THRESHOLD){
						slope_curve = 0; // flat
						ind_extreme_pts.push_back(i-1);
					}
				}
			}else{
				if (slope_curve == 1){
					if(pitch_filt[i] - pitch_filt[i-1] < -ANG_NOISE_THRESHOLD){
						slope_curve = -1;
						ind_extreme_pts.push_back(i-1);
					}else{
						if(abs(pitch_filt[i] - pitch_filt[i-1]) < ANG_NOISE_THRESHOLD){
							slope_curve = 0; // flat
							ind_extreme_pts.push_back(i-1);
						}
					}
				}
			}
		}
	}// for

	ind_extreme_pts.push_back(config_solution.size()-1); // last point's index

	std::vector<int> ind_extreme_pts_tmp;
	ind_extreme_pts_tmp.push_back(ind_extreme_pts[0]);

	for(int i=1; i <(int)ind_extreme_pts.size()-2; ++i){
        if (fabsf(pitch_filt[ind_extreme_pts[i]]-pitch_filt[ind_extreme_pts[i-1]]) < ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[i+1]]-pitch_filt[ind_extreme_pts[i]]) < ANG_PITCH_THRESHOLD){
            continue;
        }else{
            if (fabsf(pitch_filt[ind_extreme_pts[i]]-pitch_filt[ind_extreme_pts[i-1]]) >= ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[i+1]]-pitch_filt[ind_extreme_pts[i]]) >= ANG_PITCH_THRESHOLD){
                ind_extreme_pts_tmp.push_back(ind_extreme_pts[i]);
            }else{
                if (fabsf(pitch_filt[ind_extreme_pts[i]]-pitch_filt[ind_extreme_pts[i-1]]) >= ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[i+1]]-pitch_filt[ind_extreme_pts[i]]) < ANG_PITCH_THRESHOLD){
                    if (fabsf(pitch_filt[ind_extreme_pts[i+1]]-pitch_filt[ind_extreme_pts[i]]) < ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[i+2]]-pitch_filt[ind_extreme_pts[i+1]]) >= ANG_PITCH_THRESHOLD){
                        continue;
                    }else{
                        ind_extreme_pts_tmp.push_back(ind_extreme_pts[i]);
                    }
                }else{
                    if((fabs(pitch_filt[ind_extreme_pts[i]]-pitch_filt[ind_extreme_pts[i-1]]) < ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[i+1]]-pitch_filt[ind_extreme_pts[i]]) >= ANG_PITCH_THRESHOLD) &&
                       (fabs(pitch_filt[ind_extreme_pts[i+1]]-pitch_filt[ind_extreme_pts[i]]) >= ANG_PITCH_THRESHOLD) ){

                        ind_extreme_pts_tmp.push_back(ind_extreme_pts[i]);
                    }
                }
            }
        }
	}//for

	if (fabsf(pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-2]]-pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-3]]) >= ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-1]]-pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-2]]) >= ANG_PITCH_THRESHOLD) {
        ind_extreme_pts_tmp.push_back(ind_extreme_pts[ind_extreme_pts.size()-2]);
	}else{
        if (fabsf(pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-2]]-pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-3]]) < ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-1]]-pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-2]]) >= ANG_PITCH_THRESHOLD){
            ind_extreme_pts_tmp.push_back(ind_extreme_pts[ind_extreme_pts.size()-2]);
        }else{
            if (fabsf(pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-2]]-pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-3]]) >= ANG_PITCH_THRESHOLD && fabsf(pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-1]]-pitch_filt[ind_extreme_pts[ind_extreme_pts.size()-2]]) < ANG_PITCH_THRESHOLD){
                ind_extreme_pts_tmp.push_back(ind_extreme_pts[ind_extreme_pts.size()-2]);
            }
        }
	}
	ind_extreme_pts_tmp.push_back(ind_extreme_pts[ind_extreme_pts.size()-1]);

	std::vector<Real> ang_diff;
	for(int i=0; i<(int)ind_extreme_pts_tmp.size()-1; ++i){
        ang_diff.push_back(pitch_filt[ind_extreme_pts_tmp[i+1]]-pitch_filt[ind_extreme_pts_tmp[i]]);
	}

	Real ang_abs_upward_motion = deg2rad(45); // Due to the fact that the amount of upward motion is different from the downward motion for a same absolute value of commanded flipper angle.
	Real ang_abs_downward_motion = deg2rad(45); // Due to the fact that the amount of upward motion is different from the downward motion for a same absolute value of commanded flipper angle.

    int ind_ang_diff = 1;
    for(int i=0; i<(int)config_solution.size(); ++i){
        if (i > ind_extreme_pts_tmp[ind_ang_diff]){
            ind_ang_diff++;
        }
        
		if (ang_diff[ind_ang_diff-1] > ANG_PITCH_THRESHOLD){

			config_solution[i].q_[6] = ang_abs_downward_motion; // lower the flippers
			const int IND_ANTICIPATION_DOWNWARD = 3; 
			if(i-IND_ANTICIPATION_DOWNWARD > 0){ // anticipate the raising motion 4 seconds before
				for(int j=i-IND_ANTICIPATION_DOWNWARD; j<i; ++j){
					config_solution[j].q_[6] = ang_abs_downward_motion; // raise the flippers					 
				}
			}else{
				if(i>2){
					for(int j=2; j<i; ++j){
						config_solution[j].q_[6] = ang_abs_downward_motion; // raise the flippers
					}
				}
			}
			
		}else{
			if (ang_diff[ind_ang_diff-1] < -ANG_PITCH_THRESHOLD){
				config_solution[i].q_[6] = -ang_abs_upward_motion; // raise the flippers
				// ANTICIPATORY ACTIONS
				const int IND_ANTICIPATION_UPWARD = 7; 
				if(i-IND_ANTICIPATION_UPWARD > 0){ // anticipate the raising motion 4 seconds before
					for(int j=i-IND_ANTICIPATION_UPWARD; j<i; ++j){
						config_solution[j].q_[6] = -ang_abs_upward_motion; // raise the flippers						
					}
				}else{
					if(i>2){
						for(int j=2; j<i; ++j){
							config_solution[j].q_[6] = -ang_abs_upward_motion; // raise the flippers
						}
					}
				}
			}else{
				config_solution[i].q_[6] = ANG_FLIPPER_INIT; // keep them flat
			}
		}	   
	} // for
}

/**
 * This planFlipperMotionBasedOnPitchMotionOnly() function plans the flipper motion based on the changes of the pitch angle.
 * The flippers' motion is planned using relative flipper angles. This method is implemented after realizing that he upward and downward motions are different with a possible reason of the gravity acting on the flippers.
 * It is observed that the flippers move more downwardly than upwardly when a same absolute value of flipper angle motion is commanded.
 * @return The error status of this function is returned as of an integer value. 
*/
template<class TConfig> int Planner<TConfig>::planFlipperMotionBasedOnPitchMotionOnly(std::vector<TConfig>& config_solution)
{	

	float ang_flipper = ANG_FLIPPER_INIT;

	float ang_abs_upward_motion = deg2rad(30); // deg2rad(45); // Due to the fact that the amount of upward motion is different from the downward motion for a same absolute value of commanded flipper angle.
	float ang_abs_downward_motion = deg2rad(30); // deg2rad(45); // assuming that upward and downward flipper angle motion range are exactly the same. 	
	
	float ang_pitch_curr = config_solution[0].q_[4]; // the same as robot_->pose_(4);
	float ang_pitch_next = 0.0; // Initialize the pitch angle to a certain value.
	int count_consecutive_flipper_motion = 0;
	int type_flipper_motion = 0; // -1: downward motion to reduce impact; 0: no flipper set to its default value; 1: upward motion to avoid collision.

	for(int i=1; i<(int)config_solution.size(); ++i){
		
		ang_pitch_next = config_solution[i].q_[4]; // the same as, robot_->pose_(4);

		// plan flipper motion based on the robot's body pitch angle change.
		if (!count_consecutive_flipper_motion){
			if ((ang_pitch_next-ang_pitch_curr) > deg2rad(15)){ // downward body motion is expected
				if(type_flipper_motion != -1){ // if previous motion was not downward (i.e., upward or flat).
					ang_flipper += ang_abs_downward_motion; //45 //! move the flipper angle downward to reduce the body impact with the ground. Note: this value is smaller than the upward motion range b/c it seems to be easier to lower the flippers than to raise them.
				}// otherwise just keep the previous downward motion				
				count_consecutive_flipper_motion = 3; // keep the new flipper angle for a certain amount of time.
				config_solution[i].q_[6] = ang_flipper; // [rad]
				type_flipper_motion = -1;

			}else if((ang_pitch_next-ang_pitch_curr) < -deg2rad(5)){ // upward body motion is expected			
				if(type_flipper_motion != 1){ // if previous motion was not upward (i.e., downward or flat).
					ang_flipper -= ang_abs_upward_motion; // -30 //! move the flipper angle upward to avoid the collision b/w the robot and the terrain; this value is larger than when moving the flippers downward b/c of the existing asymmetry in the motion. The amount of downward motion is larger than that of upward motion when the same abolute angular motion range is applied.
				} // otherwise just keep the previuos upward motion.				
				count_consecutive_flipper_motion = 2; // keep the new flipper angle for a certain amount of time.
				if((i-IND_FLIPPER_MOTION_ANTICIPATION) > -1){
					config_solution[i-IND_FLIPPER_MOTION_ANTICIPATION].q_[6] = ang_flipper; // [rad], overwrite the previous flipper angle value in order to respond to the collision with anticipation.
				}
				config_solution[i].q_[6] = ang_flipper + ang_abs_downward_motion; // This will be the angle after lowering the already-raised flipper angle.				
				type_flipper_motion = 1;
			}else{
				switch(type_flipper_motion){
					case 0: // previous motion being flat						
						break;
					case 1: // previous motion being upward
						ang_flipper += ang_abs_downward_motion;
						break;
					case -1: // previous motion being downward
						ang_flipper -= ang_abs_upward_motion;
						break;
				}
				config_solution[i].q_[6] = ang_flipper; // [rad]
				type_flipper_motion = 0;
			}
		}else{ // non-zero count_consecutive_flipper_motion

			// Just keep the previous value
			config_solution[i].q_[6] = ang_flipper;

			// Decrease the counter so that if the counter becomes zero, this method will see if a different flipper angle should be assigned.
			count_consecutive_flipper_motion--;
		}

		ang_pitch_curr = ang_pitch_next;

	}// for

	return OK;	
}

// } // end of the namespace fraudo

#endif
