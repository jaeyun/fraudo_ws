/**
 * @file RRTRelatedAlgorithms.hpp
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         March 29 2014; 
 * Lastly modified: April 22 2015; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2015>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#ifndef RRTRELATED_ALGORITHMS_H_
#define RRTRELATED_ALGORITHMS_H_

#include <chrono>

const Real ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS = 0.30; // 10deg=0.17453292519943295, maximum allowed backward pitch angle motion
const Real ANG_ROLL_MAX_SAFETY = 1.5; // 20deg 


// namespace fraudo{
	
struct ResultsToBeStored{
	bool success_planning; 
	int num_iterations; 
	int num_nodes; 	
	Real time_elapsed;
	Real length_path; 
	Real cost_mechanical_work; 
	Real cost_stability; 
	Real cost_energy; 
	Real cost_stability_g1; 
	Real cost_stability_g2; 
	Real cost_stability_g3; 
};

template<class TConfig> class RRT : public Planner<TConfig>
{
	protected:
		
		ros::NodeHandle n_; 
		
		ros::Publisher pub_marker_nodes_startTree_;
		ros::Publisher pub_marker_edges_startTree_;
				
		Real size_step_tree_expansion_; // the tree expansion step size and is the epsilon used in LaValle's paper. This value is usually a multiple of size_step_cspace_discretization_. 
		
		Real radius_ball_goal_region_;  
		
		Graph<TConfig> tree_start_; 
		GraphNode<TConfig> *node_start_, *node_goal_; 
		
		COST_FUNCTION_TYPE cost_type_; 
				
		TConfig sampleRandConfig(); 		
		virtual int buildTree(TConfig& config_start, TConfig& config_goal, const SimulationParamInput& param_input_simulation, ResultsToBeStored& results_to_be_stored);
		RRT_RELATED_TREE_EXTENSION_RESULT extend(Graph<TConfig>& tree, const TConfig& config_rand, GraphNode<TConfig>*& node_new);
		int obtainNewConfig(TConfig& config_nearest, const TConfig& config_rand, TConfig& config_new);
						
		int returnPath(std::vector<TConfig>& config_path); 
				
		void displayNodesStartTree(); 
		void displayEdgesStartTree(); 
						
		Real computeMinimalMechanicalworkCost(const TConfig& config0, const TConfig& config1); 
		Real computeStabilityCostBWTwoConfigs(const TConfig& config0, const TConfig& config1); // config1 is not constant anymore because its configuration is updated since this is the only place where updateFullRobotConfigFromItsHorionzontalConfig() function is called the sake of the entire program's efficiency 
		Real computeStabilityCostRobot(const Robot& robot); // config1 is not constant anymore because its configuration is updated since this is the only place where updateFullRobotConfigFromItsHorionzontalConfig() function is called the sake of the entire program's efficiency 
		Real computeEnergyCostBWTwoConfigs(const TConfig& config0, const TConfig& config1);

	public: 
		Real (*costFunction_)(const TConfig& config1, const TConfig& config2); 
	
		RRT(); 
		RRT(Robot* robot, Robot* robot_goal, bool is_map_static_global, Real size_step_cspace_discretization, COST_FUNCTION_TYPE cost_type, Real (*distanceFunction)(const TConfig&, const TConfig&)=NULL);
		~RRT(); 

		int generateTrajectory(Terrain* map_elev_curr, const geometry_msgs::PoseStamped& poseStamped_start, const geometry_msgs::PoseStamped& poseStamped_goal, const SimulationParamInput& param_input_simulation);	
		
		int studyInfluencePtsContactTimeSampling(Terrain* map_elev_curr, const geometry_msgs::PoseStamped& poseStamped_start, const geometry_msgs::PoseStamped& poseStamped_goal, const SimulationParamInput param_input_simulation);
		
		Eigen::Matrix<Real,4,1> searchBestArmCoMUsingCMA(Robot& robot); 			
}; 

template<class TConfig> class BiRRT : public virtual RRT<TConfig> // I need to make the dependence to be virtual in order to avoid that BiTRRT makes two copies of the member variables and member methods of the class RRT (b/c BiTRRT depends on BiRRT and TRRT and both BiRRT and TRRT depend on RRT). If I don't add the tag "virtual", then I get the ambiguity error during the compilation time. 
{
	protected:	
	
		ros::NodeHandle n_; 
		
		ros::Publisher pub_marker_nodes_goalTree_;
		ros::Publisher pub_marker_edges_goalTree_; 
		
		Graph<TConfig> tree_goal_; 
	
		int buildTree(TConfig& config_start, TConfig& config_goal, const SimulationParamInput& param_input_simulation, ResultsToBeStored& results_to_be_stored); 		
		RRT_RELATED_TREE_EXTENSION_RESULT extend(Graph<TConfig>& tree, const TConfig& config_rand, GraphNode<TConfig>*& node_new);
		int obtainNewConfig(Graph<TConfig>& tree, GraphNode<TConfig>* & node_nearest, const TConfig& config_rand, TConfig& config_new);
		
		int returnPath(Graph<TConfig>& tree0, Graph<TConfig>& tree1, GraphNode<TConfig>& node_tree0, GraphNode<TConfig>& node_tree1, std::vector<TConfig>& config_path); 
		RRT_RELATED_TREE_EXTENSION_RESULT connectTrees(Graph<TConfig>& tree1, GraphNode<TConfig>& node_new_tree0, GraphNode<TConfig>*& node_nearest_tree1); 
		
		void displayNodesGoalTree(); 
		void displayEdgesGoalTree(); 
		
	public: 
						
		BiRRT(Robot* robot, Robot* robot_goal, bool is_map_static_global, Real size_step_cspace_discretization, COST_FUNCTION_TYPE cost_type, Real (*distanceFunction)(const TConfig&, const TConfig&)=NULL); 
		BiRRT(); 
		~BiRRT(); 
};


template<class TConfig> Real RRT<TConfig>::computeMinimalMechanicalworkCost(const TConfig& config0, const TConfig& config1)
{
	Real cost_total=0.0; 
	
	
	cost_total = std::max(config1.q_[2] - config0.q_[2] , (Real)0.0) + 1e-5 * (config1.q_.segment(0,2) - config0.q_.segment(0,2)).norm();  
	
	return cost_total;	
}



template<class TConfig> Real RRT<TConfig>::computeStabilityCostBWTwoConfigs(const TConfig& config0, const TConfig& config1) // config1 is not constant anymore because its configuration is updated since this is the only place where updateFullRobotConfigFromItsHorionzontalConfig() function is called the sake of the entire program's efficiency 
{
	// I assume here that the path segment that connects config0 and config1 has already satisfied the collision checking. 
	
	Real cost_total = 0.0; 
	
	// Just compute the cost corresponding to config1. 	

	cost_total += pow(1 - this->measureTipOverStability(), 0.2); 
		
	return cost_total; 
}


template<class TConfig> Real RRT<TConfig>::computeStabilityCostRobot(const Robot& robot) 
{
	// I assume here that the path segment that connects config0 and config1 has already satisfied the collision checking. 
	
	Real cost_total = 0.0; 
	
	// Just compute the cost corresponding to config1. 	

	cost_total += pow(1 - this->measureTipOverStability(robot), 0.2); 
		
	return cost_total; 
}


template<class TConfig> Real RRT<TConfig>::computeEnergyCostBWTwoConfigs(const TConfig& config0, const TConfig& config1)
{
	// I assume here that the path segment that connects config0 and config1 has already satisfied the collision checking. 
	
	Real cost_total = 0.0; 
	
	cost_total += 0.5 * (ROBOT_BODY_INERTIA_ROLL * pow(config1.q_[3] - config0.q_[3],2) + 
					     ROBOT_BODY_INERTIA_PITCH * pow(config1.q_[4] - config0.q_[4],2)) + 
			      ROBOT_BODY_MASS * ACCEL_GRAVITY * (config1.q_[2] - config0.q_[2]); 			   
	
	return cost_total; 
}

///////////////////////////////////////////////////////////////////////
// RRT
/**
* The constructor for the class RRT with the possibility to initialize the robot and is_map_static_global member variables.
*/
// RRT constructor
template<class TConfig> RRT<TConfig>::RRT(Robot* robot, Robot* robot_goal, bool is_map_static_global, Real size_step_cspace_discretization, COST_FUNCTION_TYPE cost_type, Real (*distanceFunction)(const TConfig&, const TConfig&))
	: Planner<TConfig>(robot, robot_goal, is_map_static_global, size_step_cspace_discretization, distanceFunction) 
{
	cost_type_ = cost_type; // for statistics in the case of RRT and BiRRT
	
	size_step_tree_expansion_ = 20 * this->size_step_cspace_discretization_; 
	
	radius_ball_goal_region_ = 0.4; // 3.0 * size_step_tree_expansion_; 
	
	pub_marker_nodes_startTree_ = n_.advertise<visualization_msgs::Marker>("nodes_start_tree_marker",1);
	pub_marker_edges_startTree_ = n_.advertise<visualization_msgs::Marker>("edges_start_tree_marker",1);
	
	tree_start_.graph_type_ = START_TREE; // meaning that the root node of this tree is the start node. 	
	
	this->id_planner_ = ID_RRT; 
	//std::cout << "planner id: " << this->id_planner_ << std::endl;	
}

template<class TConfig> RRT<TConfig>::RRT()
{
	// this->size_step_tree_expansion_in_cspace_ = 0.05; // epsilon
	
	pub_marker_nodes_startTree_ = n_.advertise<visualization_msgs::Marker>("nodes_start_tree_marker",1);
	pub_marker_edges_startTree_ = n_.advertise<visualization_msgs::Marker>("edges_start_tree_marker",1);

	tree_start_.graph_type_ = START_TREE; // meaning that the root node of this tree is the start node. 	
	
	this->id_planner_ = ID_RRT; 
	//std::cout << "planner id: " << this->id_planner_ << std::endl;
	
	#if ROBOT_CONFIG_TYPE == TRACKED_MOBILEROBOT_WITHARM_CONFIG
	/////////////////////////////////////////////////////////////////////////////////
	// for CMA
	is_minimization_problem_ = false; // in this problem, we try to maximize the robot's overall stability.  
	// is_minimization_problem_ = true; 
	if (is_minimization_problem_) tol_stop_fitness_ = 1e-4; 
	else                          tol_stop_fitness_ = -1e-4; 
	
	num_max_eval_ = 1e3*SIZE_CONFIG_CMA*SIZE_CONFIG_CMA; 
	
	Real mu_tmp = 0.5*(Real)LAMBDA_CMA; 	
	for(int i=0; i<MU_CMA; ++i){
		weights_[i] = log(mu_tmp+0.5) - log((Real)(i+1)); 
	}
	weights_ = weights_/weights_.sum(); 
	mu_eff_ = weights_.sum() * weights_.sum() / (weights_.cwiseProduct(weights_)).sum(); 
	
	c_c_ = (4 + mu_eff_ / SIZE_CONFIG_CMA) / (SIZE_CONFIG_CMA + 4 + 2 * mu_eff_/SIZE_CONFIG_CMA); 
	c_sigma_ = (mu_eff_ + 2) / (SIZE_CONFIG_CMA + mu_eff_ + 5); 
	c_1_ = 2 / (pow(SIZE_CONFIG_CMA+1.3,2) + mu_eff_); 
	c_mu_ = std::min((Real)(1-c_1_), (Real)(2*(mu_eff_-2+1/mu_eff_) / (pow(SIZE_CONFIG_CMA+2,2)+mu_eff_))); 
	damp_sigma_ = 1 + 2*std::max((Real)(0.0),(Real)sqrt((mu_eff_-1)/(SIZE_CONFIG_CMA+1))-1) + c_sigma_; 
	
	sigma_ = 0.3;
	p_sigma_.setZero(); 
	p_c_.setZero();
	B_.setIdentity(); 
	D_.setOnes(); 

	diagonal_D_ = D_.asDiagonal(); 
	C_ = B_ * diagonal_D_.cwiseProduct(diagonal_D_) * B_.transpose(); 

	inv_sqrt_C_ = B_ * diagonal_D_.inverse() * B_.transpose();	
	chi_normal_ = pow((Real)(SIZE_CONFIG_CMA),0.5) * (1.0 - 1.0/(Real)(4.0*SIZE_CONFIG_CMA) + 1.0/(Real)(21.0*SIZE_CONFIG_CMA*SIZE_CONFIG_CMA)); 
	
	//std::cout << "chi_normal_: " << std::endl << chi_normal_ << std::endl << std::endl;
	
	Eigen::Matrix<Real,SIZE_CONFIG_CMA,1> rand_num_arr; 
	for (int j=0; j<SIZE_CONFIG_CMA; ++j){
		rand_num_arr[j] = (Real)(rand())/ (Real)(RAND_MAX); 
		//std::cout << "rand_num_arr[j]: " << std::endl << rand_num_arr[j] << std::endl; 
	}
	
	config_init_cma_ = this->robot_->fwdKinematicsArmCoM(Eigen::MatrixXf::Zero(4,1)); 
	
	std::cout << "cma init config: " << std::endl << config_init_cma_ << std::endl; 

	config_mean_cma_ = config_init_cma_ + rand_num_arr;
	
	fitness_sample_vect_.resize(LAMBDA_CMA); 
	#endif 
}


/**
* The destructor of the class RRT.
*/
template<class TConfig> RRT<TConfig>::~RRT()
{
	tree_start_.clear(); 
}


/**
* generateTrajectory() is a function that generates a (optimal or suboptimal) trajectory.
* @param map_elev_curr A pointer pointing to the current elevation map. 
* @return The error status of the function is returned as of an integer value. 
*/
template<class TConfig> int RRT<TConfig>::generateTrajectory(Terrain* map_elev_curr, const geometry_msgs::PoseStamped& poseStamped_start, const geometry_msgs::PoseStamped& poseStamped_goal, const SimulationParamInput& param_input_simulation)
// template<class TConfig> int RRT<TConfig>::generateTrajectory(Terrain* map_elev_curr)
{
	this->map_elev_curr_ = map_elev_curr; 
	
	this->robot_->updateElevMap(this->map_elev_curr_); 	
	this->robot_goal_->updateElevMap(this->map_elev_curr_); // for the robot_goal_
	
	this->robot_->setLCPSamplingTime(param_input_simulation.time_sampling_lcp); 
	this->robot_goal_->setLCPSamplingTime(param_input_simulation.time_sampling_lcp);
	
	std::cout << "start robot's lcp sampling time: " <<  this->robot_->getLCPSamplingTime() << std::endl; 
	std::cout << "goal robot's lcp sampling time: " <<  this->robot_goal_->getLCPSamplingTime() << std::endl; 
		
	defineElevMapBounds(this->map_elev_curr_); 
	
	this->reserveMemoryForVectorsInvolvedInCollisionChecking(this->map_elev_curr_->obstacle_); // reserve the memory for the vectors involved in collision checking ahead in order to reduce the execution time (based on the results obtained from various profilers).
	this->ind_region_startConfig_belongsTo_ = this->identifyRegionContainingStartConfig(this->map_elev_curr_->obstacle_, this->config_start_); 
	
	std::cout << "index of the region to which the start config belongs: " << this->ind_region_startConfig_belongsTo_ << std::endl; 
	
	this->setRobotStartPose(poseStamped_start); 
	this->setRobotGoalPose(poseStamped_goal); 
	
	this->displayObstacleContours(); 
		
	this->updateFullRobotConfigFromItsHorionzontalConfig(this->config_start_, this->robot_); 
	
	this->robot_->publishJointStateAndBroadcastTF();
		
	ResultsToBeStored  results_to_be_stored; 		
	int result = buildTree(this->config_start_, this->config_goal_, param_input_simulation, results_to_be_stored);
		
	if (param_input_simulation.do_store_results){
		// Store the statistics
		std::ostringstream id_trial_str, num_total_potential_contact_pts_str, time_sampling_lcp_str, trate_trrt_str; 
		id_trial_str << param_input_simulation.id_trial; 
		num_total_potential_contact_pts_str << 2*NUM_CONTACT_TRACK; 		
		time_sampling_lcp_str << (int)(param_input_simulation.time_sampling_lcp*1000); 		
		trate_trrt_str << param_input_simulation.trate_trrt;
		
		std::string name_path, name_file; 
		if (param_input_simulation.type_study == "algorithm_type_analysis"){
			name_path = "./results/algorithm_type_analysis/"; 
			name_path += "contact" + num_total_potential_contact_pts_str.str() + "sampling" + time_sampling_lcp_str.str() + "/" + param_input_simulation.type_cost + "/"
					  + param_input_simulation.type_terrain + "/" + param_input_simulation.algorithm_planning + "/"; 			
			// std::string file_name = "./results/algorithm_type_analysis/" + param_input_simulation.type_terrain + "/" + param_input_simulation.algorithm_planning + "/" + "result" + id_trial_str.str() + ".dat"; 
		}else if (param_input_simulation.type_study == "cost_type_analysis"){
			name_path = "./results/cost_type_analysis/"; 
			name_path += "contact" + num_total_potential_contact_pts_str.str() + "sampling" + time_sampling_lcp_str.str() + "/" + param_input_simulation.type_cost + "/"
				      + param_input_simulation.type_terrain + "/" + param_input_simulation.algorithm_planning + "/"; 
		}else if (param_input_simulation.type_study == "pose_estimation_analysis"){
			name_path = "./results/pose_estimation_analysis/";
			name_path += "contact" + num_total_potential_contact_pts_str.str() + "sampling" + time_sampling_lcp_str.str() + "/" + param_input_simulation.type_cost + "/"
					  + param_input_simulation.type_terrain + "/" + param_input_simulation.algorithm_planning + "/"; 

		}else if (param_input_simulation.type_study == "trrt_parameter_analysis"){
			name_path = "./results/trrt_parameter_analysis/"; 
			name_path += "contact" + num_total_potential_contact_pts_str.str() + "sampling" + time_sampling_lcp_str.str() + "/" + param_input_simulation.type_cost + "/"
				  + param_input_simulation.type_terrain + "/" + param_input_simulation.algorithm_planning + "/" + "trate_trrt" + trate_trrt_str.str() + "/"; 				  
		}else{
			ROS_ERROR("Wrong study type has been chosen"); 
			exit(-1); 
		}		
		name_file = name_path + "statistics" + id_trial_str.str() + ".dat"; 
				  
		std::cout << name_file << std::endl;
		
		/////////////////////// Save the statistics ////////////////////////
		std::ofstream of1(name_file.c_str());
		of1 << num_total_potential_contact_pts_str.str() << " " // 0
			<< time_sampling_lcp_str.str() << " "               // 1
			<< param_input_simulation.type_cost << " "			// 2
			<< param_input_simulation.type_terrain << " "		// 3
			<< param_input_simulation.algorithm_planning << " "	// 4
			<< param_input_simulation.id_trial << " "			// 5
			<< param_input_simulation.seed << " "				// 6
			<< results_to_be_stored.success_planning << " "		// 7
			<< results_to_be_stored.num_iterations << " "		// 8
			<< results_to_be_stored.num_nodes << " "			// 9
			<< results_to_be_stored.time_elapsed << " "			// 10
			<< results_to_be_stored.length_path << " "			// 11
			<< results_to_be_stored.cost_mechanical_work << " " // 12
			<< results_to_be_stored.cost_stability << " "       // 13
			<< results_to_be_stored.cost_energy << " "          // 14
			<< results_to_be_stored.cost_stability_g1 << " "    // 15
			<< results_to_be_stored.cost_stability_g2 << " "    // 16
			<< results_to_be_stored.cost_stability_g3 << "\n";  // 17			
		of1.close(); 
		
		////////// Save the path configurations into a file ////////////////
		name_file = name_path + "config_solution" + id_trial_str.str() + ".dat"; 
		std::cout << "path configuration file name: " << name_file << std::endl; 
		std::ofstream of2(name_file.c_str()); 
		for(int i=0; i<(int)this->config_solution_.size(); ++i){
			of2 << this->config_solution_[i].q_[0] << " "
				<< this->config_solution_[i].q_[1] << " "
				<< this->config_solution_[i].q_[2] << " "
				<< this->config_solution_[i].q_[3] << " "
				<< this->config_solution_[i].q_[4] << " "
				<< this->config_solution_[i].q_[5] << " "
				<< this->config_solution_[i].q_[6] << "\n";
		}
		of2.close(); 		
	}
		
	return result; 
}



/**
* buildTree() is a function that builds a tree using the RRT algorithm.
* @param 
* @return The error status of the function is returned as of an integer value. 
*/
template<class TConfig> int RRT<TConfig>::buildTree(TConfig& config_start, TConfig& config_goal, const SimulationParamInput& param_input_simulation, ResultsToBeStored& results_to_be_stored)
{
	ROS_INFO_STREAM("RRT");
	
	timespec time_start_process, time_end_process;//, time_start_real, time_end_real, time_start_thread, time_end_thread; 

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time_start_process); // high-resolution timer provided by the CPU for each process.
	
	int result_solution_found = ERROR; 
		
	tree_start_.init(); 
	
	if(this->updateFullRobotConfigFromItsHorionzontalConfig(config_start, this->robot_) == ERROR){	
		ROS_ERROR("The start configuration is unattainable!"); 
		exit(-1); 
	}
	config_start.q_ = this->robot_->getPose(); 
	
	if(this->updateFullRobotConfigFromItsHorionzontalConfig(config_goal, this->robot_) == ERROR){
		ROS_ERROR("The goal configuration is unattainable!"); 
		exit(-1); 
	}
	config_goal.q_ = this->robot_->getPose(); 
			
	node_start_ = tree_start_.addNode(config_start); 
	
	int itera; 
	for(itera=0; itera<NUM_MAX_ITERATIONS; ++itera){
		
		
		TConfig config_rand;
		GraphNode<TConfig> *node_new; 
		
		config_rand = sampleRandConfig(); 
		// associate an approximate height to config_rand based on the terrain height
		int ind_x = floor(((config_rand.q_[0]-this->map_elev_curr_->xmin_)/this->map_elev_curr_->res_cell_x_) + TOL_NUM_ERR_FLOOR); 
		int ind_y = floor(((config_rand.q_[1]-this->map_elev_curr_->ymin_)/this->map_elev_curr_->res_cell_y_) + TOL_NUM_ERR_FLOOR); 
		if (ind_x >= this->map_elev_curr_->num_samples_x_) ind_x = this->map_elev_curr_->num_samples_x_-1; 
		if (ind_y >= this->map_elev_curr_->num_samples_y_) ind_y = this->map_elev_curr_->num_samples_y_-1; 

		config_rand.q_[2] = this->map_elev_curr_->Z_(ind_x,ind_y) + ROBOT_TRACK_HEIGHT/2.0; 
						
		if (extend(tree_start_, config_rand, node_new) != TRAPPED){
				
			if((node_new->config_.q_.segment(0,2) - config_goal.q_.segment(0,2)).norm() < radius_ball_goal_region_){ // distance in x and y only 
								
				// TWO_TREES_CONNECTION is used b/c I'd like to keep the yaw angles of both the node_new->config_ and config_goal configurations.
				if(this->isTraversableBWTwoConfigs(node_new->config_, config_goal, TWO_TREES_CONNECTION, START_TREE) == ERROR){
					continue; // abandon this iteration since there can not be a connection b/w the new node and the goal node. 
				}
								
				node_goal_ = tree_start_.addNode(config_goal); 
				GraphEdge<TConfig>* edge_to_goal = tree_start_.addEdge(node_new, node_goal_); 
				node_goal_->setNodeParent(node_new); 
				node_goal_->setEdgeToParent(edge_to_goal); 
				result_solution_found = returnPath(this->config_solution_);
				break; // a path has been found, hence the path search is quitted. 
			}
		}
	}	
	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time_end_process); // high-resolution timer provided by the CPU for each process.

	Real cost_mechanical_work = 0.0; 
	Real cost_stability = 0.0; 
	Real cost_energy = 0.0; 
	Real length_path=0.0; 
	Real cost_stability_g1 = 0.0; 
	Real cost_stability_g2 = 0.0; 
	Real cost_stability_g3 = 0.0; 

	displayNodesStartTree(); 	
	displayEdgesStartTree(); 
	
	if (result_solution_found == OK){
		ROS_INFO_STREAM("A solution has been found!"); 
		
		this->displayConfigSolution();
		
		for(int i=1; i<(int)this->config_solution_.size(); ++i){
			
			// It is necessary to update the robot pose with the already-found configuration for measuring the stability.
			this->updateFullRobotConfig(this->config_solution_[i], this->robot_); 
			
			// For statistics
			// Note: regardless of the type of cost function employed for searching the optimal path, here I compute the cost associated with the found path in all three aspects with the purpose to compare the results later. 
			cost_mechanical_work += computeMinimalMechanicalworkCost(this->config_solution_[i-1], this->config_solution_[i]);
			cost_stability += computeStabilityCostBWTwoConfigs(this->config_solution_[i-1], this->config_solution_[i]);
			cost_energy += computeEnergyCostBWTwoConfigs(this->config_solution_[i-1], this->config_solution_[i]);
			
			cost_stability_g1 += (1 - this->measureTipOverStability());
			cost_stability_g2 += (1 - pow(this->measureTipOverStability(), 5.0));
			cost_stability_g3 += pow(1 - this->measureTipOverStability(), 0.2); 
			
			
			length_path += sqrt(pow(this->config_solution_[i-1].q_[0]-this->config_solution_[i].q_[0], 2.0) + pow(this->config_solution_[i-1].q_[1]-this->config_solution_[i].q_[1], 2.0) + pow(this->config_solution_[i-1].q_[2]-this->config_solution_[i].q_[2], 2.0));

			this->robot_->publishJointStateAndBroadcastTF();
		}

	}else{
		ROS_INFO_STREAM("No solution has been found!"); 
	}
	
	results_to_be_stored.success_planning = result_solution_found; 
	results_to_be_stored.num_iterations = itera; 
	results_to_be_stored.num_nodes = this->tree_start_.nodes_.size(); 
	results_to_be_stored.time_elapsed = (Real) (diff(time_start_process,time_end_process).tv_sec) + (Real)(diff(time_start_process,time_end_process).tv_nsec) / 1e9; 	
	results_to_be_stored.length_path = length_path; 
	results_to_be_stored.cost_mechanical_work = cost_mechanical_work; 
	results_to_be_stored.cost_stability = cost_stability; 
	results_to_be_stored.cost_energy = cost_energy; 
	results_to_be_stored.cost_stability_g1 = cost_stability_g1; 
	results_to_be_stored.cost_stability_g2 = cost_stability_g2; 
	results_to_be_stored.cost_stability_g3 = cost_stability_g3; 	
		
	std::ostringstream ss_time_diff_process;//, ss_time_diff_real, ss_time_diff_thread; 
	ss_time_diff_process << std::setw(9) << std::setfill('0') << diff(time_start_process,time_end_process).tv_nsec; 
		
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "RESULTS: " << std::endl; 	
	std::cout << "Elapsed time (process): " << diff(time_start_process,time_end_process).tv_sec << "." << ss_time_diff_process.str() << std::endl; 
	std::cout << "# iterations: " << itera << std::endl; 
	std::cout << "# nodes: " << this->tree_start_.nodes_.size() << std::endl; 
	std::cout << "Success: " << result_solution_found << std::endl; 	
	std::cout << "Path length: " << length_path << std::endl;	
	std::cout << "Path mechanical work: " << cost_mechanical_work << std::endl;
	std::cout << "Path stability cost: " << cost_stability << std::endl;
	std::cout << "Path energy consumption: " << cost_energy << std::endl;
	std::cout << "Path stability cost g1: " << cost_stability_g1 << std::endl;
	std::cout << "Path stability cost g2: " << cost_stability_g2 << std::endl;
	std::cout << "Path stability cost g3: " << cost_stability_g3 << std::endl;	
	std::cout << "-------------------------------------------------------" << std::endl;
				
	return result_solution_found;	
}


/**
* sampleRandConfig() is a function that randomly samples a configuration in a given configuration space. 
* @param 
* @return The error status of the function is returned as of an integer value. 
*/
template<class TConfig> TConfig RRT<TConfig>::sampleRandConfig()
{	
	TConfig config; 
	for(int i=0; i<(int) this->ind_sample_dims_in_config_dims_.size(); ++i){			
		config.q_[this->ind_sample_dims_in_config_dims_[i]] = generateRandNumUniform(this->bounds_sample_dims_(i,0), this->bounds_sample_dims_(i,1)); // bounds_sample_dims_: first column==lower bound, second column==upper bound.				
		
	}

	return config; 
}

/**
* extend() is a function that extends the RRT using the standard RRT algorithm.
* @param 
* @return The error status of the function is returned as of an integer value. 
*/
template<class TConfig> RRT_RELATED_TREE_EXTENSION_RESULT RRT<TConfig>::extend(Graph<TConfig>& tree, const TConfig& config_rand, GraphNode<TConfig>*& node_new) // even though I could use the member variable of the class Planner and therefore there would not be need for passing the tree as argument, but for bidirectional graph case, I need to specify the right tree, and therefore, I decided to pass the tree info as an argument of the function. 
{		
	GraphNode<TConfig> *node_nearest = NULL;
	GraphEdge<TConfig>* edge_new = NULL; 
	TConfig config_new; 
	
	node_nearest = tree.getNearestNode(this->distanceFunction_, config_rand); // the tree_ is a member variable of the class Planner, which is the base class to Rthe class RRT (a derived class). 
	if(node_nearest != NULL){
		if ( obtainNewConfig(node_nearest->config_, config_rand, config_new) == OK ){
			
			////////////////////////////////////////////////////////////////////////////////////////////
			// ESTIMATE THE ROBOT POSE TO KNOW WHETHER THE ROBOT FACES BACKWARD TO THE STAIR; IF SO SUCH PATH IS NOT FEASIBLE!
			// BUT, THIS CAN BE VERY TIME CONSUMING
			if (this->updateFullRobotConfigFromItsHorionzontalConfig(config_new, this->robot_) == ERROR){ // update the robot configuration						
				return TRAPPED; 		
			}
			config_new.q_ = this->robot_->getPose();
				
			// if the motion from the nearest node to the new node is backward
			if (this->is_robot_motion_forward_ == false){	// is_robot_motion_forward_ is updated in the isTraversableBWTwoConfigs() function located in planner.hpp, and this function has been called in the obtainNewConfig() function.
				// if the backward motion has to be carried out, then such a motion is not allowed if the absolute value of the associated pitch angle is larger than a threshold. 
				if(fabs(config_new.q_[4] - node_nearest->config_.q_[4]) > ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS){ // When the nose is facing down the corresponding pitch angle is positive. The value for ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS is found empirically. 
				
					// for analyzing the influence of ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS to the planning efficiency.
					tree.num_samples_rejected_by_high_pitch_++;
					
					return TRAPPED; 
				}
			}
			
			node_new = tree.addNode(config_new);
			edge_new = tree.addEdge(node_nearest, node_new);
			
			node_new->setNodeParent(node_nearest); 
			node_new->setEdgeToParent(edge_new); 
			
			if (config_new == config_rand){
				return REACHED; // enum or constant integer
			}else{
				return ADVANCED; // enum or constant integer
			}
		}
	}
	return TRAPPED; // enum or constant integer	
}


/**
* obtainNewConfig() is a function that returns a new configuration if such configuration has already satisfied the collision checking procedure. 
* @param 
* @return The error status of the function is returned as of an integer value. 
*/
template<class TConfig> int RRT<TConfig>::obtainNewConfig(TConfig& config_nearest, const TConfig& config_rand, TConfig& config_new)
{
	Real distance; 
	
	distance = distanceFunction_(config_nearest, config_rand); 
	if (distance > size_step_tree_expansion_){
						
		if(interpolateSampleLinearly(config_nearest, config_rand, (size_step_tree_expansion_/distance), config_new) == ERROR){ // if OK, config will have some meaningful coordinates; but, if ERROR, then config is NULL.
			return ERROR; 
		}	
	}else{
		config_new = config_rand; 
	}
		
	config_new.q_[5] = modNPiPi(atan2f(config_new.q_[1] - config_nearest.q_[1], config_new.q_[0] - config_nearest.q_[0])); // yaw angle. If the robot_model_ is POINT_MASS, then this will be useless, since I don't look at its orientation. But, if the robot_model_ is RIGID_BODY, then this is a must step. 
	
	if(this->isTraversableBWTwoConfigs(config_nearest, config_new, TREE_EXTENSION, START_TREE) == ERROR){
		return ERROR; // abandon this iteration since there can not be a connection b/w the new node and the goal node. 
	}

	return OK; 	
}


template<class TConfig> int RRT<TConfig>::returnPath(std::vector<TConfig>& config_path)
{
	std::list<TConfig> config_path_list; 
	
	if (this->node_start_ == NULL){
		return ERROR; 
	}
	
	GraphNode<TConfig>* node = NULL; 		
			
	// now continue with start-node tree.	
	config_path_list.push_front(node_goal_->config_); 
	node = node_goal_->getParentNode(); 	
	if( node == NULL ){
		return ERROR;
	}
	
	while( node != this->node_start_ ){
		if (node->getEdgeToParentNode() != NULL){		
			config_path_list.push_front(node->config_); 
		}else{
			return ERROR; 
		}
		
		node = node->getParentNode(); 		
		if (node == NULL){
			return ERROR; 
		}
	}
	
	if (node == this->node_start_){
		config_path_list.push_front(node->config_); 
	}
	
	
	config_path.clear(); 
	config_path.resize(config_path_list.size()); 
	std::copy(config_path_list.begin(), config_path_list.end(), config_path.begin()); 
	
	return OK; 
}

	

/**
* This displayGraphNodes() function displays the RRT* tree nodes as points in RVIZ. 
*/
template<class TConfig> void RRT<TConfig>::displayNodesStartTree()
{
	visualization_msgs::Marker marker;
	// set the frame ID and timestamp
	if(this->map_elev_curr_->getIsMapStaticGlobal()){
		marker.header.frame_id = "terrain_elev_map";
	}else{
		marker.header.frame_id = "d6dvo_map";
	}
	marker.header.stamp = ros::Time();

	// set the namespace, type, action and id for this marker. This serves to create a unique ID.
	marker.ns = "graph_nodes";
	marker.type = visualization_msgs::Marker::SPHERE_LIST;
	marker.action = visualization_msgs::Marker::ADD;
	marker.id = 1;

	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;

	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;

	marker.scale.x = 0.5*g_size_node_display;//5*step_c_space_; // 0.05;
	marker.scale.y = 0.5*g_size_node_display;//5*step_c_space_; // 0.05;
	marker.scale.z = 0.5*g_size_node_display;//5*step_c_space_; // 0.05;

	marker.color.r = 1.0;
	marker.color.g = 1.0;
	marker.color.b = 1.0;
	marker.color.a = 1.0;

	// specify the color of the marker
	std_msgs::ColorRGBA color;
	color.r = 1.0;
	color.g = 0.0;
	color.b = 0.0;
	color.a = 1.0;
	
	marker.color = color;

	// Point
	geometry_msgs::Point point_a;

	marker.points.reserve(tree_start_.nodes_.size()); // this should make the execution more efficient, since I reserve the memory space ahead rather than reallocating the memory size posteriorly. 
	
	// loop through all vertices
	typename std::list<GraphNode<TConfig> * >::const_iterator inode;
	for(inode=tree_start_.nodes_.begin() ; inode!=tree_start_.nodes_.end() ; inode++){
		point_a.x = (*inode)->config_.q_[0];
		point_a.y = (*inode)->config_.q_[1];
		point_a.z = this->map_elev_curr_->estimateTerrainHeight((*inode)->config_.q_[0], (*inode)->config_.q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;

		// add the point pair to the message
		marker.points.push_back(point_a);
	}

	// publish the marker
	pub_marker_nodes_startTree_.publish(marker);
	ros::Duration(0.2).sleep();	
}

/**
* This displayGraphEdges() function displays the RRT* tree edges as line segments in RVIZ. 
*/
template<class TConfig> void RRT<TConfig>::displayEdgesStartTree()
{
	visualization_msgs::Marker marker;
	// set the frame ID and timestamp
	if(this->map_elev_curr_->getIsMapStaticGlobal()){
		marker.header.frame_id = "/terrain_elev_map";
	}else{
		marker.header.frame_id = "/d6dvo_map";
	}
	marker.header.stamp = ros::Time();

	// set the namespace, type, action and id for this marker. This serves to create a unique ID.
	marker.ns = "graph_edges";
	marker.type = visualization_msgs::Marker::LINE_LIST;
	marker.action = visualization_msgs::Marker::ADD;
	marker.id = 2;

	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;

	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;

	marker.scale.x = 0.02; // g_size_node_display;//3*step_c_space_; // 0.02;
	marker.scale.y = 0.02; // g_size_node_display;//3*step_c_space_; // 0.02;
	marker.scale.z = 0.02; // g_size_node_display;//3*step_c_space_; // 0.02;

	marker.color.r = 1.0;
	marker.color.g = 0.0;
	marker.color.b = 1.0;
	marker.color.a = 1.0;

	// specify the color of the marker
	std_msgs::ColorRGBA color;
	color.r = 1.0;
	color.g = 0.0;
	color.b = 0.0;
	color.a = 1.0;

	marker.color = color; 
	
	// Point
	geometry_msgs::Point point_a, point_b;

	marker.points.reserve(2*tree_start_.edges_.size()); // this should make the execution more efficient, since I reserve the memory space ahead rather than reallocating the memory size posteriorly. 
	
	// loop through all vertices
	typename std::list<GraphEdge<TConfig> * >::const_iterator iedge;
	for(iedge=tree_start_.edges_.begin() ; iedge!=tree_start_.edges_.end() ; ++iedge){
		point_a.x = (*iedge)->getStartNode()->config_.q_[0];
		point_a.y = (*iedge)->getStartNode()->config_.q_[1];
		point_a.z = this->map_elev_curr_->estimateTerrainHeight((*iedge)->getStartNode()->config_.q_[0], (*iedge)->getStartNode()->config_.q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;

		point_b.x = (*iedge)->getEndNode()->config_.q_[0];
		point_b.y = (*iedge)->getEndNode()->config_.q_[1];
		point_b.z = this->map_elev_curr_->estimateTerrainHeight((*iedge)->getEndNode()->config_.q_[0], (*iedge)->getEndNode()->config_.q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;

		// add the point pair to the message
		marker.points.push_back(point_a);
		marker.points.push_back(point_b);
	}

	// publish the marker
	pub_marker_edges_startTree_.publish(marker);
	ros::Duration(0.2).sleep();	
}

/**
* This displayGraphEdges() function displays the RRT* tree edges as line segments in RVIZ. 
*/
template<class TConfig> int RRT<TConfig>::studyInfluencePtsContactTimeSampling(Terrain* map_elev_curr, const geometry_msgs::PoseStamped& poseStamped_start, const geometry_msgs::PoseStamped& poseStamped_goal, const SimulationParamInput param_input_simulation)
{
	this->map_elev_curr_ = map_elev_curr; 
	this->robot_->updateElevMap(this->map_elev_curr_); 
	defineElevMapBounds(this->map_elev_curr_); 
	
	this->reserveMemoryForVectorsInvolvedInCollisionChecking(this->map_elev_curr_->obstacle_); // reserve the memory for the vectors involved in collision checking ahead in order to reduce the execution time (based on the results obtained from various profilers).
	
	this->setRobotStartPose(poseStamped_start); 
	this->setRobotGoalPose(poseStamped_goal); 
	
	this->displayObstacleContours(); 

	
	////////////////////////////////////////////////////////////////////
	// 1. Read the x, y, yaw of the path configurations that have been found previously and saved into a file. 
	std::string name_path = "./results/pose_estimation_analysis/";
	std::string name_file_in = name_path + "path_configuration.dat"; 
	std::ifstream file_data_in(name_file_in.c_str());
	if(!file_data_in.is_open()){
		ROS_ERROR("Could not open %s", name_file_in.c_str()); 
		return ERROR; 
	}
	
	std::string line; 
	std::vector<TConfig> config_solution; 
	
	std::getline(file_data_in, line); 
	while(!file_data_in.eof()){	
		TConfig config; 
		int status = sscanf(line.c_str(), "%f %f %f %f %f %f %f", &(config.q_[0]), &(config.q_[1]), &(config.q_[2]), &(config.q_[3]), &(config.q_[4]), &(config.q_[5]), &(config.q_[6])); 
		if (status != 7){
			ROS_ERROR("could not properly parse %s", name_file_in.c_str());
			return ERROR; 
		}
		
		config_solution.push_back(config);
		
		std::getline(file_data_in, line); 
	}
		
	file_data_in.close(); 
	
	////////////////////////////////////////////////////////////////////
	// 2 and 3: Estimate the robot pose and save the results into files.
	std::ostringstream num_total_potential_contact_pts_str, time_sampling_lcp_str; 	
	num_total_potential_contact_pts_str << NUM_TOTAL_CONTACT; 
	time_sampling_lcp_str << (int)(param_input_simulation.time_sampling_lcp*1000); 	
	std::string name_file_out; 
	name_file_out = name_path + "contact" + num_total_potential_contact_pts_str.str() + "sampling" + time_sampling_lcp_str.str() + "/" + 
					param_input_simulation.type_cost + "/" + param_input_simulation.type_terrain + "/" + param_input_simulation.algorithm_planning + "/poseEstimated.dat"; 
			  			       
	std::cout << name_file_out << std::endl;		
	std::ofstream file_data_out(name_file_out.c_str());		
	if(!file_data_out.is_open()){
		ROS_ERROR("Could not open %s", name_file_out.c_str()); 
		return ERROR; 
	}	
	// 2. Estimate the robot pose for each triplet (x,y,yaw) with the chosen pair (number of potential contact points, LCP sampling time). 
	for(int i=0; i<(int) config_solution.size(); ++i){
		
		this->updateFullRobotConfigFromItsHorionzontalConfig(config_solution[i], this->robot_); 
		config_solution[i].q_ = this->robot_->getPose(); 
		
		
		//3. Save the estimated pose into a file 	
		file_data_out << this->robot_->getPose(0) << " "
					  << this->robot_->getPose(1) << " "
					  << this->robot_->getPose(2) << " "
					  << this->robot_->getPose(3) << " "
					  << this->robot_->getPose(4) << " "
					  << this->robot_->getPose(5) << " "
					  << this->robot_->getPose(6) << "\n";
	}
	file_data_out.close();	
	
	return OK; 
}

///////////////////////////////////////////////////////////////////////
// BiRRT
/**
* The constructor for the class RRT with the possibility to initialize the robot and is_map_static_global member variables.
*/
template<class TConfig> BiRRT<TConfig>::BiRRT(Robot* robot, Robot* robot_goal, bool is_map_static_global, Real size_step_cspace_discretization, COST_FUNCTION_TYPE cost_type, Real (*distanceFunction)(const TConfig&, const TConfig&))
	: RRT<TConfig>(robot, robot_goal, is_map_static_global, size_step_cspace_discretization, cost_type, distanceFunction) 
{	
	tree_goal_.graph_type_ = GOAL_TREE; // meaning that the root node of the secondary tree is the goal node.
	
	pub_marker_nodes_goalTree_ = n_.advertise<visualization_msgs::Marker>("nodes_goal_tree_marker",1);
	pub_marker_edges_goalTree_ = n_.advertise<visualization_msgs::Marker>("edges_goal_tree_marker",1); 

	this->id_planner_ = ID_BIRRT; 
	//std::cout << "planner id: " << this->id_planner_ << std::endl;
}

template<class TConfig> BiRRT<TConfig>::BiRRT()
{	
	tree_goal_.graph_type_ = GOAL_TREE; // meaning that the root node of the secondary tree is the goal node.	
	
	pub_marker_nodes_goalTree_ = n_.advertise<visualization_msgs::Marker>("nodes_goal_tree_marker",1);
	pub_marker_edges_goalTree_ = n_.advertise<visualization_msgs::Marker>("edges_goal_tree_marker",1); 	
	
	this->id_planner_ = ID_BIRRT; 
	//std::cout << "planner id: " << this->id_planner_ << std::endl;
}

/**
* The destructor of the class BiRRT
*/
template<class TConfig> BiRRT<TConfig>::~BiRRT()
{	
	tree_goal_.clear(); 	
}




template<class TConfig> int BiRRT<TConfig>::buildTree(TConfig& config_start, TConfig& config_goal, const SimulationParamInput& param_input_simulation, ResultsToBeStored& results_to_be_stored)
{	
	ROS_INFO_STREAM("BiRRT"); 
	
	timespec time_start_process, time_end_process;//, time_start_real, time_end_real, time_start_thread, time_end_thread; 

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time_start_process); // high-resolution timer provided by the CPU for each process.
	
	int result_solution_found = ERROR; 
			
	this->tree_start_.init(); // "this" b/c tree_ is from the class Planner
	tree_goal_.init(); // no need for "this" b/c tree_second_ is a member variable of the class BiRRT. 	
	
	if(this->updateFullRobotConfigFromItsHorionzontalConfig(config_start, this->robot_) == ERROR){	
		ROS_ERROR("The start configuration is unattainable!"); 
		exit(-1); 
	}
	this->robot_->publishJointStateAndBroadcastTF();
	config_start.q_ = this->robot_->getPose(); 
	
	if(this->updateFullRobotConfigFromItsHorionzontalConfig(config_goal, this->robot_goal_) == ERROR){
		ROS_ERROR("The goal configuration is unattainable!"); 
		exit(-1); 
	}
	this->robot_goal_->publishJointStateAndBroadcastTF();
	config_goal.q_ = this->robot_goal_->getPose(); 	
	this->node_start_ = this->tree_start_.addNode(config_start);
	this->node_goal_ = tree_goal_.addNode(config_goal); 
		
	int itera; // in order to display the number of iterations. 
	for(itera=0; itera<NUM_MAX_ITERATIONS; ++itera){
		Graph<TConfig> *tree0=NULL, *tree1=NULL; 
		
		// Symmetric growth of the two trees. 		
		if (this->tree_start_.nodes_.size() <= tree_goal_.nodes_.size()){
			tree0 = &(this->tree_start_); 
			tree1 = &tree_goal_; 
		}else{
			tree0 = &tree_goal_; 
			tree1 = &(this->tree_start_); 
		}
		
		GraphNode<TConfig> *node_new_tree0, *node_nearest_tree1; 
		
		TConfig config_rand;
		config_rand = this->sampleRandConfig(); 
		// associate an approximate height to config_rand based on the terrain height
		int ind_x = floor(((config_rand.q_[0]-this->map_elev_curr_->xmin_)/this->map_elev_curr_->res_cell_x_) + TOL_NUM_ERR_FLOOR); 
		int ind_y = floor(((config_rand.q_[1]-this->map_elev_curr_->ymin_)/this->map_elev_curr_->res_cell_y_) + TOL_NUM_ERR_FLOOR); 
		if (ind_x >= this->map_elev_curr_->num_samples_x_) ind_x = this->map_elev_curr_->num_samples_x_-1; 
		if (ind_y >= this->map_elev_curr_->num_samples_y_) ind_y = this->map_elev_curr_->num_samples_y_-1; 

		config_rand.q_[2] = this->map_elev_curr_->Z_(ind_x,ind_y) + ROBOT_TRACK_HEIGHT/2.0; 
						
		if (extend(*tree0, config_rand, node_new_tree0) != TRAPPED){
			if (connectTrees(*tree1, *node_new_tree0, node_nearest_tree1) == REACHED){
				result_solution_found = returnPath(*tree0, *tree1, *node_new_tree0, *node_nearest_tree1, this->config_solution_); 
				break;		
			}
		}
	}

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time_end_process); // high-resolution timer provided by the CPU for each process.
	
	Real cost_mechanical_work = 0.0; 
	Real cost_stability = 0.0; 
	Real cost_energy = 0.0; 
	Real length_path=0.0; 
	Real cost_stability_g1 = 0.0; 
	Real cost_stability_g2 = 0.0; 
	Real cost_stability_g3 = 0.0; 
	
	if (result_solution_found == OK){
		
		ROS_INFO_STREAM("A solution has been found!"); 
		
		for(int i=1; i<(int)this->config_solution_.size(); ++i){
		
			// It is necessary to update the robot pose with the already-found configuration for measuring the stability.
			this->updateFullRobotConfig(this->config_solution_[i], this->robot_); 
		
			// for statistics
			// Note: regardless of the type of cost function employed for searching the optimal path, here I compute the cost associated with the found path in all three aspects with the purpose to compare the results later. 
			cost_mechanical_work += computeMinimalMechanicalworkCost(this->config_solution_[i-1], this->config_solution_[i]);
			cost_stability += computeStabilityCostBWTwoConfigs(this->config_solution_[i-1], this->config_solution_[i]);
			cost_energy += computeEnergyCostBWTwoConfigs(this->config_solution_[i-1], this->config_solution_[i]);
			
			cost_stability_g1 += (1 - this->measureTipOverStability());
			cost_stability_g2 += (1 - pow(this->measureTipOverStability(), 5.0));
			cost_stability_g3 += pow(1 - this->measureTipOverStability(), 0.2); 
			
			length_path += sqrt(pow(this->config_solution_[i-1].q_[0]-this->config_solution_[i].q_[0], 2.0) + pow(this->config_solution_[i-1].q_[1]-this->config_solution_[i].q_[1], 2.0) + pow(this->config_solution_[i-1].q_[2]-this->config_solution_[i].q_[2], 2.0));
		}
	}else{
		ROS_INFO_STREAM("No solution has been found!"); 
	}
	
	results_to_be_stored.success_planning = result_solution_found; 
	results_to_be_stored.num_iterations = itera; 
	results_to_be_stored.num_nodes = this->tree_start_.nodes_.size() + this->tree_goal_.nodes_.size(); 
	results_to_be_stored.time_elapsed = (Real) (diff(time_start_process,time_end_process).tv_sec) + (Real)(diff(time_start_process,time_end_process).tv_nsec) / 1e9; 	
	results_to_be_stored.length_path = length_path; 
	results_to_be_stored.cost_mechanical_work = cost_mechanical_work; 
	results_to_be_stored.cost_stability = cost_stability; 
	results_to_be_stored.cost_energy = cost_energy;
	results_to_be_stored.cost_stability_g1 = cost_stability_g1;  
	results_to_be_stored.cost_stability_g2 = cost_stability_g2;  
	results_to_be_stored.cost_stability_g3 = cost_stability_g3;  
	
	std::ostringstream ss_time_diff_process;//, ss_time_diff_real, ss_time_diff_thread; 
	ss_time_diff_process << std::setw(9) << std::setfill('0') << diff(time_start_process,time_end_process).tv_nsec; 
		
	std::cout << "-------------------------------------------------------" << std::endl;
	std::cout << "RESULTS: " << std::endl; 	
	std::cout << "Elapsed time (process): " << diff(time_start_process,time_end_process).tv_sec << "." << ss_time_diff_process.str() << std::endl; 
	std::cout << "# iterations: " << itera << std::endl; 
	std::cout << "# nodes: " << this->tree_start_.nodes_.size() + tree_goal_.nodes_.size() << std::endl; 
	std::cout << "# nodes (start tree): " << this->tree_start_.nodes_.size() << std::endl;
	std::cout << "# nodes (goal tree): " << tree_goal_.nodes_.size() << std::endl;
	std::cout << "Success: " << result_solution_found << std::endl; 	
	std::cout << "Path length: " << length_path << std::endl;	
	std::cout << "Path mechanical work: " << cost_mechanical_work << std::endl;
	std::cout << "Path stability cost: " << cost_stability << std::endl;
	std::cout << "Path energy consumption: " << cost_energy << std::endl;
	std::cout << "Path stability cost g1: " << cost_stability_g1 << std::endl;
	std::cout << "Path stability cost g2: " << cost_stability_g2 << std::endl;
	std::cout << "Path stability cost g3: " << cost_stability_g3 << std::endl;
	std::cout << "-------------------------------------------------------" << std::endl;
			
	return result_solution_found;
}

/**
* extend() is a function that extends the BiRRT using the standard RRT algorithm.
* @param 
* @return The error status of the function is returned as of an integer value. 
*/
template<class TConfig> RRT_RELATED_TREE_EXTENSION_RESULT BiRRT<TConfig>::extend(Graph<TConfig>& tree, const TConfig& config_rand, GraphNode<TConfig>*& node_new) // even though I could use the member variable of the class Planner and therefore there would not be need for passing the tree as argument, but for bidirectional graph case, I need to specify the right tree, and therefore, I decided to pass the tree info as an argument of the function. 
{		
	GraphNode<TConfig> *node_nearest = NULL;// , *node_new = NULL; 
	GraphEdge<TConfig>* edge_new = NULL; 
	TConfig config_new; 
	
	node_nearest = tree.getNearestNode(this->distanceFunction_, config_rand); // the tree_ is a member variable of the class Planner, which is the base class to Rthe class RRT (a derived class). 
	if(node_nearest != NULL){
		if ( obtainNewConfig(tree, node_nearest, config_rand, config_new) == OK ){
			
			////////////////////////////////////////////////////////////////////////////////////////////
			// ESTIMATE THE ROBOT POSE TO KNOW WHETHER THE ROBOT FACES BACKWARD TO THE STAIR; IF SO SUCH PATH IS NOT FEASIBLE!
			// BUT, THIS CAN BE VERY TIME CONSUMING
			if (this->updateFullRobotConfigFromItsHorionzontalConfig(config_new, this->robot_) == ERROR){ // update the robot configuration	
				return TRAPPED; 		
			}
			config_new.q_ = this->robot_->getPose();
			
			if (tree.graph_type_ == START_TREE){ // for the start tree, the cost is computed from the nearest node to the new node
			
				// if the motion from the nearest node to the new node is backward
				if (this->is_robot_motion_forward_ == false){ // is_robot_motion_forward_ is updated in the isTraversableBWTwoConfigs() function located in planner.hpp, and this function has been called in the obtainNewConfig() function 

					// if the backward motion has to be carried out, then such a motion is not allowed if the absolute value of the associated pitch angle is larger than a threshold. 							
					if(fabs(config_new.q_[4] - node_nearest->config_.q_[4]) > ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS){ // When the nose is facing down the corresponding pitch angle is positive. The value for ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS is found empirically. 							
					
						// for analyzing the influence of ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS to the planning efficiency.
						tree.num_samples_rejected_by_high_pitch_++;
						
						return TRAPPED; 
					}
				}
			}else{
				
				// if the motion from the new node to the nearest node is backward
				if (this->is_robot_motion_forward_ == false){	// is_robot_motion_forward_ is updated in the isTraversableBWTwoConfigs() function located in planner.hpp, and this function has been called in the obtainNewConfig() function 
					
					// if the backward motion has to be carried out, then such a motion is not allowed if the absolute value of the associated pitch angle is larger than a threshold. 
					if(fabs(node_nearest->config_.q_[4] - config_new.q_[4]) > ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS){ // When the nose is facing down the corresponding pitch angle is positive. The value for ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS is found empirically. 
					
						// for analyzing the influence of ANG_PITCH_TRAVERSABLE_WITHOUT_FLIPPERS to the planning efficiency.
						tree.num_samples_rejected_by_high_pitch_++;
						
						return TRAPPED; 
					}
				}
			}
			
			node_new = tree.addNode(config_new);
			edge_new = tree.addEdge(node_nearest, node_new);
			
			node_new->setNodeParent(node_nearest); 
			node_new->setEdgeToParent(edge_new); 
			
			if (config_new == config_rand){
				return REACHED; // enum or constant integer
			}else{
				return ADVANCED; // enum or constant integer
			}
		}
	}
	return TRAPPED; // enum or constant integer	
}

/**
* obtainNewConfig() is a function that returns a new configuration if such configuration has already satisfied the collision checking procedure. 
* @param 
* @return The error status of the function is returned as of an integer value. 
*/
template<class TConfig> int BiRRT<TConfig>::obtainNewConfig(Graph<TConfig>& tree, GraphNode<TConfig>* & node_nearest, const TConfig& config_rand, TConfig& config_new)
{
	Real distance; 
			
	distance = distanceFunction_(node_nearest->config_, config_rand); 
	if (distance > this->size_step_tree_expansion_){
						
		if(interpolateSampleLinearly(node_nearest->config_, config_rand, (this->size_step_tree_expansion_/distance), config_new) == ERROR){ // if OK, config will have some meaningful coordinates; but, if ERROR, then config is NULL.
			return ERROR; 
		}
	}else{
		config_new = config_rand; 
	}
		
	TConfig *config0, *config1; // config0 and config are the start and end configurations of a considered path segment. 
	if (tree.graph_type_ == START_TREE){ // in this case, the yaw angle of the nearest tree-node is updated with the information of the new node. 
		config0 = &(node_nearest->config_); 
		config1 = &config_new; 
		config1->q_[5] = modNPiPi(atan2f(config1->q_[1] - config0->q_[1], config1->q_[0] - config0->q_[0])); 
								
	}else{ // tree.graph_type_ == GOAL_TREE, // in this case, the yaw angle of the new node is updated with the information of the nearest tree-node
		config0 = &config_new; 
		config1 = &(node_nearest->config_); 
		config0->q_[5] = modNPiPi(atan2f(config1->q_[1] - config0->q_[1], config1->q_[0] - config0->q_[0]));
	}

	if(this->isTraversableBWTwoConfigs(*config0, *config1, TREE_EXTENSION, tree.graph_type_) == ERROR){
		return ERROR; 
	}
			
	return OK; 	
}


template<class TConfig> RRT_RELATED_TREE_EXTENSION_RESULT BiRRT<TConfig>::connectTrees(Graph<TConfig>& tree1, GraphNode<TConfig>& node_new_tree0, GraphNode<TConfig>*& node_nearest_tree1)
{
	node_nearest_tree1 = tree1.getNearestNode(this->distanceFunction_, node_new_tree0.config_); // <-- for some reason I can't assign the address of the nearest node to the "node_nearest_treeB", but I need to pass through the local pointer "node_nearest_tmp". Why? 
	if(node_nearest_tree1 != NULL){
		if((node_new_tree0.config_.q_.segment(0,2) - node_nearest_tree1->config_.q_.segment(0,2)).norm() < this->radius_ball_goal_region_){ // distance in x and y only 

			TConfig *config0, *config1; // config0 and config are the start and end configurations of a considered path segment. 
			if (tree1.graph_type_ == GOAL_TREE){ 
				config0 = &node_new_tree0.config_; 
				config1 = &node_nearest_tree1->config_; 
			}else{ // tree1.graph_type_ == START_TREE
				config0 = &node_nearest_tree1->config_; 
				config1 = &node_new_tree0.config_; 
			}
			
			// here I don't need to update the robot pose since both config0 and config1 are supposed to have the right configuration (estimated using the pose estimation function). 
			
			// for connection the trees, do not change the yaw angle of the config1. 			
			if(this->isTraversableBWTwoConfigs(*config0, *config1, TWO_TREES_CONNECTION) == OK){ // Here the function agrument for the GRAPH_TYPE does not make sense to be given since here we are dealing with both trees. Therefore, let us omit this argument. 
				return REACHED; 
			}
		}
		 
	}
	return TRAPPED; 
}


template<class TConfig> int BiRRT<TConfig>::returnPath(Graph<TConfig>& tree0, Graph<TConfig>& tree1, GraphNode<TConfig>& node_tree0, GraphNode<TConfig>& node_tree1, std::vector<TConfig>& config_path)
{
	std::list<TConfig> config_path_list; 
	
	if (this->node_start_ == NULL || this->node_goal_ == NULL){
		return ERROR; 
	}
	
	GraphNode<TConfig> *node_union_tree_start, *node_union_tree_goal; 
	
	if( tree0.graph_type_ == START_TREE){ // then this is the start-node tree, 
		node_union_tree_start = &node_tree0;
		node_union_tree_goal = &node_tree1; 
	}else{
		node_union_tree_start = &node_tree1;
		node_union_tree_goal = &node_tree0; 
	}
	
	GraphNode<TConfig>* node = NULL;
	
	// start with the goal-node tree
	config_path_list.push_back(node_union_tree_goal->config_);
	node = node_union_tree_goal->getParentNode(); 

	if (node == NULL){
		return ERROR;
	}
	
	while( node != this->node_goal_ ){
		if (node->getEdgeToParentNode() != NULL){		
			config_path_list.push_back(node->config_);
		}else{
			return ERROR;
		}
				
		node = node->getParentNode();
		if (node == NULL){
			return ERROR;
		}
		
	}
	
	if (node == this->node_goal_){
		config_path_list.push_back(node->config_); 
	}
	
	// now continue with start-node tree.	
	// For the start-node tree, the yaw angle does not need to be recomputed because it is initially computed in the forward manner (from a parent to its child). 
	
	config_path_list.push_front(node_union_tree_start->config_); 
	node = node_union_tree_start->getParentNode(); 
	if( node == NULL ){
		return ERROR;
	}
	
	while( node != this->node_start_ ){
		if (node->getEdgeToParentNode() != NULL){		
			config_path_list.push_front(node->config_); 
		}else{
			return ERROR; 
		}
		
		node = node->getParentNode(); 
		if (node == NULL){
			return ERROR; 
		}
	}
	
	if (node == this->node_start_){
		config_path_list.push_front(node->config_); 
	}
	
	config_path.clear(); 
	config_path.resize(config_path_list.size()); 
	std::copy(config_path_list.begin(), config_path_list.end(), config_path.begin()); 
	
	return OK; 
}


/**
* This displayGraphNodes() function displays the RRT* tree nodes as points in RVIZ. 
*/
template<class TConfig> void BiRRT<TConfig>::displayNodesGoalTree()
{
	visualization_msgs::Marker marker;
	// set the frame ID and timestamp
	marker.header.frame_id = "/terrain_elev_map";
	marker.header.stamp = ros::Time();

	// set the namespace, type, action and id for this marker. This serves to create a unique ID.
	marker.ns = "goalTree_nodes";
	marker.type = visualization_msgs::Marker::SPHERE_LIST;
	marker.action = visualization_msgs::Marker::ADD;
	marker.id = 1;

	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;

	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;

	marker.scale.x = .5*g_size_node_display;//5*step_c_space_; // 0.05;
	marker.scale.y = .5*g_size_node_display;//5*step_c_space_; // 0.05;
	marker.scale.z = .5*g_size_node_display;//5*step_c_space_; // 0.05;

	marker.color.r = 0.0;
	marker.color.g = 0.0;
	marker.color.b = 1.0;
	marker.color.a = 1.0;

	// specify the color of the marker
	std_msgs::ColorRGBA color;	

	color.r = 0.0;
	color.g = 0.0;
	color.b = 1.0;
	color.a = 1.0;
	
	marker.color = color;

	// Point
	geometry_msgs::Point point_a;

	marker.points.reserve(tree_goal_.nodes_.size()); // this should make the execution more efficient, since I reserve the memory space ahead rather than reallocating the memory size posteriorly. 

	// loop through all vertices
	typename std::list<GraphNode<TConfig> * >::const_iterator inode;
	for(inode=tree_goal_.nodes_.begin() ; inode!=tree_goal_.nodes_.end() ; inode++){
		point_a.x = (*inode)->config_.q_[0];
		point_a.y = (*inode)->config_.q_[1];
		point_a.z = this->map_elev_curr_->estimateTerrainHeight((*inode)->config_.q_[0], (*inode)->config_.q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;

		// add the point pair to the message
		marker.points.push_back(point_a);
	}

	// publish the marker
	pub_marker_nodes_goalTree_.publish(marker);
	ros::Duration(0.2).sleep();	
}

/**
* This displayGraphEdges() function displays the RRT* tree edges as line segments in RVIZ. 
*/
template<class TConfig> void BiRRT<TConfig>::displayEdgesGoalTree()
{
	visualization_msgs::Marker marker;
	
	// set the frame ID and timestamp
	marker.header.frame_id = "/terrain_elev_map";
	marker.header.stamp = ros::Time();

	// set the namespace, type, action and id for this marker. This serves to create a unique ID.
	marker.ns = "goalTree_edges";
	marker.type = visualization_msgs::Marker::LINE_LIST;
	marker.action = visualization_msgs::Marker::ADD;
	marker.id = 2;

	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;

	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;

	marker.scale.x = 0.02; // g_size_node_display;//3*step_c_space_; // 0.02;
	marker.scale.y = 0.02; // g_size_node_display;//3*step_c_space_; // 0.02;
	marker.scale.z = 0.02; // g_size_node_display;//3*step_c_space_; // 0.02;

	marker.color.r = 1.0;
	marker.color.g = 0.0;
	marker.color.b = 1.0;
	marker.color.a = 1.0;

	// specify the color of the marker
	std_msgs::ColorRGBA color;

	color.r = 0.0;
	color.g = 0.0;
	color.b = 1.0;
	color.a = 1.0;

	marker.color = color; 
	
	// Point
	geometry_msgs::Point point_a, point_b;

	marker.points.reserve(2*tree_goal_.edges_.size()); // this should make the execution more efficient, since I reserve the memory space ahead rather than reallocating the memory size posteriorly. 

	// loop through all vertices
	typename std::list<GraphEdge<TConfig> * >::const_iterator iedge;
	for(iedge=tree_goal_.edges_.begin() ; iedge!=tree_goal_.edges_.end() ; ++iedge){
		point_a.x = (*iedge)->getStartNode()->config_.q_[0];
		point_a.y = (*iedge)->getStartNode()->config_.q_[1];
		point_a.z = this->map_elev_curr_->estimateTerrainHeight((*iedge)->getStartNode()->config_.q_[0], (*iedge)->getStartNode()->config_.q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;

		point_b.x = (*iedge)->getEndNode()->config_.q_[0];
		point_b.y = (*iedge)->getEndNode()->config_.q_[1];
		point_b.z = this->map_elev_curr_->estimateTerrainHeight((*iedge)->getEndNode()->config_.q_[0], (*iedge)->getEndNode()->config_.q_[1]) + 1.0 * ROBOT_TRACK_HEIGHT;


		// add the point pair to the message
		marker.points.push_back(point_a);
		marker.points.push_back(point_b);
	}

	// publish the marker
	pub_marker_edges_goalTree_.publish(marker);
	ros::Duration(0.2).sleep();	
}


// } // end of the namespace fraudo

#endif
