/**
 * @file planPathOverStaticGlobalMap.cpp
 * @author Jae Yun Jun <jaeyun.isir@gmail.com>
 * 
 * Created:         April 16 2014; 
 * Lastly modified: April 18 2015; 
 * 
 * ISIR, UPMC-CNRS, Paris, France
 * 
 * @section License
 * 
 * copyright (c) <2015>, <Jae Y. Jun>
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 

 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 * 
**/

#include <ros/ros.h>

#include <fraudo_motion_planning/terrain.h>
#include <fraudo_motion_planning/robot.h>
#include <fraudo_motion_planning/planner.hpp>
#include <fraudo_motion_planning/RRTRelatedAlgorithms.hpp>

#include <fraudo_msgs/TerrainElevMapWithObstacles.h>
#include <std_msgs/Bool.h>

#include <tf/transform_listener.h>
#include <geometry_msgs/PoseStamped.h>

// using namespace fraudo; 

void subscribeTerrainElevMapWithObstaclesTopic(const fraudo_msgs::TerrainElevMapWithObstacles::ConstPtr& msg_terrain_elev_map_with_obstacles_ptr);
void subscribePoseStampedStartTopic(const geometry_msgs::PoseStamped& poseStamped_start); 
void subscribePoseStampedGoalTopic(const geometry_msgs::PoseStamped& poseStamped_goal); 


Planner<TrackedMobileRobotConfig>* planner = NULL; 

Terrain *map_elev_curr, *map_elev_new; 

bool do_parameter_study = false; //true;

bool is_new_map_received = false; 
bool is_robot_start_pose_received = false; 
bool is_robot_goal_pose_received = false; 
bool can_new_map_be_listened = true; 
bool can_new_robot_start_pose_be_listened = true; 
bool can_new_robot_goal_pose_be_listened = true; 

// Assigning values to the class Config's static variables. 
int Config::num_config_dimensions_ = NUM_CONFIG_DIMENSIONS_TRACKEDMOBILEROBOT; 
int Config::num_sample_dimensions_ = NUM_SAMPLE_DIMENSIONS_TRACKEDMOBILEROBOT; 

geometry_msgs::PoseStamped g_poseStamped_start, g_poseStamped_goal; 


template<typename TConfig> Real distanceSquared3DMobileRobot(const TConfig& config1, const TConfig& config2)
{
	Real dist_trans; // [m], translational distance.

	// I will not do sqrt since it requires quite significant number of comuptations for each distance measurement. 		
	dist_trans = pow(config1.q_[0]-config2.q_[0], 2.0) + pow(config1.q_[1]-config2.q_[1], 2.0) + pow(config1.q_[2]-config2.q_[2], 2.0); // distance in 3D 

	return dist_trans;
}


/**
 * The main function 
 *
 * @param  argc counter of arguments.
 * @param   argv input arguments.
 * @return The status of how the program is exited is returned. 
*/
int main(int argc, char** argv)
{
	
	int seed = (int)(time(NULL)); 
	
	int err = OK; 
	
	ROS_INFO("Static global map path planning algorithm has been started..."); 
	
	ros::init(argc, argv, "static_global_map_path_planning"); 
	
	ros::NodeHandle nh; 
	
	ros::Subscriber sub_terrain_elev_map_with_obstacles = nh.subscribe("/terrain_elev_map_with_obstacles", 2, subscribeTerrainElevMapWithObstaclesTopic); 
	ros::Subscriber sub_poseStamped_start = nh.subscribe("/poseStamped_start", 2, subscribePoseStampedStartTopic); 
	ros::Subscriber sub_poseStamped_goal = nh.subscribe("/poseStamped_goal", 2, subscribePoseStampedGoalTopic);
	
	ros::Publisher pub_terrain_map_received = nh.advertise<std_msgs::Bool>("/terrain_map_received",1); 
			
	std::string algorithm = "birrt"; 
	
	bool is_map_static_global = true; 
	Real size_step_cspace_discretization = 0.01; //0.01; // [m]; the step size for the discretization of the configuration space. 
	
	Real (*distanceFunction)(const TrackedMobileRobotConfig&, const TrackedMobileRobotConfig&); 
		
	Robot robot("robot1"); 
	Robot robot_goal("robot2"); 
		
	map_elev_curr = new Terrain(is_map_static_global); 
	map_elev_new = new Terrain(is_map_static_global); 
	
	std::istringstream iss; 
	std::string argument="birrt", type_terrain="flat", type_cost="tip_over_stability", type_study="algorithm_type_analysis"; 
	int num_contact_track=0, id_trial=0;	// , num_contact_total=0
	COST_FUNCTION_TYPE cost_function = TIP_OVER_STABILITY; 
	Real time_sampling_lcp=0.01, trr_param0=0.0, trate_trrt=1.0, distance_max_refinement=0.1, ratio_max_refinementNodes_to_totalNodes=0.1; 
	int do_store_results = 0; 
	if (argc>1){
		for(int i=1; i<argc; ++i){
			iss.clear(); 
			argument = std::string(argv[i]); 
			
			if (argument.compare(0, 6, "-algo=") == 0){
				iss.str(argument.substr(6));
				(iss >>  algorithm);
				if (!(algorithm=="rrt" || algorithm=="birrt")){
					ROS_ERROR("wrong planning algorithm"); 
					exit(-1);
				}
			}else if (argument.compare(0, 9, "-terrain=") == 0){
				iss.str(argument.substr(9)); 
				(iss >> type_terrain); 
				if (!(type_terrain=="flat" || type_terrain=="step" || 
				      type_terrain=="step_up_and_down" || type_terrain=="ramp" || 
				      type_terrain=="stair" || type_terrain=="stair_and_ramp" || 
				      type_terrain=="stair_isir" || type_terrain=="stair_with_crossing" || 
				      type_terrain=="stair_ramp_step_hole" || type_terrain=="stair_ramp_smallSteps_hole" || 
				      type_terrain=="stair_ramp_smallSteps_hole_parkings" || type_terrain=="rough" || 
				      type_terrain=="pcd_stairs2" || type_terrain=="pcd_stairs3" || type_terrain=="flat_with_step")){
					ROS_ERROR("wrong terrain"); 
					exit(-1);
				}				
			}else if (argument.compare(0, 6, "-cost=") == 0){
				iss.str(argument.substr(6)); 
				(iss >> type_cost); 
				if (type_cost == "mechanical_work"){
					cost_function = MECHANICAL_WORK; 
				}else if (type_cost == "tip_over_stability"){
					cost_function = TIP_OVER_STABILITY; 
				}else if (type_cost == "energy_consumption"){
					cost_function = ENERGY_CONSUMPTION; 
				}else if (type_cost == "tip_over_stability_g1"){
					cost_function = TIP_OVER_STABILITY_G1; 
				}else if (type_cost == "tip_over_stability_g2"){
					cost_function = TIP_OVER_STABILITY_G2; 
				}else if (type_cost == "tip_over_stability_g3"){
					cost_function = TIP_OVER_STABILITY_G3; 
				}else{
					ROS_ERROR("wrong type of cost function"); 
					exit(-1); 
				}					
			}else if (argument.compare(0, 20, "-numContactPerTrack=") == 0){
				iss.str(argument.substr(20)); 
				(iss >> num_contact_track); 
			}else if (argument.compare(0, 13, "-timeStepLCP=") == 0){
				iss.str(argument.substr(13)); 
				(iss >> time_sampling_lcp); 
			}
			else if (argument.compare(0, 6, "-seed=") == 0){
				iss.str(argument.substr(6)); 
				(iss >> seed); 
			}else if (argument.compare(0, 10, "-id_trial=") == 0){
				iss.str(argument.substr(10));
				(iss >> id_trial); 
			}else if (argument.compare(0, 11, "-trrParam0=") == 0){
				iss.str(argument.substr(11)); 
				(iss >> trr_param0); 
			}else if (argument.compare(0, 7, "-study=") == 0){
				iss.str(argument.substr(7)); 
				(iss >> type_study); 
			}else if (argument.compare(0, 12, "-trate_trrt=") == 0){
				iss.str(argument.substr(12)); 
				(iss >> trate_trrt); 
			}else if (argument.compare(0, 18, "-do_store_results=") == 0){
				iss.str(argument.substr(18)); 
				(iss >> do_store_results); 
			}else if (argument.compare(0, 21, "-dist_max_refinement=") == 0){
				iss.str(argument.substr(21)); 
				(iss >> distance_max_refinement); 
			}else if (argument.compare(0, 24, "-ratio_refinement_nodes=") == 0){
				iss.str(argument.substr(24)); 
				(iss >> ratio_max_refinementNodes_to_totalNodes); 
			}

		}
	}
	
	std::cout << "In main(): distance_max_refinement: " << distance_max_refinement << std::endl;

	SimulationParamInput param_input_simulation; 
	param_input_simulation.type_terrain = type_terrain; 
	param_input_simulation.algorithm_planning = algorithm;
	param_input_simulation.type_cost = type_cost;
	param_input_simulation.type_study = type_study;
	param_input_simulation.id_trial = id_trial;
	param_input_simulation.seed = seed;
	param_input_simulation.trate_trrt = trate_trrt; 
	param_input_simulation.do_store_results = do_store_results; 
	param_input_simulation.time_sampling_lcp = time_sampling_lcp;
	param_input_simulation.distance_max_refinement = distance_max_refinement; 
	param_input_simulation.ratio_max_refinementNodes_to_totalNodes = ratio_max_refinementNodes_to_totalNodes;
		
	
	srand(seed); // change the seed!	
	std::cout << "seed: " << seed << std::endl;
					
	if(algorithm == "rrt"){				
		distanceFunction = distanceSquared3DMobileRobot<TrackedMobileRobotConfig>; 
		planner = new RRT<TrackedMobileRobotConfig>(&robot, &robot_goal, is_map_static_global, size_step_cspace_discretization, cost_function, distanceFunction);
	}else if(algorithm == "birrt"){
		distanceFunction = distanceSquared3DMobileRobot<TrackedMobileRobotConfig>; 
		planner = new BiRRT<TrackedMobileRobotConfig>(&robot, &robot_goal, is_map_static_global, size_step_cspace_discretization, cost_function, distanceFunction);
	}else{
		ROS_ERROR("wrong planning algorithm"); 
		exit(-1); 
	}
				
	// specify that the sample space is the first two dimensions of the configuration space. That is, x and y coordinates
	std::vector<int>ind_sample_dims_in_config_dims; 
	ind_sample_dims_in_config_dims.push_back(0); // for x
	ind_sample_dims_in_config_dims.push_back(1); // for y
	planner->setIndSampleDimsInConfigDims(ind_sample_dims_in_config_dims);
			 	
	ros::Rate r(1); // loop cycle speed 1Hz	
	while(ros::ok()){
	
		ROS_INFO_STREAM("Waiting for the Terrain Publisher..."); 
		
		if (is_new_map_received && is_robot_start_pose_received){
			
			std_msgs::Bool did_planner_receive_terrain_map;
			did_planner_receive_terrain_map.data = true;
			pub_terrain_map_received.publish(did_planner_receive_terrain_map); 
		
			is_new_map_received = false; 
			is_robot_start_pose_received = false; 
			
			map_elev_curr = map_elev_new; 
			
			Eigen::Matrix<float,NUM_SAMPLE_DIMENSIONS_TRACKEDMOBILEROBOT,2> bounds_sample_dims; // first column: lower bound; second column: upper bound
			// for x and y
			bounds_sample_dims << map_elev_curr->xmin_, map_elev_curr->xmax_, 
								  map_elev_curr->ymin_, map_elev_curr->ymax_; 
						
			planner->setBoundsSampleDims(bounds_sample_dims); // first column: lower bound; second column: upper bound
						
			if (do_parameter_study == false){
				err = planner->generateTrajectory(map_elev_curr, g_poseStamped_start, g_poseStamped_goal, param_input_simulation);
			}else{
				err = planner->studyInfluencePtsContactTimeSampling(map_elev_curr, g_poseStamped_start, g_poseStamped_goal, param_input_simulation);
			}
						
			break; 
		}
		
		ros::spinOnce(); 
		r.sleep(); 
	}	
	
	ROS_INFO_STREAM("Finished with planning path(s) for a static global map."); 
	
	return err; 
	
}

/**
 * Subscribes the topic of terrain elevation maps. 
 * 
 * @param  fraudo_msgs::TerrainElevMapWithObstacles::ConstPtr& msg_terrain_elev_map_with_obstacles_ptr
*/	
void subscribeTerrainElevMapWithObstaclesTopic(const fraudo_msgs::TerrainElevMapWithObstacles::ConstPtr& msg_terrain_elev_map_with_obstacles_ptr)
{
	if(can_new_map_be_listened){
		can_new_map_be_listened = false; 
		
		map_elev_new->num_samples_x_ = msg_terrain_elev_map_with_obstacles_ptr->num_samples_x;
		map_elev_new->num_samples_y_ = msg_terrain_elev_map_with_obstacles_ptr->num_samples_y;
		map_elev_new->res_cell_x_ = msg_terrain_elev_map_with_obstacles_ptr->res_cell_x;
		map_elev_new->res_cell_y_ = msg_terrain_elev_map_with_obstacles_ptr->res_cell_y;
		map_elev_new->xmin_ = msg_terrain_elev_map_with_obstacles_ptr->xmin;
		map_elev_new->xmax_ = msg_terrain_elev_map_with_obstacles_ptr->xmax;
		map_elev_new->ymin_ = msg_terrain_elev_map_with_obstacles_ptr->ymin;
		map_elev_new->ymax_ = msg_terrain_elev_map_with_obstacles_ptr->ymax;
	
		map_elev_new->ind_label_region_startConfig_belongsTo_ = msg_terrain_elev_map_with_obstacles_ptr->ind_label_region_startConfig_belongsTo;
		
		map_elev_new->updateElevMap(msg_terrain_elev_map_with_obstacles_ptr->pc2); 
		
		// subscribe the convex hulls of the segmented planes.
		// First erase the vector map_elev_new->coords_convex_hulls_segmented_planes_ in order not to carry the ones I had assigned from past maps.
		int num_total_obstacles_past = map_elev_new->obstacle_.size(); // if there was any obstacle defined previously. This makes more sense when we have multiple local maps that are built sequentially, and we plan over each of these local maps. 
		for(int i=0; i<num_total_obstacles_past; ++i){
			map_elev_new->obstacle_[i].clear(); 
		}
		map_elev_new->obstacle_.clear(); 

		map_elev_new->num_total_obstacles_ = msg_terrain_elev_map_with_obstacles_ptr->num_total_obstacles; 
		for(int i=0; i<map_elev_new->num_total_obstacles_; ++i){
			std::vector<Eigen::Vector3f> pt_obstacle_contour; 
			for(int j=0; j<msg_terrain_elev_map_with_obstacles_ptr->obstacle[i].num_total_edge_pts; ++j){
				pt_obstacle_contour.push_back(Eigen::Vector3f(msg_terrain_elev_map_with_obstacles_ptr->obstacle[i].x[j], 
				                                              msg_terrain_elev_map_with_obstacles_ptr->obstacle[i].y[j], 
															  msg_terrain_elev_map_with_obstacles_ptr->obstacle[i].z[j])); 

			}
			map_elev_new->obstacle_.push_back(pt_obstacle_contour); 
		}	
		
		ROS_INFO_STREAM("New elevation map is acquired!"); 
		
		is_new_map_received = true; 	
	}
}

/**
 * Subscribes the topic of PoseStamped start point
 *  
 * @param  poseStamped_start a pointer for geometry_msgs::PoseStamped
*/	
void subscribePoseStampedStartTopic(const geometry_msgs::PoseStamped& poseStamped_start)
{
	if(can_new_robot_start_pose_be_listened){		
		can_new_robot_start_pose_be_listened = false; 		
		
		g_poseStamped_start = poseStamped_start; 
		map_elev_new->setRobotStartPose(poseStamped_start); 					
		
		ROS_INFO_STREAM("New start robot pose is acquired!"); 		
		is_robot_start_pose_received = true;
	}	
}

/**
 * Subscribes the topic of PoseStamped goal point
 *  
 * @param  poseStamped_goal a pointer for geometry_msgs::PoseStamped
*/	
void subscribePoseStampedGoalTopic(const geometry_msgs::PoseStamped& poseStamped_goal)
{
	if(can_new_robot_goal_pose_be_listened){					
		can_new_robot_goal_pose_be_listened = false; 		
		
		g_poseStamped_goal = poseStamped_goal; 
		
		ROS_INFO_STREAM("New goal robot pose is acquired!"); 		
		is_robot_goal_pose_received = true; 
	}
	
}
